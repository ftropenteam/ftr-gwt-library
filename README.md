The artifacts are now available in the maven central repo. We hope that people will find it helpfull. 


```
#!xml
<dependency>
   <groupId>eu.future-earth.gwt</groupId> 
   <artifactId>ftr-gwt-calendar-emulation</artifactId>
   <version>2.0</version>
</dependency>
<dependency> 
    <groupId>eu.future-earth.gwt</groupId>
    <artifactId>ftr-gwt-ui</artifactId>
    <version>2.1</version>
</dependency>
<dependency> 
   <groupId>eu.future-earth.gwt</groupId>
   <artifactId>ftr-gwt-calendar-ui</artifactId>
   <version>2.2</version>
</dependency>
<dependency>
   <groupId>eu.future-earth.gwt</groupId>
   <artifactId>ftr-gwt-charts</artifactId>
   <version>2.0</version>
</dependency>
<dependency>
   <groupId>eu.future-earth.gwt</groupId>
   <artifactId>ftr-gwt-slider</artifactId>
   <version>2.1</version>
</dependency>

```



Training and consulting

We offer training and consulting for GWT.
Please take a look at the following link: http://www.future-earth.eu/?page_id=72

Currently the training description is in Dutch, but the course can also be given in English. Please contact us for more information.

An Demo version can be seen here:
http://www.future-earth.eu/framework/ 
We had some questions about a commercial version of the library. We have discussed about this but are against it. We are willing to supply support on an hourly basis or maybe under a support contract setup. We want to know what the general opinion is about this. Would such a possiblety make the library more easy to use within your company.

Let us know at info@future-earth.eu

= Contents =

The Library contains at the moment:
  * Google look a like Week calender
  * Google look a like Day calender
  * Google look a like Month calender
  * DateBox A Formatted text box which allows users to enter dates.
  * TimeBox A Formatted Text Box which allows users to enter times.

More will follow with each release.



Please do file issues. We are aware of several issues that people want resolved. Please check the list before entering new issues. One of our testers is The developer of the rounded corners. See http://code.google.com/p/cobogw/

The use of the library is fairly simple. checkout the source code to see how the library can be used.