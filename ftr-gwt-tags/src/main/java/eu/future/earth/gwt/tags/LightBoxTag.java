package eu.future.earth.gwt.tags;

import java.io.IOException;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.JspWriter;
import jakarta.servlet.jsp.tagext.TagSupport;

/**
 * <p>
 * Title: RootTag
 * </p>
 * <p>
 * Description: This tag is usefull for use in other tagt to determine the root of the web application.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Future Earth
 * </p>
 * 
 * @author Marteijn Nouwens
 * @version 1.0
 */
public class LightBoxTag extends TagSupport {

	private String image = null;

	private String title = null;

	private boolean complete = true;

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2991312018062132961L;

	/**
	 * Creates an instance of the RooTag.
	 */
	public LightBoxTag() {
		super();
	}

	/**
	 * Process the start tag for this instance. This tag always Skips the body. It just prints the root context if
	 * possible. It awalys ill end the tag with a <code>/</code>.
	 * 
	 * 
	 * 
	 * <a href="employee_agenda_viewperactivity.png" rel="lightbox" title="Agenda per activiteit bekeken"> <img
	 * class="clickableImage" alt="Klik om te vergroten" title="Klik om te vergroten"
	 * src="employee_agenda_viewperactivity.png" /> </a>
	 * 
	 * @throws JspException
	 *             if an error occurred while processing this tag
	 */
	public int doStartTag() throws JspException {
		if (super.pageContext.getRequest() instanceof HttpServletRequest) {
			final HttpServletRequest request = (HttpServletRequest) super.pageContext.getRequest();
			final JspWriter out = super.pageContext.getOut();
			try {
				String imageComplete = image;
				if (complete) {
					imageComplete = request.getContextPath();
					if (!imageComplete.endsWith("/")) {
						imageComplete += "/";
					}
					imageComplete += image;
				}
				out.print("<a href=\"" + imageComplete + "\" rel=\"lightbox\" ");
				if (title != null) {
					out.print("title=\"" + title + "\"");
				} else {
					out.print("title=\"" + image + "\"");
				}
				out.println(">");
				out.print("<img class=\"clickableImage\" title=\"Klik om te vergroten\" src=\"" + imageComplete + "\" />");
				out.print("</a>");
			} catch (IOException ex) {
				// SHould not happen but log as Error
				throw new JspException(ex);
			}
		}
		return TagSupport.SKIP_BODY;
	}
}
