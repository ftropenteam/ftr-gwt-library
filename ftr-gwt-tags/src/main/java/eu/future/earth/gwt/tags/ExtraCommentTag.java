package eu.future.earth.gwt.tags;

import java.io.IOException;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.JspWriter;
import jakarta.servlet.jsp.tagext.TagSupport;

/**
 * <p>
 * Title: RootTag
 * </p>
 * <p>
 * Description: This tag is usefull for use in other tagt to determine the root of the web application.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Future Earth
 * </p>
 * 
 * @author Marteijn Nouwens
 * @version 1.0
 */
public class ExtraCommentTag extends TagSupport {

	private String image = null;

	private String title = null;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2991312018062132961L;

	/**
	 * Creates an instance of the RooTag.
	 */
	public ExtraCommentTag() {
		super();
	}

	/**
	 * Process the start tag for this instance. This tag always Skips the body. It just prints the root context if
	 * possible. It awalys ill end the tag with a <code>/</code>.
	 * 
	 * <div class="imageholder"> <span> Facturatiegegevens bekijken </span> <a
	 * href="employee_agenda_viewperactivity.png" rel="lightbox" title="Agenda per activiteit bekeken"> <img
	 * class="clickableImage" alt="Klik om te vergroten" title="Klik om te vergroten"
	 * src="employee_agenda_viewperactivity.png" /> </a> </div>
	 * 
	 * @throws JspException
	 *             if an error occurred while processing this tag
	 */
	public int doStartTag() throws JspException {
		if (super.pageContext.getRequest() instanceof HttpServletRequest) {
			final HttpServletRequest request = (HttpServletRequest) super.pageContext.getRequest();
			final JspWriter out = super.pageContext.getOut();
			try {

				out.println("<fieldset class=\"extra\">");

				out.print("<legend class=\"info\"><img title=\"Info\"  src=\"");
				String imageComplete = request.getContextPath();
				if (!imageComplete.endsWith("/")) {
					imageComplete += "/images/";
				} else {
					imageComplete += "images/";
				}
				imageComplete += image;
				out.print(imageComplete);
				out.print("\" />");
				if (title != null) {
					out.print("<span>");
					out.print(title);
					out.print("</span>");
				}
				out.print("</legend>");
			} catch (IOException ex) {
				// SHould not happen but log as Error
				throw new JspException(ex);
			}
		}
		return TagSupport.EVAL_BODY_INCLUDE;
	}

	@Override
	public int doEndTag() throws JspException {
		try {
			final JspWriter out = super.pageContext.getOut();
			out.print("</fieldset>");
		} catch (IOException ex) {
			// SHould not happen but log as Error
			throw new JspException(ex);
		}
		return super.doEndTag();
	}

}
