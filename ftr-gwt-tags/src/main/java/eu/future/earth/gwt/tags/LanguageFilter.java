package eu.future.earth.gwt.tags;

import java.io.IOException;
import java.util.Locale;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

public class LanguageFilter implements Filter {

	private FilterConfig filterConfig;

	// Handle the passed-in FilterConfig
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	// Process the request/response pair
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) {
		try {
			HttpServletRequest req = (HttpServletRequest) request;
			HttpSession session = req.getSession();
			String loc = req.getParameter("locale");
			if (loc != null && loc.length() > 0) {
				// Set the coosen local if valid
				final Locale newLoc = new Locale(loc);
				session.setAttribute(Globals.LOCALE_KEY, newLoc);
			} else {
				if (session.getAttribute(Globals.LOCALE_KEY) != null) {
					Locale userPreferredLocale = request.getLocale();
					if (userPreferredLocale != null) {
						session.setAttribute(Globals.LOCALE_KEY, userPreferredLocale);
					}
				}
			}
			filterChain.doFilter(request, response);
		} catch (ServletException sx) {
			filterConfig.getServletContext().log(sx.getMessage());
		} catch (IOException iox) {
			filterConfig.getServletContext().log(iox.getMessage());
		}
	}

	// Clean up resources
	public void destroy() {
	}
}
