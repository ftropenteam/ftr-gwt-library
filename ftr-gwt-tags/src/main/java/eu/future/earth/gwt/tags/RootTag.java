package eu.future.earth.gwt.tags;

import java.io.IOException;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.JspWriter;
import jakarta.servlet.jsp.tagext.TagSupport;

/**
 * <p>
 * Title: RootTag
 * </p>
 * <p>
 * Description: This tag is usefull for use in other tagt to determine the root of the web application.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Future Earth
 * </p>
 * 
 * @author Marteijn Nouwens
 * @version 1.0
 */
public class RootTag extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2991312018062132961L;

	/**
	 * Creates an instance of the RooTag.
	 */
	public RootTag() {
		super();
	}

	/**
	 * Process the start tag for this instance. This tag always Skips the body. It just prints the root context if possible. It awalys ill end the tag with a <code>/</code>.
	 * 
	 * @throws JspException
	 *             if an error occurred while processing this tag
	 */
	public int doStartTag() throws JspException {
		if (super.pageContext.getRequest() instanceof HttpServletRequest) {
			final HttpServletRequest request = (HttpServletRequest) super.pageContext.getRequest();
			final JspWriter out = super.pageContext.getOut();
			try {
				final String root = request.getContextPath();
				out.print(root);
				if (!root.endsWith("/")) {
					out.print("/");
				}
			} catch (IOException ex) {
				// SHould not happen but log as Error
				throw new JspException(ex);
			}
		}
		return TagSupport.SKIP_BODY;
	}
}
