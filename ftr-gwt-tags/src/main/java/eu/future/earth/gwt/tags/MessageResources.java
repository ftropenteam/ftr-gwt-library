/*
 * $Id: MessageResources.java 471754 2006-11-06 14:55:09Z husted $
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package eu.future.earth.gwt.tags;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * General purpose abstract class that describes an API for retrieving Locale-sensitive messages from underlying
 * resource locations of an unspecified design, and optionally utilizing the <code>MessageFormat</code> class to produce
 * internationalized messages with parametric replacement.
 * <p>
 * Calls to <code>getMessage()</code> variants without a <code>Locale</code> argument are presumed to be requesting a
 * message string in the default <code>Locale</code> for this JVM.
 * <p>
 * Calls to <code>getMessage()</code> with an unknown key, or an unknown <code>Locale</code> will return
 * <code>null</code> if the <code>returnNull</code> property is set to <code>true</code>. Otherwise, a suitable error
 * message will be returned instead.
 * <p>
 * <strong>IMPLEMENTATION NOTE</strong> - Classes that extend this class must be Object so that instances may be used in
 * distributable application server environments.
 * 
 * @version $Rev: 471754 $ $Date: 2005-08-29 23:57:50 -0400 (Mon, 29 Aug 2005) $
 */
public class MessageResources implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -694156596474286663L;

	// ------------------------------------------------------------- Properties

	private final static Logger log = Logger.getLogger(MessageResources.class.getName());

	// --------------------------------------------------------- Static Methods

	/**
	 * The default MessageResourcesFactory used to create MessageResources instances.
	 */
	protected static MessageResourcesFactory defaultFactory = null;

	/**
	 * The configuration parameter used to initialize this MessageResources.
	 */
	protected String config = null;

	/**
	 * The default Locale for our environment.
	 */
	protected Locale defaultLocale = Locale.getDefault();

	/**
	 * The <code>MessageResourcesFactory</code> that created this instance.
	 */
	protected MessageResourcesFactory factory = null;

	/**
	 * The set of previously created MessageFormat objects, keyed by the key computed in <code>messageKey()</code>.
	 */
	protected HashMap<String, MessageFormat> formats = new HashMap<String, MessageFormat>();

	/**
	 * Indicate is a <code>null</code> is returned instead of an error message string when an unknown Locale or key is
	 * requested.
	 */
	protected boolean returnNull = false;

	/**
	 * Indicates whether 'escape processing' should be performed on the error message string.
	 */
	private boolean escape = true;

	// ----------------------------------------------------------- Constructors

	/**
	 * Construct a new MessageResources according to the specified parameters.
	 * 
	 * @param factory
	 *            The MessageResourcesFactory that created us
	 * @param config
	 *            The configuration parameter for this MessageResources
	 */
	public MessageResources(MessageResourcesFactory factory, String config) {
		this(factory, config, false);
	}

	/**
	 * Construct a new MessageResources according to the specified parameters.
	 * 
	 * @param factory
	 *            The MessageResourcesFactory that created us
	 * @param config
	 *            The configuration parameter for this MessageResources
	 * @param returnNull
	 *            The returnNull property we should initialize with
	 */
	public MessageResources(MessageResourcesFactory factory, String config, boolean returnNull) {
		super();
		this.factory = factory;
		this.config = config;
		this.returnNull = returnNull;
	}

	/**
	 * The configuration parameter used to initialize this MessageResources.
	 * 
	 * @return parameter used to initialize this MessageResources
	 */
	public String getConfig() {
		return (this.config);
	}

	/**
	 * The <code>MessageResourcesFactory</code> that created this instance.
	 * 
	 * @return <code>MessageResourcesFactory</code> that created instance
	 */
	public MessageResourcesFactory getFactory() {
		return (this.factory);
	}

	/**
	 * Indicates that a <code>null</code> is returned instead of an error message string if an unknown Locale or key is
	 * requested.
	 * 
	 * @return true if null is returned if unknown key or locale is requested
	 */
	public boolean getReturnNull() {
		return (this.returnNull);
	}

	/**
	 * Indicates that a <code>null</code> is returned instead of an error message string if an unknown Locale or key is
	 * requested.
	 * 
	 * @param returnNull
	 *            true Indicates that a <code>null</code> is returned if an unknown Locale or key is requested.
	 */
	public void setReturnNull(boolean returnNull) {
		this.returnNull = returnNull;
	}

	/**
	 * Indicates whether 'escape processing' should be performed on the error message string.
	 * 
	 * @since Struts 1.2.8
	 */
	public boolean isEscape() {
		return escape;
	}

	/**
	 * Set whether 'escape processing' should be performed on the error message string.
	 * 
	 * @since Struts 1.2.8
	 */
	public void setEscape(boolean escape) {
		this.escape = escape;
	}

	// --------------------------------------------------------- Public Methods

	/**
	 * Returns a text message for the specified key, for the default Locale.
	 * 
	 * @param key
	 *            The message key to look up
	 */
	public String getMessage(String key) {
		return this.getMessage((Locale) null, key, null);
	}

	/**
	 * Returns a text message after parametric replacement of the specified parameter placeholders.
	 * 
	 * @param key
	 *            The message key to look up
	 * @param args
	 *            An array of replacement parameters for placeholders
	 */
	public String getMessage(String key, Object[] args) {
		return this.getMessage((Locale) null, key, args);
	}

	/**
	 * Returns a text message after parametric replacement of the specified parameter placeholders.
	 * 
	 * @param key
	 *            The message key to look up
	 * @param arg0
	 *            The replacement for placeholder {0} in the message
	 */
	public String getMessage(String key, Object arg0) {
		return this.getMessage((Locale) null, key, arg0);
	}

	/**
	 * Returns a text message after parametric replacement of the specified parameter placeholders.
	 * 
	 * @param key
	 *            The message key to look up
	 * @param arg0
	 *            The replacement for placeholder {0} in the message
	 * @param arg1
	 *            The replacement for placeholder {1} in the message
	 */
	public String getMessage(String key, Object arg0, Object arg1) {
		return this.getMessage((Locale) null, key, arg0, arg1);
	}

	/**
	 * Returns a text message after parametric replacement of the specified parameter placeholders.
	 * 
	 * @param key
	 *            The message key to look up
	 * @param arg0
	 *            The replacement for placeholder {0} in the message
	 * @param arg1
	 *            The replacement for placeholder {1} in the message
	 * @param arg2
	 *            The replacement for placeholder {2} in the message
	 */
	public String getMessage(String key, Object arg0, Object arg1, Object arg2) {
		return this.getMessage((Locale) null, key, arg0, arg1, arg2);
	}

	/**
	 * Returns a text message after parametric replacement of the specified parameter placeholders.
	 * 
	 * @param key
	 *            The message key to look up
	 * @param arg0
	 *            The replacement for placeholder {0} in the message
	 * @param arg1
	 *            The replacement for placeholder {1} in the message
	 * @param arg2
	 *            The replacement for placeholder {2} in the message
	 * @param arg3
	 *            The replacement for placeholder {3} in the message
	 */
	public String getMessage(String key, Object arg0, Object arg1, Object arg2, Object arg3) {
		return this.getMessage((Locale) null, key, arg0, arg1, arg2, arg3);
	}

	/**
	 * Returns a text message for the specified key, for the default Locale. A null string result will be returned by
	 * this method if no relevant message resource is found for this key or Locale, if the <code>returnNull</code>
	 * property is set. Otherwise, an appropriate error message will be returned.
	 * <p>
	 * This method must be implemented by a concrete subclass.
	 * 
	 * @param locale
	 *            The requested message Locale, or <code>null</code> for the system default Locale
	 * @param key
	 *            The message key to look up
	 */
	/**
	 * The set of locale keys for which we have already loaded messages, keyed by the value calculated in
	 * <code>localeKey()</code>.
	 */
	protected HashMap<String, String> locales = new HashMap<String, String>();

	/**
	 * The cache of messages we have accumulated over time, keyed by the value calculated in <code>messageKey()</code>.
	 */
	protected HashMap<String, String> messages = new HashMap<String, String>();

	// --------------------------------------------------------- Public Methods

	/**
	 * Returns a text message for the specified key, for the specified or default Locale. A null string result will be
	 * returned by this method if no relevant message resource is found for this key or Locale, if the
	 * <code>returnNull</code> property is set. Otherwise, an appropriate error message will be returned.
	 * <p>
	 * This method must be implemented by a concrete subclass.
	 * 
	 * @param locale
	 *            The requested message Locale, or <code>null</code> for the system default Locale
	 * @param key
	 *            The message key to look up
	 * @return text message for the specified key and locale
	 */
	public String getMessage(Locale locale, String key) {
		if (log.isLoggable(Level.FINE)) {
			log.log(Level.FINE, "getMessage(" + locale + "," + key + ")");
		}
		// Initialize variables we will require
		String localeKey = localeKey(locale);
		String originalKey = messageKey(localeKey, key);
		String message = null;
		// Search the specified Locale
		message = findMessage(locale, key, originalKey);
		if (message != null) {
			return message;
		}

		if (!defaultLocale.equals(locale)) {
			localeKey = localeKey(defaultLocale);
			message = findMessage(localeKey, key, originalKey);
		}

		if (message != null) {
			return message;
		}

		// Find the message in the default properties file
		message = findMessage("", key, originalKey);
		if (message != null) {
			return message;
		}

		// Return an appropriate error indication
		if (returnNull) {
			return (null);
		} else {
			return ("???" + messageKey(locale, key) + "???");
		}
	}

	// ------------------------------------------------------ Protected Methods

	/**
	 * Load the messages associated with the specified Locale key. For this implementation, the <code>config</code>
	 * property should contain a fully qualified package and resource name, separated by periods, of a series of
	 * property resources to be loaded from the class loader that created this PropertyMessageResources instance. This
	 * is exactly the same name format you would use when utilizing the <code>java.util.PropertyResourceBundle</code>
	 * class.
	 * 
	 * @param localeKey
	 *            Locale key for the messages to be retrieved
	 */
	protected synchronized void loadLocale(String localeKey) {

		// Have we already attempted to load messages for this locale?
		if (locales.get(localeKey) != null) {
			return;
		}

		locales.put(localeKey, localeKey);

		// Set up to load the property resource for this locale key, if we can
		String name = config.replace('.', '/');

		if (localeKey.length() > 0) {
			name += ("_" + localeKey);
		}

		name += ".properties";

		InputStream is = null;
		Properties props = new Properties();

		// Load the specified property resource

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		if (classLoader == null) {
			classLoader = this.getClass().getClassLoader();
		}

		is = classLoader.getResourceAsStream(name);

		if (is != null) {
			try {
				Reader reader = new InputStreamReader(is, "UTF-8");
				props.load(reader);
			} catch (IOException e) {

				log.log(Level.SEVERE, "loadLocale()", e);
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					log.log(Level.SEVERE, "loadLocale()", e);
				}
			}

		}

		// Copy the corresponding values into our cache
		if (props.size() < 1) {
			return;
		}

		synchronized (messages) {
			Iterator<Object> names = props.keySet().iterator();
			while (names.hasNext()) {
				String key = (String) names.next();
				messages.put(messageKey(localeKey, key), props.getProperty(key));
			}
		}
	}

	// -------------------------------------------------------- Private Methods

	/**
	 * Returns a text message for the specified key, for the specified Locale.
	 * <p>
	 * A null string result will be returned by this method if no relevant message resource is found. This method
	 * searches through the locale <i>hierarchy</i> (i.e. variant --> languge --> country) for the message.
	 * 
	 * @param locale
	 *            The requested message Locale, or <code>null</code> for the system default Locale
	 * @param key
	 *            The message key to look up
	 * @param originalKey
	 *            The original message key to cache any found message under
	 * @return text message for the specified key and locale
	 */
	private String findMessage(Locale locale, String key, String originalKey) {
		// Initialize variables we will require
		String localeKey = localeKey(locale);
		String message = null;
		int underscore = 0;

		// Loop from specific to general Locales looking for this message
		while (true) {
			message = findMessage(localeKey, key, originalKey);
			if (message != null) {
				break;
			}

			// Strip trailing modifiers to try a more general locale key
			underscore = localeKey.lastIndexOf("_");

			if (underscore < 0) {
				break;
			}

			localeKey = localeKey.substring(0, underscore);
		}

		return message;

	}

	/**
	 * Returns a text message for the specified key, for the specified Locale.
	 * <p>
	 * A null string result will be returned by this method if no relevant message resource is found.
	 * 
	 * @param locale
	 *            The requested key of the Locale
	 * @param key
	 *            The message key to look up
	 * @param originalKey
	 *            The original message key to cache any found message under
	 * @return text message for the specified key and locale
	 */
	private String findMessage(String localeKey, String key, String originalKey) {
		// Load this Locale's messages if we have not done so yet
		loadLocale(localeKey);

		// Check if we have this key for the current locale key
		String messageKey = messageKey(localeKey, key);

		// Add if not found under the original key
		boolean addIt = !messageKey.equals(originalKey);

		synchronized (messages) {
			String message = (String) messages.get(messageKey);

			if (message != null) {
				if (addIt) {
					messages.put(originalKey, message);
				}

			}
			return (message);
		}
	}

	/**
	 * Returns a text message after parametric replacement of the specified parameter placeholders. A null string result
	 * will be returned by this method if no resource bundle has been configured.
	 * 
	 * @param locale
	 *            The requested message Locale, or <code>null</code> for the system default Locale
	 * @param key
	 *            The message key to look up
	 * @param args
	 *            An array of replacement parameters for placeholders
	 */
	public String getMessage(Locale locale, String key, Object[] args) {
		// Cache MessageFormat instances as they are accessed
		if (locale == null) {
			locale = defaultLocale;
		}
		MessageFormat format = null;
		String formatKey = messageKey(locale, key);
		synchronized (formats) {
			format = (MessageFormat) formats.get(formatKey);
			if (format == null) {
				String formatString = getMessage(locale, key);
				if (formatString == null) {
					return returnNull ? null : ("???" + formatKey + "???");
				}

				format = new MessageFormat(escape(formatString));
				format.setLocale(locale);
				formats.put(formatKey, format);
			}
		}
		return format.format(args);
	}

	/**
	 * Returns a text message after parametric replacement of the specified parameter placeholders. A null string result
	 * will never be returned by this method.
	 * 
	 * @param locale
	 *            The requested message Locale, or <code>null</code> for the system default Locale
	 * @param key
	 *            The message key to look up
	 * @param arg0
	 *            The replacement for placeholder {0} in the message
	 */
	public String getMessage(Locale locale, String key, Object arg0) {
		return this.getMessage(locale, key, new Object[] {
			arg0
		});
	}

	/**
	 * Returns a text message after parametric replacement of the specified parameter placeholders. A null string result
	 * will never be returned by this method.
	 * 
	 * @param locale
	 *            The requested message Locale, or <code>null</code> for the system default Locale
	 * @param key
	 *            The message key to look up
	 * @param arg0
	 *            The replacement for placeholder {0} in the message
	 * @param arg1
	 *            The replacement for placeholder {1} in the message
	 */
	public String getMessage(Locale locale, String key, Object arg0, Object arg1) {
		return this.getMessage(locale, key, new Object[] {
				arg0, arg1
		});
	}

	/**
	 * Returns a text message after parametric replacement of the specified parameter placeholders. A null string result
	 * will never be returned by this method.
	 * 
	 * @param locale
	 *            The requested message Locale, or <code>null</code> for the system default Locale
	 * @param key
	 *            The message key to look up
	 * @param arg0
	 *            The replacement for placeholder {0} in the message
	 * @param arg1
	 *            The replacement for placeholder {1} in the message
	 * @param arg2
	 *            The replacement for placeholder {2} in the message
	 */
	public String getMessage(Locale locale, String key, Object arg0, Object arg1, Object arg2) {
		return this.getMessage(locale, key, new Object[] {
				arg0, arg1, arg2
		});
	}

	/**
	 * Returns a text message after parametric replacement of the specified parameter placeholders. A null string result
	 * will never be returned by this method.
	 * 
	 * @param locale
	 *            The requested message Locale, or <code>null</code> for the system default Locale
	 * @param key
	 *            The message key to look up
	 * @param arg0
	 *            The replacement for placeholder {0} in the message
	 * @param arg1
	 *            The replacement for placeholder {1} in the message
	 * @param arg2
	 *            The replacement for placeholder {2} in the message
	 * @param arg3
	 *            The replacement for placeholder {3} in the message
	 */
	public String getMessage(Locale locale, String key, Object arg0, Object arg1, Object arg2, Object arg3) {
		return this.getMessage(locale, key, new Object[] {
				arg0, arg1, arg2, arg3
		});
	}

	/**
	 * Return <code>true</code> if there is a defined message for the specified key in the system default locale.
	 * 
	 * @param key
	 *            The message key to look up
	 */
	public boolean isPresent(String key) {
		return this.isPresent(null, key);
	}

	/**
	 * Return <code>true</code> if there is a defined message for the specified key in the specified Locale.
	 * 
	 * @param locale
	 *            The requested message Locale, or <code>null</code> for the system default Locale
	 * @param key
	 *            The message key to look up
	 */
	public boolean isPresent(Locale locale, String key) {
		String message = getMessage(locale, key);

		if (message == null) {
			return false;
		} else if (message.startsWith("???") && message.endsWith("???")) {
			return false;
		} else {
			return true;
		}
	}

	// ------------------------------------------------------ Protected Methods

	/**
	 * Escape any single quote characters that are included in the specified message string.
	 * 
	 * @param string
	 *            The string to be escaped
	 */
	protected String escape(String string) {
		if (!isEscape()) {
			return string;
		}

		if ((string == null) || (string.indexOf('\'') < 0)) {
			return string;
		}

		int n = string.length();
		StringBuffer sb = new StringBuffer(n);

		for (int i = 0; i < n; i++) {
			char ch = string.charAt(i);

			if (ch == '\'') {
				sb.append('\'');
			}

			sb.append(ch);
		}

		return sb.toString();
	}

	/**
	 * Compute and return a key to be used in caching information by a Locale. <strong>NOTE</strong> - The locale key
	 * for the default Locale in our environment is a zero length String.
	 * 
	 * @param locale
	 *            The locale for which a key is desired
	 */
	protected String localeKey(Locale locale) {
		return (locale == null) ? "" : locale.toString();
	}

	/**
	 * Compute and return a key to be used in caching information by Locale and message key.
	 * 
	 * @param locale
	 *            The Locale for which this format key is calculated
	 * @param key
	 *            The message key for which this format key is calculated
	 */
	protected String messageKey(Locale locale, String key) {
		return (localeKey(locale) + "." + key);
	}

	/**
	 * Compute and return a key to be used in caching information by locale key and message key.
	 * 
	 * @param localeKey
	 *            The locale key for which this cache key is calculated
	 * @param key
	 *            The message key for which this cache key is calculated
	 */
	protected String messageKey(String localeKey, String key) {
		return (localeKey + "." + key);
	}

	/**
	 * Create and return an instance of <code>MessageResources</code> for the created by the default
	 * <code>MessageResourcesFactory</code>.
	 * 
	 * @param config
	 *            Configuration parameter for this message bundle.
	 */
	public synchronized static MessageResources getMessageResources(String config) {
		if (defaultFactory == null) {
			defaultFactory = MessageResourcesFactory.createFactory();
		}

		return defaultFactory.createResources(config);
	}

}
