/*
 * $Id: TagUtils.java 471754 2006-11-06 14:55:09Z husted $
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package eu.future.earth.gwt.tags;

import java.io.IOException;
import java.util.Locale;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.JspWriter;
import jakarta.servlet.jsp.PageContext;

/**
 * Provides helper methods for JSP tags.
 * 
 * @version $Rev: 471754 $
 * @since Struts 1.2
 */
public class TagUtils {
	/**
	 * The Singleton instance.
	 * 
	 * @since 1.3.5 Changed to non-final so it may be overridden, use at your own risk (you've been warned!!)
	 */
	private static TagUtils instance = new TagUtils();

	/**
	 * The message resources for this package. 
	 */
	private static final MessageResources messages = MessageResources.getMessageResources("eu.future.earth.gwt.tags.LocalStrings");



	/**
	 * Constructor for TagUtils.
	 */
	protected TagUtils() {
		super();
	}

	/**
	 * Returns the Singleton instance of TagUtils.
	 */
	public static TagUtils getInstance() {
		return instance;
	}

	/**
	 * Set the instance. This blatently violates the Singleton pattern, but then some say Singletons are an anti-pattern.
	 * 
	 * @since 1.3.5 Changed to non-final and added setInstance() so TagUtils may be overridden, use at your own risk (you've been warned!!)
	 * @param instance
	 *            The instance to set.
	 */
	public static void setInstance(TagUtils instance) {
		TagUtils.instance = instance;
	}

	/**
	 * URLencodes a string assuming the character encoding is UTF-8.
	 * 
	 * @param url
	 * @return String The encoded url in UTF-8
	 */
	public String encodeURL(String url) {
		return encodeURL(url, "UTF-8");
	}

	/**
	 * Use the new URLEncoder.encode() method from Java 1.4 if available, else use the old deprecated version. This method uses reflection to find the appropriate method; if the reflection operations throw exceptions, this will return the url encoded with the old URLEncoder.encode() method.
	 * 
	 * @param enc
	 *            The character encoding the urlencode is performed on.
	 * @return String The encoded url.
	 */
	public String encodeURL(String url, String enc) {
		return ResponseUtils.encodeURL(url, enc);
	}

	/**
	 * Look up and return current user locale, based on the specified parameters.
	 * 
	 * @param pageContext
	 *            The PageContext associated with this request
	 * @param locale
	 *            Name of the session attribute for our user's Locale. If this is <code>null</code>, the default locale key is used for the lookup.
	 * @return current user locale
	 */
	public Locale getUserLocale(PageContext pageContext, String locale) {
		return RequestUtils.getUserLocale((HttpServletRequest) pageContext.getRequest(), locale);
	}

	/**
	 * Locate and return the specified bean, from an optionally specified scope, in the specified page context. If no such bean is found, return <code>null</code> instead. If an exception is thrown, it will have already been saved via a call to <code>saveException()</code>.
	 * 
	 * @param pageContext
	 *            Page context to be searched
	 * @param name
	 *            Name of the bean to be retrieved
	 * @param scopeName
	 *            Scope to be searched (page, request, session, application) or <code>null</code> to use <code>findAttribute()</code> instead
	 * @return JavaBean in the specified page context
	 * @throws JspException
	 *             if an invalid scope name is requested
	 */
	public Object lookup(PageContext pageContext, String name, String scopeName) throws JspException {
		return pageContext.findAttribute(name);
	}

	/**
	 * Look up and return a message string, based on the specified parameters.
	 * 
	 * @param pageContext
	 *            The PageContext associated with this request
	 * @param bundle
	 *            Name of the servlet context attribute for our message resources bundle
	 * @param locale
	 *            Name of the session attribute for our user's Locale
	 * @param key
	 *            Message key to be looked up and returned
	 * @return message string
	 * @throws JspException
	 *             if a lookup error occurs (will have been saved in the request already)
	 */
	public String message(PageContext pageContext, String locale, String key) throws JspException {
		return message(pageContext, locale, key);
	}

	/**
	 * Look up and return a message string, based on the specified parameters.
	 * 
	 * @param pageContext
	 *            The PageContext associated with this request
	 * @param bundle
	 *            Name of the servlet context attribute for our message resources bundle
	 * @param locale
	 *            Name of the session attribute for our user's Locale
	 * @param key
	 *            Message key to be looked up and returned
	 * @param args
	 *            Replacement parameters for this message
	 * @return message string
	 * @throws JspException
	 *             if a lookup error occurs (will have been saved in the request already)
	 */
	public String message(PageContext pageContext, String locale, String key, Object[] args) throws JspException {
		MessageResources resources = retrieveMessageResources(pageContext);

		Locale userLocale = getUserLocale(pageContext, locale);
		String message = null;
		if (args == null) {
			message = resources.getMessage(userLocale, key);
		} else {
			message = resources.getMessage(userLocale, key, args);
		}
		return message;
	}

	/**
	 * Returns the appropriate MessageResources object for the current module and the given bundle.
	 * 
	 * @param pageContext
	 *            Search the context's scopes for the resources.
	 * @param bundle
	 *            The bundle name to look for. If this is <code>null</code>, the default bundle name is used.
	 * @param checkPageScope
	 *            Whether to check page scope
	 * @return MessageResources The bundle's resources stored in some scope.
	 * @throws JspException
	 *             if the MessageResources object could not be found.
	 */
	public MessageResources retrieveMessageResources(PageContext pageContext) throws JspException {
		MessageResources resources = null;

		// if (checkPageScope) {
		// resources = (MessageResources) pageContext.getAttribute(bundle, PageContext.PAGE_SCOPE);
		// }
		//
		if (resources == null) {
			resources = (MessageResources) pageContext.getAttribute(Globals.MESSAGES_KEY, PageContext.APPLICATION_SCOPE);
		}

		if (resources == null) {
			resources = MessageResourcesFactory.createFactory().createResources("GwtResources");
			pageContext.setAttribute(Globals.MESSAGES_KEY, resources, PageContext.APPLICATION_SCOPE);
		}

		if (resources == null) {
			JspException e = new JspException(messages.getMessage("message.bundle", Globals.MESSAGES_KEY));
			throw e;
		}

		return resources;
	}

	/**
	 * Write the specified text as the response to the writer associated with this page. <strong>WARNING</strong> - If you are writing body content from the <code>doAfterBody()</code> method of a custom tag class that implements <code>BodyTag</code>, you should be calling <code>writePrevious()</code> instead.
	 * 
	 * @param pageContext
	 *            The PageContext object for this page
	 * @param text
	 *            The text to be written
	 * @throws JspException
	 *             if an input/output error occurs (already saved)
	 */
	public void write(PageContext pageContext, String text) throws JspException {
		JspWriter writer = pageContext.getOut();
		
		try {
			writer.print(text);
		} catch (IOException e) {
			throw new JspException(messages.getMessage("write.io", e.toString()));
		}
	}

}
