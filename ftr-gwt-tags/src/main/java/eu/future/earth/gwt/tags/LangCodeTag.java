package eu.future.earth.gwt.tags;

import java.util.ArrayList;
import java.util.Locale;
import java.util.StringTokenizer;

import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.tagext.BodyTagSupport;

public class LangCodeTag extends BodyTagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -581799554702623746L;

	public LangCodeTag() {
		super();
	}

	public int doStartTag() throws JspException {
		final TagUtils utils = TagUtils.getInstance();
		final Locale locale = utils.getUserLocale(this.pageContext, null);
		utils.write(this.pageContext, getLanguageCode(locale));
		return SKIP_BODY;
	}

	public String getLanguageCode(Locale loc) {
		if (loc != null) {
			String lanCode = loc.getLanguage();
			if (lanCode.equals(defaultLocale)) {
				return lanCode;
			} else {
				if (locales != null && locales.length > 0) {
					for (int i = 0; i < locales.length; i++) {
						if (locales[i].equals(lanCode)) {
							return lanCode;
						}
					}
				}
				return defaultLocale;
			}
		} else {
			return defaultLocale;
		}

	}

	private String defaultLocale = "en";

	private String[] locales = null;

	public void setLocales(String newRoles) {
		this.locales = analyzeRoles(newRoles);
	}

	/**
	 * This method parses a String seperatod by , in to an array.
	 * 
	 * @param input
	 *            String - The input
	 * @return String[] - The Roles.
	 */
	public static String[] analyzeRoles(String input) {
		final ArrayList<String> found = new ArrayList<String>();
		final StringTokenizer tokens = new StringTokenizer(input, ",");
		while (tokens.hasMoreTokens()) {
			final String tok = (String) tokens.nextToken();
			found.add(tok.trim());
		}
		return (String[]) found.toArray(new String[0]);
	}

	public void setDefaultLocale(String defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

}
