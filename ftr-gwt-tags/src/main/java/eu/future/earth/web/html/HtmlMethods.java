package eu.future.earth.web.html;

import java.io.IOException;

import jakarta.servlet.jsp.JspWriter;

/***
 * <p>
 * Title: Future Earth Web Library
 * </p>
 * <p>
 * Description: This Util class contains methods for crating html code.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Future Earth
 * </p>
 * 
 * @author Marteijn Nouwens
 */
public class HtmlMethods {

	public static final String LAST_DIR = "/";

	public static final String IDENT = "      ";

	/**
	 * The Indentifier for the seperator in image for rollover image generation _
	 */
	public static final String SIZE_SEP = "_";

	/**
	 * Just the dot
	 */
	public static final String DOT = ".";

	/**
	 * sitemap_u_122_18.gif <b>u</b>
	 */
	public static final String NORMAL = "u";

	/**
	 * sitemap_a_122_18.gif <b>a</b>
	 */
	public static final String OVER = "a";

	private HtmlMethods() {
		super();
	}

	/**
	 * Creates an name value par for html on the outputstream.
	 * 
	 * @param out
	 *            JspWriter - the JspWriter
	 * @param name
	 *            String - The name of the pair.
	 * @param value
	 *            String- The Value
	 * @throws IOException
	 *             - Thrown when an Error Occurs during writing.
	 */
	public static void writePair(JspWriter out, String name, String value) throws IOException {
		out.print(" " + name + "=\"" + value + "\" ");
	}

	/**
	 * Creates an name value par for html on the StringBuffer. Print's a space in case it is needed.
	 * 
	 * @param out
	 *            StringBuffer - the StringBuffer
	 * @param name
	 *            String - The name of the pair.
	 * @param value
	 *            String- The Value
	 */
	public static void writePair(StringBuffer out, String name, String value) {
		if (!(out.length() > 0 && out.charAt(out.length() - 1) == ' ')) {
			out.append(" ");
		}
		out.append(name);
		out.append("=\"");
		out.append(value);
		out.append("\"");
	}

	/**
	 * Return's the Class for the Row number.
	 * 
	 * @param i
	 *            int - The Row Number
	 * @return String - <b>RowEven</b> if the number is even or <b>RowUneven</b> when te number is uneven.
	 */
	public static String getRowColor(int i) {
		return rowColor(i, "RowEven", "RowUnEven ");
	}

	/**
	 * This funtion return the correct Class or Color code.
	 * 
	 * @param number
	 *            int - The Row number.
	 * @param even
	 *            String - The string for even rows.
	 * @param uneven
	 *            String - The String for uneven rows.
	 * @return String - The correct string. One of the two supplied.
	 */
	public static String rowColor(int number, String even, String uneven) {
		if (even(number)) {
			return even;
		}
		return uneven;
	}

	/**
	 * Method Check's if the int Value is even or uneven.
	 * 
	 * @param number
	 *            - the integer to check.
	 * @return boolean - true if even false when uneven
	 */
	public static boolean even(int number) {
		boolean result = false;
		if ((number & 1) != 1) {
			result = true;
		}
		return result;
	}

	/**
	 * THis create an image html code.
	 * 
	 * @param imageUrl
	 *            String - The Image url.
	 * @return String - The Img html code.
	 */
	public static String makeImage(String imageUrl) {
		int from = imageUrl.lastIndexOf(LAST_DIR);
		if (from < 0) {
			from = 0;
		}
		final int first = imageUrl.indexOf(SIZE_SEP, from);
		final int sec = imageUrl.indexOf(SIZE_SEP, first + 1);
		final int third = imageUrl.indexOf(SIZE_SEP, sec + 1);
		final int dot = imageUrl.lastIndexOf(DOT);

		final StringBuffer result = new StringBuffer();
		result.append(" src=\"");
		result.append(imageUrl);
		result.append("\" ");
		if (first > 0 && sec > 0 && third > 0 && dot > 0) {
			final String wide = imageUrl.substring(sec + 1, third);
			final String height = imageUrl.substring(third + 1, dot);
			writePair(result, HtmlTags.HEIGHT, height);
			writePair(result, HtmlTags.WIDTH, wide);

		} else {
			final int stheight = imageUrl.lastIndexOf(SIZE_SEP);
			if (stheight > 0) {
				final String heightString = imageUrl.substring(stheight + 1, dot);
				writePair(result, HtmlTags.HEIGHT, heightString);
				final int stwide = imageUrl.lastIndexOf(SIZE_SEP, first);
				if (stwide > 0) {
					final String wideString = imageUrl.substring(stwide + 1, stheight);
					try {
						Integer.parseInt(wideString);
						writePair(result, HtmlTags.WIDTH, wideString);
					} catch (NumberFormatException ex) {
						writePair(result, HtmlTags.WIDTH, heightString);
					}
				} else {
					writePair(result, HtmlTags.WIDTH, heightString);
				}

			}
		}
		writePair(result, HtmlTags.BORDER, "0");
		return result.toString();
	}

	/**
	 * Create image for an url.
	 * 
	 * @param imageUrl
	 *            String - The Image url.
	 * @return String - The Img html code.
	 */
	public static String createImage(String imageUrl) {
		return createImage(imageUrl, null);
	}

	/**
	 * Creates a clickable image.
	 * 
	 * @param imageUrl
	 *            String - The Image url.
	 * @param url
	 *            - The value of href.
	 * @return String - The Img html code.
	 */
	public static String createImage(String imageUrl, String url) {
		return createImage(imageUrl, url, null);
	}

	/**
	 * Create an Image over call for the Future Earth js Library and Standards
	 * 
	 * @param imageUrl
	 *            - the image url formatted as follewed sitemap_u_122_18.gif <b>a</b> where u is the normal version and
	 *            the a is the imge to be used for the rol over.
	 * @param url
	 *            - the Url when clicked, if null it will be left out.
	 * @param altText
	 *            - optional alt text
	 * @return - the Scrting include the Utils.js in your page
	 */
	public static String createImage(String imageUrl, String url, String altText) {
		return createImage(imageUrl, url, altText, null);
	}

	/**
	 * Create an Image over call for the Future Earth js Library and Standards
	 * 
	 * @param imageUrl
	 *            - the image url formatted as follewed sitemap_u_122_18.gif <b>a</b> where u is the normal version and
	 *            the a is the imge to be used for the rol over.
	 * @param url
	 *            - the Url when clicked, if null it will be left out.
	 * @param altText
	 *            - optional alt text - May be null.
	 * @param imageUrlOver
	 *            - The url for the image over. If not supplied it will be left out.
	 * @return - the Scrting include the Utils.js in your page
	 */
	public static String createImage(String imageUrl, String url, String altText, String imageUrlOver) {
		if (imageUrl == null) {
			return "";
		}
		final StringBuffer result = new StringBuffer();
		int from = imageUrl.lastIndexOf(LAST_DIR);
		if (from < 0) {
			from = 0;
		}

		final int first = imageUrl.indexOf(SIZE_SEP, from);
		final int sec = imageUrl.indexOf(SIZE_SEP, first + 1);
		final int third = imageUrl.indexOf(SIZE_SEP, sec + 1);
		final int dot = imageUrl.lastIndexOf(DOT);

		if (url != null) {
			result.append("<a href=\"");
			result.append(url);
			result.append("\">");
		}

		result.append("<img src=\"");
		result.append(imageUrl);
		result.append("\" ");
		if (first > 0 && sec > 0 && third > 0 && dot > 0 && imageUrlOver == null) {
			final String wide = imageUrl.substring(sec + 1, third);
			final String height = imageUrl.substring(third + 1, dot);
			writePair(result, HtmlTags.HEIGHT, height);
			writePair(result, HtmlTags.WIDTH, wide);

			final String urlImageOver = imageUrl.substring(0, first) + SIZE_SEP + OVER + imageUrl.substring(sec, imageUrl.length());
			result.append(" onMouseOver=\"return setImage(this,'");
			result.append(urlImageOver);
			result.append("')\" onMouseOut=\"return setImage(this,'");
			result.append(imageUrl);
			result.append("')\"");

		} else {
			final int stheight = imageUrl.lastIndexOf(SIZE_SEP);
			if (stheight > 0) {
				final String heightString = imageUrl.substring(stheight + 1, dot);
				writePair(result, HtmlTags.HEIGHT, heightString);
				final int stwide = imageUrl.lastIndexOf(SIZE_SEP, first);
				if (stwide > 0) {
					final String wideString = imageUrl.substring(stwide + 1, stheight);
					try {
						Integer.parseInt(wideString);
						writePair(result, HtmlTags.WIDTH, wideString);
					} catch (NumberFormatException ex) {
						writePair(result, HtmlTags.WIDTH, heightString);
					}
				} else {
					writePair(result, HtmlTags.WIDTH, heightString);
				}

			}
			if (imageUrlOver != null) {
				result.append(" onMouseOver=\"return setImage(this,'");
				result.append(imageUrlOver);
				result.append("')\" onMouseOut=\"return setImage(this,'");
				result.append(imageUrl);
				result.append("')\"");

			}
		}
		if (altText != null) {
			HtmlMethods.writePair(result, "alt", altText);
		}
		result.append(" border=\"0\" />");
		if (url != null) {
			result.append("</a>");
		}
		return result.toString();
	}

}
