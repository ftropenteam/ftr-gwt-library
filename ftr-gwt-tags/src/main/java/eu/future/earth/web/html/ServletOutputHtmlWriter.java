package eu.future.earth.web.html;

import jakarta.servlet.ServletOutputStream;

public class ServletOutputHtmlWriter extends HtmlWriter {

	public ServletOutputHtmlWriter(ServletOutputStream newOut) {
		super(new ServletOutputWriter(newOut));
	}

}
