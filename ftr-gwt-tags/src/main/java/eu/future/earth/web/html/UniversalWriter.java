package eu.future.earth.web.html;

public interface UniversalWriter {

	void write(String data);

}
