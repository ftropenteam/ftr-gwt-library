package eu.future.earth.web.html;

/**
 * <p>
 * Title: Future Earth Web Library
 * </p>
 * <p>
 * Description: This Interface defines all the Html Tags and Attributes for the
 * Html Tags.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Future Earth
 * </p>
 * 
 * @author Marteijn Nouwens
 */
public interface HtmlTags {

	/**
	 * This is the href Attribute.
	 */
	String HREF = "href";

	/**
	 * This is the Src Attribute.
	 */
	String SRC = "src";

	/**
	 * The align Attribute.
	 */
	String ALIGN = "align";

	/**
	 * The Vertical align Attribute.
	 */
	String VALIGN = "valign";

	/**
	 * The alt Attribute.
	 */
	String ALT = "alt";

	/**
	 * This is the nowrap Attribute.
	 */
	String NOWRAP = "nowrap";

	/**
	 * This is the Class Attribute.
	 */
	String CLASS = "class";

	/**
	 * The Style Attribute.
	 */
	String STYLE = "style";

	/**
	 * This is the Height Attribute.
	 */
	String HEIGHT = "height";

	/**
	 * This is the Width Attribute.
	 */
	String WIDTH = "width";

	/**
	 * This is the Border Attribute.
	 */
	String BORDER = "border";

	/**
	 * THis is the Cell Spacing Attribute.
	 */
	String CELLSPACING = "cellspacing";

	/**
	 * THis is the Cell Padding Attribute.
	 */
	String CELLPADDING = "cellpadding";

	/**
	 * The Name of an object.
	 */
	String NAME = "name";

	/**
	 * The Type of an object.
	 */
	String TYPE = "type";

	/**
	 * The Value of and input type.
	 */
	String VALUE = "value";

	/**
	 * The Indicatur of a input type is selected.
	 */
	String SELECTED = "selected";

	/**
	 * If the Checkbox is selected.
	 */
	String CHECKED = "checked";

	/**
	 * The Id of an Object.
	 */
	String ID = "id";

	/**
	 * This is the Input name.
	 */
	String INPUT = "input";

	/**
	 * The Html Tag.
	 */
	String HTML = "html";

	/**
	 * The Body Tag.
	 */
	String BODY = "body";

	/**
	 * The Table Tag.
	 */
	String TABLE = "table";

	/**
	 * The Table Body Tag.
	 */
	String TBODY = "tbody";

	/**
	 * The Form Tag.
	 */
	String FORM = "form";

	/**
	 * The Action for an form
	 */
	String ACTION = "action";

	/**
	 * The Method for the Form. Can be Post or Get.
	 */
	String METHOD = "method";

	/**
	 * Can be used to call an Javascript method.
	 */
	String ON_SUBMIT = "onsubmit";

	/**
	 * The DIV tag.
	 */
	String DIV = "div";

	/**
	 * The Img Tag.
	 */
	String IMG = "img";

	/**
	 * The a Tag.
	 */
	String A = "a";

	/**
	 * The Tr Tag.
	 */
	String TR = "tr";

	/**
	 * The frameset.
	 */
	String FRAME_SET = "frameset";

	/**
	 * The Rows Attribute
	 */
	String ROWS = "rows";

	/**
	 * The Scrolling
	 */
	String SCROLLING = "scrolling";

	/**
	 * Columns attribute
	 */
	String COLS = "cols";

	/**
	 * The Frame spacing
	 */
	String FRAME_SPACING = "framespacing";

	/**
	 * Frame border
	 */
	String FRAME_BORDER = "frameborder";

	/**
	 * The Frame components.
	 */
	String FRAME = "frame";

	/**
	 * No Frames tag.
	 */
	String NO_FRAMES = "noframes";

	/**
	 * The Td Tag.
	 */
	String TD = "td";

	/**
	 * The Head Tag
	 */
	String HEAD = "head";

	/**
	 * The Title Tag
	 */
	String TITLE = "title";

	/**
	 * The Link Tag
	 */
	String LINK = "link";

	/**
	 * The Rel Attribute.
	 */
	String REL = "rel";

	/**
	 * Target Tag.
	 */
	String TARGET = "target";

	/**
	 * The Th Tag.
	 */
	String TH = "th";

	/**
	 * This is the span html Tag.
	 */
	String SPAN = "span";

	/**
	 * The Colspan for Td elements.
	 */
	String COL_SPAN = "colspan";

	/**
	 * The Row Span for Tr Elements.
	 */
	String ROW_SPAN = "rowspan";

	/**
	 * This is a new Line Html Tag.
	 */
	String NEW_LINE = "<br />";

	/**
	 * This is the space tag for Html.
	 */
	String NBSP = "&nbsp;";

	/**
	 * This is the Url Dir Seperator.
	 */
	String DIR_SEP = "/";

}
