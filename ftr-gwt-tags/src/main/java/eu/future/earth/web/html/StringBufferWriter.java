package eu.future.earth.web.html;

public class StringBufferWriter implements UniversalWriter {

	private StringBuffer buffer = null;

	public StringBufferWriter(StringBuffer newBuffer) {
		super();
		buffer = newBuffer;
	}

	public void write(String data) {
		buffer.append(data);
	}
}
