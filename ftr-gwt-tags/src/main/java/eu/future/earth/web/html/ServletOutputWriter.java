package eu.future.earth.web.html;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.servlet.ServletOutputStream;

/**
 * <p>
 * Title: The Servlet writer that Can be used.
 * </p>
 * <p>
 * Description: The Servlet Writer.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Future Earth
 * </p>
 * 
 * @author Marteijn Nouwens
 */
public class ServletOutputWriter implements UniversalWriter {

	private static final Logger LOGGER = Logger.getLogger("Jsp");

	private ServletOutputStream out = null;

	public ServletOutputWriter(ServletOutputStream newOut) {
		super();
		out = newOut;
	}

	public void write(String data) {
		try {
			out.print(data);
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, "", ex);
		}
	}

	//

}
