package eu.future.earth.web.html;

import java.util.ArrayList;

/**
 * <p>
 * Title: Future Earth Web Library
 * </p>
 * <p>
 * Description: |
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Future Earth
 * </p>
 * 
 * @author Marteijn Nouwens
 */
public class HtmlWriter {

	protected static final String QUOTE = "\"";
	protected static final String INDENT = "  ";
	protected static final String END = "/";
	protected static final String OP = "<";
	protected static final String CL = ">";
	protected static final String NL = "\n";

	private ArrayList<String> tree = new ArrayList<String>();
	private int identCount = -1;
	private boolean open = false;
	private UniversalWriter buffer = null;

	public HtmlWriter(UniversalWriter newStringBuffer) {
		super();
		buffer = newStringBuffer;
	}

	private void writeIdented(String data) {
		for (int i = 0; i < identCount; i++) {
			writeDirect(INDENT);
		}
		writeDirect(data);
	}

	private void writeDirect(String data) {
		if (data != null) {
			buffer.write(data);
		}
	}

	public void openTag(String newTag) {
		if (newTag == null) {
			return;
		}
		if (open) {
			writeDirect(CL);
		}

		identAndNewLine();

		writeIdented(OP + newTag);
		tree.add(newTag);
		open = true;
	}

	public void emptyElement(String newTag) {
		openTag(newTag);
		closeEmptyTag();
	}

	private void identAndNewLine() {
		writeDirect(NL);
		identCount++;
	}

	public void space() {
		if (open) {
			writeDirect(CL);
			open = false;
		}
		writeDirect(HtmlTags.NBSP);
	}

	public void newLine() {
		if (open) {
			writeDirect(CL);
			open = false;
		}
		writeDirect(HtmlTags.NEW_LINE);
	}

	public void writeText(String newText) {
		if (open) {
			writeDirect(CL);
			open = false;
		}
		if (!HtmlTags.A.equals(getCurrentTag())) {
			writeDirect(NL);
			writeIdented(newText);
		} else {
			writeDirect(newText);
		}

	}

	public void writePair(String tagname, String newText) {
		if (open) { // Only allowed when the ta is open.
			writeDirect(" " + tagname + "=\"" + newText + "\"");
		}
	}

	public void writeSingle(String tagname) {
		if (open) { // Only allowed when the ta is open.
			writeDirect(" " + tagname);
		}
	}

	public void writePair(String tagname, int newText) {
		if (open) { // Only allowed when the tag is open.
			writeDirect(" " + tagname + "=\"" + newText + "\"");
		}
	}

	public void writeImage(String imageUrl) {
		writeImage(imageUrl, true);
	}

	/**
	 * Write image info without any sizing info
	 * 
	 * @param imageUrl
	 *            String - The iamge url
	 * @param close
	 *            boolean - The indicatier if to close.
	 */
	public void writeImage(String imageUrl, boolean close) {
		writeSizedImage(imageUrl, close, false);
	}

	/**
	 * In this case the image has to contain size information
	 * 
	 * @param imageUrl
	 *            String - THe image url
	 * @param close
	 *            boolean - The Closing indecator
	 * @param hasSizeInfo
	 *            boolean - THe indecator is saize is included in url.
	 */
	public void writeSizedImage(String imageUrl, boolean close,
			boolean hasSizeInfo) {
		int from = imageUrl.lastIndexOf(HtmlMethods.LAST_DIR);
		if (from < 0) {
			from = 0;
		}
		openTag(HtmlTags.IMG);
		writePair(HtmlTags.SRC, imageUrl);
		if (hasSizeInfo) {
			final int first = imageUrl.indexOf(HtmlMethods.SIZE_SEP, from);
			final int sec = imageUrl.indexOf(HtmlMethods.SIZE_SEP, first + 1);
			final int third = imageUrl.indexOf(HtmlMethods.SIZE_SEP, sec + 1);
			final int dot = imageUrl.lastIndexOf(HtmlMethods.DOT);

			if (first > 0 && sec > 0 && third > 0 && dot > 0) {
				final String wide = imageUrl.substring(sec + 1, third);
				final String height = imageUrl.substring(third + 1, dot);
				writePair(HtmlTags.HEIGHT, height);
				writePair(HtmlTags.WIDTH, wide);
			} else {
				final int stheight = imageUrl.lastIndexOf(HtmlMethods.SIZE_SEP);
				if (stheight > 0) {
					final String heightString = imageUrl.substring(
							stheight + 1, dot);
					writePair(HtmlTags.HEIGHT, heightString);
					final int stwide = imageUrl.indexOf(HtmlMethods.SIZE_SEP,
							stheight + 1);
					if (stwide > 0) {
						final String wideString = imageUrl.substring(
								stwide + 1, stheight);
						try {
							Integer.parseInt(wideString);
							writePair(HtmlTags.WIDTH, wideString);
						} catch (NumberFormatException ex) {
							writePair(HtmlTags.WIDTH, heightString);
						}
					} else {
						writePair(HtmlTags.WIDTH, heightString);
					}

				}
			}
		}
		writePair(HtmlTags.BORDER, 0);
		if (close) {
			closeEmptyTag();
		}
	}

	public void closeEmptyTag() {
		if (tree.size() > 0) {
			tree.remove(tree.size() - 1);
			if (open) {
				writeDirect(END + CL);
			}
			identCount--;
			open = false;
		}
	}

	private String getCurrentTag() {
		if (tree.size() == 0) {
			return null;
		} else {
			return (String) tree.get(tree.size() - 1);
		}
	}

	public void closeTag() {
		if (tree.size() > 0) {
			final String last = (String) tree.remove(tree.size() - 1);
			if (open) {
				writeDirect(CL);
			}
			if (!HtmlTags.A.equals(last)) {
				writeDirect(NL);
				writeIdented(OP + END + last + CL);
			} else {
				writeDirect(OP + END + last + CL);
			}

			identCount--;
			open = false;
		}
	}

}
