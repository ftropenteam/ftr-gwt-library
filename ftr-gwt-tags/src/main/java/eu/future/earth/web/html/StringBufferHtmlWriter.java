package eu.future.earth.web.html;

public class StringBufferHtmlWriter extends HtmlWriter {

	public StringBufferHtmlWriter(StringBuffer newBuffer) {
		super(new StringBufferWriter(newBuffer));
	}

}
