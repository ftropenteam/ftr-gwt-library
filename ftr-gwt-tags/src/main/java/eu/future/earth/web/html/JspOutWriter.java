package eu.future.earth.web.html;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.servlet.jsp.JspWriter;

public class JspOutWriter implements UniversalWriter {

	private static final Logger LOGGER = Logger.getLogger("Jsp");

	private JspWriter out = null;

	public JspOutWriter(JspWriter newOut) {
		super();
		out = newOut;
	}

	public void write(String data) {
		try {
			out.write(data);
		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, "", ex);
		}
	}
}
