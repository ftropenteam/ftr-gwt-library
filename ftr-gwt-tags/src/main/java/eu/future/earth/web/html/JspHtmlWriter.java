package eu.future.earth.web.html;

import jakarta.servlet.jsp.JspWriter;

public class JspHtmlWriter extends HtmlWriter {

	public JspHtmlWriter(JspWriter newOut) {
		super(new JspOutWriter(newOut));
	}

}
