/*
 * Copyright 2008 Google Inc. Licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package eu.future.earth.slider.client;

import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FocusPanel;

/**
 * A widget that allows the user to select a value within a range of possible values using a sliding bar that responds to mouse events. <h3>Keyboard
 * Events</h3>
 * <p>
 * SliderBarVertical listens for the following key events. Holding down a key will repeat the action until the key is released.
 * <ul class='css'>
 * <li>up arrow - shift up one step</li>
 * <li>down arrow - shift down one step</li>
 * <li>page up - shift up one page</li>
 * <li>page down - shift down one page</li>
 * <li>home - jump to min value</li>
 * <li>end - jump to max value</li>
 * <li>space - jump to middle value</li>
 * </ul>
 * </p>
 */
public class SliderBarFocusPanel extends FocusPanel {

	private BrowserEventParent listener;

	public SliderBarFocusPanel(BrowserEventParent newListener) {
		super();
		listener = newListener;
	}

	@Override
	public final void onBrowserEvent(final Event event) {
		super.onBrowserEvent(event);
		if (listener != null) {
			listener.onBrowserFocusPanelEvent(event);
		}
	}

}