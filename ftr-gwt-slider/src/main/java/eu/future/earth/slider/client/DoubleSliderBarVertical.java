/*
 * Copyright 2008 Google Inc. Licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package eu.future.earth.slider.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.*;

/**
 * A widget that allows the user to select a value within a range of possible values using a sliding bar that responds
 * to mouse events. <h3>Keyboard Events</h3>
 * <p>
 * SliderBarVertical listens for the following key events. Holding down a key will repeat the action until the key is
 * released.
 * <ul class='css'>
 * <li>up arrow - shift up one step</li>
 * <li>down arrow - shift down one step</li>
 * <li>page up - shift up one page</li>
 * <li>page down - shift down one page</li>
 * <li>home - jump to min value</li>
 * <li>end - jump to max value</li>
 * <li>space - jump to middle value</li>
 * </ul>
 * </p>
 */
public class DoubleSliderBarVertical extends Composite implements RequiresResize, HasValue<Double>, HasValueChangeHandlers<Double>, BrowserEventParent {

    /**
     * The space bar key code.
     */
    private static final int SPACE_BAR = 32;

    /**
     * The current value.
     */
    private double value;

    /**
     * A bit indicating whether the slider is enabled.
     */
    private boolean enabled = true;

    /**
     * The images used with the sliding bar.
     */
    private final SliderBarVerticalResources res;

    /**
     * The knob that slides across the line.
     */
    private final Image knobImage;

    /**
     * The maximum slider value.
     */
    private double maxValue;

    /**
     * The minimum slider value.
     */
    private double minValue;

    // private boolean snapToTick = false;
    private double tickValue = 0.1;

    int intTick = 0;

    private double majorTickValue = 5;

    public double getMajorTickValue() {
        return majorTickValue;
    }

    public void setMajorTickValue(double majorTickValue) {
        this.majorTickValue = majorTickValue;
        drawLabels();
    }

    public double getTickValue() {
        return tickValue;
    }

    public void setTickValue(double tickValue) {
        this.tickValue = tickValue;
        intTick = (int) tickValue * 10;
        drawLabels();
    }

    /**
     * A bit indicating whether we are currently sliding the slider bar due to mouse events.
     */
    private boolean slidingMouse = false;

    private AbsolutePanel main = new AbsolutePanel();

    public DoubleSliderBarVertical() {
        this(1, 1000, 1);
    }

    public DoubleSliderBarVertical(final double minValue2, final double maxValue2) {
        this(minValue2, maxValue2, SliderBarVerticalResources.INSTANCE, minValue2);
    }

    public DoubleSliderBarVertical(final double minValue2, final double maxValue2, final double curValue2) {
        this(minValue2, maxValue2, SliderBarVerticalResources.INSTANCE, curValue2);
    }

    private FocusPanel panel;

    private final com.google.gwt.dom.client.Element knobElement;


    public DoubleSliderBarVertical(final double minValue2, final double maxValue2, final SliderBarVerticalResources res2, final double curValue2) {
        super();
        res = res2;
        res.sliderBarCss().ensureInjected();
        minValue = minValue2;
        maxValue = maxValue2;
        value = curValue2;

        panel = new SliderBarFocusPanel(this);

        panel.setStyleName(res.sliderBarCss().sliderBarVerticalShell());
        // Create the knob
        knobImage = new Image(res.slider());
        knobImage.setStyleName(res.sliderBarCss().sliderBarVerticalKnob());
        knobElement = knobImage.getElement();
        panel.add(knobImage);

        panel.sinkEvents(Event.MOUSEEVENTS | Event.KEYEVENTS | Event.FOCUSEVENTS | Event.ONMOUSEWHEEL);

        panel.addKeyUpHandler(event -> {
            if (enabled) {
                doKeyAction(event.getNativeKeyCode());
            }
        });

        panel.addClickHandler(event -> {
            if (enabled) {
                setCurrentValue(getValueForY(event.getClientY()), true);
            }
        });

        panel.addMouseWheelHandler(event -> {
            if (enabled) {
                GWT.log("onmousewheel");
                event.preventDefault();
                final int deltaY = event.getDeltaY();
                if (deltaY > 0) {
                    shiftDown(deltaY);
                } else {
                    shiftUp(Math.abs(deltaY));
                }
            }

        });

        initWidget(main);

    }

    private double getValueForY(double y) {
        if (y > 0) {
            y -= 5;
            final double lineTop = getAbsoluteTop();
            final double percent = 1.0 - ((y - lineTop) / getOffsetHeight());
            return (getTotalRange() * percent + minValue);
        }
        return value;
    }

    private boolean showLabels = true;

    /**
     * The value change handler.
     *
     * @param handler the handler
     * @return the handle
     */
    @Override
    public final HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Double> handler) {
        return addHandler(handler, ValueChangeEvent.getType());
    }

    public void doKeyAction(final int keyCode) {
        switch (keyCode) {
            case KeyCodes.KEY_HOME:
                setCurrentValue(minValue);
                break;
            case KeyCodes.KEY_END:
                setCurrentValue(maxValue);
                break;
            case KeyCodes.KEY_UP:
                shiftUp(tickValue);
                break;
            case KeyCodes.KEY_DOWN:
                shiftDown(tickValue);
                break;
            case SPACE_BAR:
                setCurrentValue((int) (minValue + getTotalRange() / 2));
                break;
            default:
        }
    }

    private int labelSpacing = 15;

    public int getLabelSpacing() {
        return labelSpacing;
    }

    public void setLabelSpacing(int labelSpacing) {
        this.labelSpacing = labelSpacing;
        drawLabels();
    }

    private double getPositionFor(double newValue) {
        if (maxValue <= minValue) {
            return 0;
        }
        final double percent = (newValue - minValue) / getTotalRange();
        return height - (height * Math.max(0.0, Math.min(1.0, percent)));

    }

    private double height = 200;

    private void drawLabels() {
        if (isAttached()) {
            main.clear();

            height = (getTotalRange() * labelSpacing) / tickValue;
            int newWidth = 20;

            setPixelSize(newWidth + 30, (int) height);
            // GWT.log("Size = " + height);
            if (showLabels) {
                for (double i = minValue; i < maxValue; ) {
                    i += tickValue;
                    double linePos = getPositionFor(i);
                    SimplePanel line = new SimplePanel();
                    line.setPixelSize(newWidth, 1);

                    if (times(i, majorTickValue)) {
                        Label label = new Label(String.valueOf((int) i));
                        label.setHorizontalAlignment(Label.ALIGN_RIGHT);
                        main.add(label, newWidth + 2, (int) linePos);
                        line.setStyleName(res.sliderBarCss().majorLine());
                    } else {
                        line.setStyleName(res.sliderBarCss().minorLine());
                    }
                    main.add(line, 0, (int) linePos + 8);
                }
            }

            main.add(panel, 0, 5);
            panel.setPixelSize(newWidth, (int) height);
        }
    }

    public static boolean times(double dNumber, double dTimes) {
        int number = (int) (dNumber * 10);
        int times = (int) (dTimes * 10);
        if (number == 0) {
            return false;
        }
        final double res = (double) number / (double) times;
        final int test = (int) res;
        return String.valueOf((double) test).equals(String.valueOf(res));
    }

    /**
     * Draw the knob where it is supposed to be relative to the line.
     */
    private void drawKnob() {
        // Abort if not attached
        if (!isAttached()) {
            return;
        }
        // Move the knob to the correct position

        final double knobTopOffset = getPositionFor(value);
        knobElement.getStyle().setProperty("top", knobTopOffset - 1 + "px");
    }

    /**
     * Return the current value.
     *
     * @return the current value
     */
    public final double getCurrentValue() {
        return value;
    }

    /**
     * Calculate the knob's position in coordinates.
     *
     * @return the knob's position
     */
    private int getKnobPosition() {
        return knobImage.getAbsoluteTop();
    }

    /**
     * Return the max value.
     *
     * @return the max value
     */
    public final double getMaxValue() {
        return maxValue;
    }

    /**
     * Return the minimum value.
     *
     * @return the minimum value
     */
    public final double getMinValue() {
        return minValue;
    }

    /**
     * Return the total range between the minimum and maximum values.
     *
     * @return the total range
     */
    public final double getTotalRange() {
        if (minValue > maxValue) {
            return 0.0;
        } else {
            return maxValue - minValue;
        }
    }

    @Override
    public final Double getValue() {
        return value;
    }

    /**
     * @return Gets whether this widget is enabled
     */
    public final boolean isEnabled() {
        return enabled;
    }

    /**
     * Listen for events that will move the knob.
     *
     * @param event the event that occurred
     */
    @Override
    public final void onBrowserFocusPanelEvent(final Event event) {
        if (enabled) {
            switch (DOM.eventGetType(event)) {

                case Event.ONBLUR:
                    GWT.log("blur");

                    if (slidingMouse) {
                        slidingMouse = false;
                        slideKnob(event, true);
                    }
                    break;
                // case Event.ONKEYDOWN:
                // GWT.log("onkeydown");
                // DOM.eventPreventDefault(event);
                // if (slidingMouse) {
                // slidingMouse = false;
                // }
                // doKeyAction(DOM.eventGetKeyCode(event));
                // break;

                // Mouse Events
                case Event.ONMOUSEDOWN:
                    GWT.log("onmousedown");
                    if (sliderClicked(event)) {
                        slidingMouse = true;
                        DOM.setCapture(panel.getElement());
                        event.preventDefault();
                        slideKnob(event, false);
                    }
                    break;
                case Event.ONMOUSEUP:
                    GWT.log("onmouseup");
                    DOM.releaseCapture(panel.getElement());
                    if (slidingMouse) {
                        slideKnob(event, true);
                        slidingMouse = false;
                    }
                    break;
                case Event.ONMOUSEMOVE:
                    if (slidingMouse) {
                        GWT.log("onmousemove");
                        slideKnob(event, false);
                    }
                    break;
                default:
            }
        }
    }

    /**
     * Slide the knob to a new location.
     *
     * @param event     the mouse event
     * @param fireEvent whether to just slide the knob
     */
    private void slideKnob(final Event event, final boolean fireEvent) {
        final int y = event.getClientY();
        if (y > 0) {
            setCurrentValue(getValueForY(y), fireEvent);
        }
    }

    /**
     * Checks whether the slider was clicked or not.
     *
     * @param event the mouse event
     * @return whether the slider was clicked
     */
    private boolean sliderClicked(final Event event) {
        boolean sliderClicked = false;
        final int y = event.getClientY();
        final int knopPos = getKnobPosition();
        GWT.log("y = " + y + " and pos = " + knopPos);
        if (y < (knopPos + 7) && y > (knopPos - 7)) {
            // if (DOM.eventGetTarget(event).equals(knobImage.getElement())) {
            sliderClicked = true;
        }
        return sliderClicked;
    }

    /**
     * Handle the resize event.
     */
    @Override
    public void onResize() {
        if (isAttached()) {
            drawKnob();
        }
    }

    private void resetCurrentValue(final boolean fireEvent) {
        setCurrentValue(getCurrentValue(), fireEvent);
    }

    /**
     * Set the current value and fire the onValueChange event.
     *
     * @param curValue2 the current value
     */
    public final void setCurrentValue(final double curValue2) {
        setCurrentValue(curValue2, true);
    }

    public final void setCurrentValue(final double newValue, final boolean fireEvent) {
        // Confine the value to the range
        GWT.log("new = " + newValue);
        double curValue2 = snapToTickValue(newValue);

        value = Math.max(minValue, Math.min(maxValue, curValue2));

        // Redraw the knob
        drawKnob();
        GWT.log("cu" + curValue2);
        // Fire the ValueChangeEvent
        if (fireEvent) {
            // keyTimer.cancel();
            ValueChangeEvent.fire(this, value);
        }
    }

    public double snapToTickValue(double y) {
        if (tickValue < 0) {
            return y;
        }

        return ((double) Math.round(((y * 10) / tickValue) * tickValue)) / 10;
    }

    @Override
    protected void onAttach() {
        super.onAttach();
        drawLabels();
        drawKnob();
    }

    /**
     * Sets whether this widget is enabled.
     *
     * @param enabled2 true to enable the widget, false to disable it
     */
    public final void setEnabled(final boolean enabled2) {

        enabled = enabled2;
        if (enabled) {
            knobImage.removeStyleName(res.sliderBarCss().sliderDisabled());
        } else {
            knobImage.addStyleName(res.sliderBarCss().sliderDisabled());
        }
    }

    public final void setMaxValue(final double maxValue2) {
        maxValue = maxValue2;
        resetCurrentValue(true);
        drawLabels();
    }


    public final void setMaxValue(final double maxValue2, final boolean fireEvent) {
        maxValue = maxValue2;
        resetCurrentValue(fireEvent);
    }

    /**
     * Set the minimum value.
     *
     * @param minValue2 the current value
     */
    public final void setMinValue(final double minValue2) {
        minValue = minValue2;
        resetCurrentValue(true);
        drawLabels();
    }

    /**
     * Set the value of the slider in real terms.
     *
     * @param value the value
     */
    @Override
    public final void setValue(final Double value) {
        setCurrentValue(value, false);
    }

    /**
     * Set the value of the slider in real terms.
     *
     * @param value     the value
     * @param fireEvent whether to fire an event
     */
    @Override
    public final void setValue(final Double value, final boolean fireEvent) {
        setCurrentValue(value, fireEvent);
    }

    /**
     * Shift to the right (greater value).
     *
     * @param numSteps the number of steps to shift
     */
    public final void shiftDown(final double numSteps) {
        setCurrentValue(getCurrentValue() - numSteps);
    }

    /**
     * Shift to the left (smaller value).
     *
     * @param numSteps the number of steps to shift
     */
    public final void shiftUp(final double numSteps) {
        setCurrentValue(getCurrentValue() + numSteps);
    }

}