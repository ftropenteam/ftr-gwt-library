package eu.future.earth.slider.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

/**
 * A {@link ClientBundle} that provides resources for {@link SliderBarVertical}.
 */
public interface SliderBarVerticalResources extends ClientBundle {
	public interface SliderBarVerticalResourcesCss extends CssResource {

		String sliderBarVerticalKnob();

		String sliderBarVerticalShell();
		
		String minorLine();
		
		String sliderDisabled();

		String sliderBarVerticalShellFocused();

		String majorLine();
	}

	/**
	 * The image bundle.
	 */
	SliderBarVerticalResources INSTANCE = GWT.create(SliderBarVerticalResources.class);

	/**
	 * An image used for the sliding knob.
	 * 
	 * @return a prototype of this image
	 */
	@Source("slider.png")
	ImageResource slider();

	/**
	 * The Css.
	 * 
	 * @return the resource
	 */
	@Source("SliderBarVertical.css")
	SliderBarVerticalResourcesCss sliderBarCss();

}