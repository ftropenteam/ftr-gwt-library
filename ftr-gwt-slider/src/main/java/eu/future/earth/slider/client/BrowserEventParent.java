package eu.future.earth.slider.client;

import com.google.gwt.user.client.Event;

public interface BrowserEventParent {

	void onBrowserFocusPanelEvent(Event event);

}
