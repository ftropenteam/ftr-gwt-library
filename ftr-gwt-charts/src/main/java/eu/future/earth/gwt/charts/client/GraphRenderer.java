package eu.future.earth.gwt.charts.client;


public interface GraphRenderer {

	void renderData(GraphRaster canvas);

	double getMaxY();

	double getMaxX();

	void mouseMoveOver(int clientX, int clientY);

	void mouseClick(int clientX, int clientY);
	
}
