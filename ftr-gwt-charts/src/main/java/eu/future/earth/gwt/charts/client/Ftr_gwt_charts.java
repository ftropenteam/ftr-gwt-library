package eu.future.earth.gwt.charts.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Ftr_gwt_charts implements EntryPoint {


	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final Button sendButton = new Button("Send");

		// We can add style names to widgets
		sendButton.addStyleName("sendButton");

		HorizontalPanel test = new HorizontalPanel();

		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		RootPanel.get("nameFieldContainer").add(test);
		RootPanel.get("sendButtonContainer").add(sendButton);

		GraphRaster canvas = new GraphRaster();
		test.add(canvas);

		canvas.addXLine(new RasterLine(10, "10", true));
		canvas.addXLine(new RasterLine(20, "20", false));
		canvas.addXLine(new RasterLine(30, "30", true));
		canvas.addXLine(new RasterLine(40, "40", false));
		canvas.addXLine(new RasterLine(50, "50", true));
		canvas.addXLine(new RasterLine(60, "60", false));
		canvas.addXLine(new RasterLine(70, "70", true));
		canvas.addXLine(new RasterLine(80, "80", false));
		canvas.addXLine(new RasterLine(90, "90", true));


		canvas.addYLine(new RasterLine(10, "10", true));
		canvas.addYLine(new RasterLine(20, "20", false));
		canvas.addYLine(new RasterLine(30, "30", true));
		canvas.addYLine(new RasterLine(40, "40", false));
		canvas.addYLine(new RasterLine(50, "50", true));
		canvas.addYLine(new RasterLine(60, "60", false));
		canvas.addYLine(new RasterLine(70, "70", true));
		canvas.addYLine(new RasterLine(80, "80", false));
		canvas.addYLine(new RasterLine(90, "90", true));
		
		LineRenderer<DefaultPointItem> renderer2 = new LineRenderer<DefaultPointItem>(
				new DefaultPointTranslator());
		renderer2.setColor("#8b0000");
		renderer2.addItem(new DefaultPointItem(10,10));
		renderer2.addItem(new DefaultPointItem(20,30));
		renderer2.addItem(new DefaultPointItem(30,40));
		renderer2.addItem(new DefaultPointItem(40,30));
		renderer2.addItem(new DefaultPointItem(50,20));
		
		canvas.addLine(renderer2);

		
		LineRenderer<DefaultPointItem> renderer = new LineRenderer<DefaultPointItem>(
				new DefaultPointTranslator());

		renderer.addItem(new DefaultPointItem(10,10));
		renderer.addItem(new DefaultPointItem(20,20));
		renderer.addItem(new DefaultPointItem(30,10));
		renderer.addItem(new DefaultPointItem(40,10));
		renderer.addItem(new DefaultPointItem(50,00));
		
		canvas.addLine(renderer);
		canvas.setShowXItems(false);
//		canvas.setShowYItems(false);
		canvas.draw();
	
	}
}
