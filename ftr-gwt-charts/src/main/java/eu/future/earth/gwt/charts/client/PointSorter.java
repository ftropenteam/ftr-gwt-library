package eu.future.earth.gwt.charts.client;

import java.util.Comparator;

public class PointSorter<T> implements Comparator<T> {

	private PointTranslator<T> translator;

	public PointSorter(PointTranslator<T> translator) {
		super();
		this.translator = translator;
	}

	public int compare(T o1, T o2) {
		if (translator.getXCoor(o1) == translator.getXCoor(o2)) {
			return 0;
		} else {
			if (translator.getXCoor(o1) < translator.getXCoor(o2)) {
				return -1;
			} else {
				return 1;
			}
		}
	}

}
