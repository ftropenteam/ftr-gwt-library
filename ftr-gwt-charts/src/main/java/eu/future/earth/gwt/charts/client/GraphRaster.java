package eu.future.earth.gwt.charts.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RequiresResize;

public class GraphRaster extends Composite implements RequiresResize {

    private final AbsolutePanel holder = new AbsolutePanel();

    private Canvas canvas;

    public GraphRaster() {
        super();
        initWidget(holder);
        canvas = Canvas.createIfSupported();
        canvas.addMouseMoveHandler(event -> displayTip(event));

        canvas.addMouseDownHandler(event -> handleClick(event));
    }

    protected void displayTip(MouseMoveEvent event) {
        for (GraphRenderer line : lines) {
            line.mouseMoveOver(event.getX(), event.getY());
        }

    }

    protected void handleClick(MouseDownEvent event) {
        for (GraphRenderer line : lines) {
            line.mouseClick(event.getX(), event.getY());
        }

    }

    public void renderGrid() {

    }

    private String backgroundColor = "#FFFFFF";

    public int getLeftOffSet() {
        return leftOffSet;
    }

    public void setLeftOffSet(int leftOffSet) {
        this.leftOffSet = leftOffSet;
    }

    private double height = 400;

    private double width = 400;

    private int leftOffSet = 50;

    public int getBottumOffSet() {
        return bottumOffSet;
    }

    public void setBottumOffSet(int bottumOffSet) {
        this.bottumOffSet = bottumOffSet;
    }

    private int bottumOffSet = 20;

    // public void setPixelSize(int newWidth, int newHeight) {
    // height = newHeight;
    // width = newWidth;
    // resize();
    // }

    private void resize() {
        holder.clear();
        yLabel.clear();
        int offsety = 0;
        if (showXItems) {
            offsety = bottumOffSet;
        }
        int offsetx = 0;
        if (showYItems) {
            offsetx = leftOffSet;
        }
        setPixelSize(((int) width) + offsetx, ((int) height) + offsety);
        holder.setPixelSize(((int) width) + offsetx, ((int) height) + offsety);

        if (showYItems) {
            yLabel.setPixelSize(leftOffSet, ((int) height) + offsety);
        } else {
            yLabel.setPixelSize(leftOffSet, ((int) height) + offsety);
        }
        canvas.setCoordinateSpaceHeight((int) height);
        canvas.setCoordinateSpaceWidth((int) width);
        canvas.setPixelSize((int) width, (int) height);
        holder.add(canvas, offsetx, 0);
        if (backgroundColor != null) {
            canvas.getElement().getStyle().setBackgroundColor(backgroundColor);
        }
    }

    @Override
    public int getOffsetHeight() {
        return (int) height;
    }

    private List<GraphRenderer> lines = new ArrayList<GraphRenderer>();

    public void addLine(GraphRenderer newLine) {
        lines.add(newLine);
    }

    private List<RasterLine> yRaster = new ArrayList<>();

    private List<RasterLine> xRaster = new ArrayList<>();

    private final YLabelPenel yLabel = new YLabelPenel();

    public YLabelPenel getyLabel() {
        return yLabel;
    }

    private boolean showXItems = true;

    private boolean showYItems = true;

    public void addYLine(RasterLine newLine) {
        yRaster.add(newLine);
    }

    public void addXLine(RasterLine newLine) {
        xRaster.add(newLine);
    }

    public boolean isShowXItems() {
        return showXItems;
    }

    public void setShowXItems(boolean showXItems) {
        this.showXItems = showXItems;
        resize();
    }

    public boolean isShowYItems() {
        return showYItems;
    }

    public void setShowYItems(boolean showYItems) {
        this.showYItems = showYItems;
        resize();
    }

    public void clearData() {
        lines.clear();
        yRaster.clear();
        xRaster.clear();
    }

    public void draw() {

        height = 0;
        width = 0;

        // Determine size needed
        for (RasterLine xLine : xRaster) {
            if (xLine.getPos() > 0) {
                width = Math.max(xLine.getPos(), width);
            }
        }

        for (RasterLine xLine : yRaster) {
            if (xLine.getPos() > 0) {
                height = Math.max(xLine.getPos(), height);
            }
        }

        for (GraphRenderer line : lines) {
            height = Math.max(height, line.getMaxY());
            width = Math.max(width, line.getMaxX());
        }

        height += 10;
        width += 10;

        resize();

        Context2d paintOn = this.canvas.getContext2d();
        paintOn.setLineWidth(1);
        paintOn.setStrokeStyle("red");
        paintOn.beginPath();
        paintOn.setGlobalAlpha(1.0);
        paintOn.moveTo(0, 0);

        int height = this.canvas.getCoordinateSpaceHeight();
        int width = this.canvas.getCoordinateSpaceWidth();

        paintOn.lineTo(0, height);
        paintOn.lineTo(width, height);
        paintOn.stroke();
        paintOn.closePath();

        int offsety = 0;
        if (showXItems) {
            offsety = 18;
        }
        int offsetx = 0;
        if (showYItems) {
            offsetx = leftOffSet;
        }
        paintOn.setLineWidth(0.5);
        paintOn.setStrokeStyle("black");

        for (RasterLine xLine : xRaster) {
            if (xLine.getPos() > 0) {
                if (xLine.isBold()) {
                    // canvas.setLineWidth(1.0);
                    paintOn.setGlobalAlpha(1.0);
                } else {
                    // canvas.setLineWidth(1.0);
                    paintOn.setGlobalAlpha(0.2);
                }
                paintOn.beginPath();
                paintOn.moveTo(xLine.getPos(), height);
                paintOn.lineTo(xLine.getPos(), 0);
                paintOn.stroke();
                paintOn.closePath();
            }
            if (showXItems && xLine.isShowLabel()) {
                Label label = new Label(xLine.getLabel());
                label.setTitle(xLine.getTitle());
                holder.add(label, (int) (offsetx - 3 + xLine.getPos()), height);
            }

        }

        for (RasterLine yLine : yRaster) {
            if (yLine.getPos() > 0) {
                if (yLine.isBold()) {
                    paintOn.setGlobalAlpha(0.6);
                } else {
                    paintOn.setGlobalAlpha(0.2);
                }
                paintOn.beginPath();
                paintOn.moveTo(0, height - yLine.getPos());
                paintOn.lineTo(width, height - yLine.getPos());
                paintOn.stroke();
                paintOn.closePath();
            }
            if (showYItems && yLine.isShowLabel()) {
                Label label = new Label(yLine.getLabel());
                label.setTitle(yLine.getTitle());
                label.setHorizontalAlignment(Label.ALIGN_RIGHT);
                if (offsetx > 5) {
                    label.setWidth(offsetx - 4 + "px");
                } else {
                    if (leftOffSet > 5) {
                        label.setWidth(leftOffSet - 4 + "px");
                    }
                }
                holder.add(label, 0, (int) ((height - yLine.getPos()) - offsety + 10));
            } else {
                if (yLine.isShowLabel()) {
                    Label label = new Label(yLine.getLabel());
                    label.setTitle(yLine.getTitle());
                    label.setHorizontalAlignment(Label.ALIGN_RIGHT);
                    if (offsetx > 5) {
                        label.setWidth(offsetx - 4 + "px");
                    } else {
                        if (leftOffSet > 5) {
                            label.setWidth(leftOffSet - 4 + "px");
                        }
                    }
                    yLabel.add(label, 0, (int) ((height - yLine.getPos()) - (offsety + 10)));
                }
            }
        }

        double maxX = 0;
        double maxY = 0;
        for (GraphRenderer item : lines) {
            maxX = Math.max(maxX, item.getMaxX());
            maxY = Math.max(maxY, item.getMaxY());
        }

        for (GraphRenderer line : lines) {
            line.renderData(this);
        }
    }

    public Canvas getCanvas() {
        return canvas;
    }

    @Override
    public void onResize() {

    }

}
