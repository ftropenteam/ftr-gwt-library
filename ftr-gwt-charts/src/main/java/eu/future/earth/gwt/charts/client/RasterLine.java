package eu.future.earth.gwt.charts.client;

import java.io.Serializable;

public class RasterLine implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6882943723455136798L;

	private double pos;

	private String label;

	private String title;

	private boolean bold = false;

	public RasterLine(double pos, boolean showLabel) {
		super();
		this.pos = pos;
		this.showLabel = showLabel;
	}

	public RasterLine(double pos, String label, boolean showLabel) {
		super();
		this.pos = pos;
		this.label = label;
		this.title = label;
		this.showLabel = showLabel;
	}

	public RasterLine(double pos, String label, String title, boolean showLabel) {
		super();
		this.pos = pos;
		this.label = label;
		this.title = title;
		this.showLabel = showLabel;
	}

	private boolean showLabel = true;

	public double getPos() {
		return pos;
	}

	public void setPos(double pos) {
		this.pos = pos;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isShowLabel() {
		return showLabel;
	}

	public void setShowLabel(boolean showLabel) {
		this.showLabel = showLabel;
	}

	public boolean isBold() {
		return bold;
	}

	public void setBold(boolean bold) {
		this.bold = bold;
	}

}
