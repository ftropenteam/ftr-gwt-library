package eu.future.earth.gwt.charts.client;

public class DefaultPointTranslatorWithData<E> implements PointTranslator<DefaultPointItemWithData<E>> {

	public double getXCoor(DefaultPointItemWithData<E> theObject) {
		return theObject.getX();
	}

	public double getYCoor(DefaultPointItemWithData<E> theObject) {
		return theObject.getY();
	}

	@Override
	public String getToolTip(DefaultPointItemWithData<E> theObect) {
		return theObect.getTip();
	}

	@Override
	public String getCustomColor(DefaultPointItemWithData<E> theObect) {
		return theObect.getCustomColor();
	}

}
