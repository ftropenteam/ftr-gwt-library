/*
 * Copyright 2009 EasyEnterprise, all rights reserved.
 */
package eu.future.earth.gwt.charts.client.event.mouseover;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

public interface HasMouseOverPointHandlers<T> extends HasHandlers {

  HandlerRegistration addMouseOverPointHandler(MouseOverPointHandler<T> handler);
}
