package eu.future.earth.gwt.charts.client;

public class DefaultPointItemWithData<E> extends DefaultPointItem {

	public DefaultPointItemWithData(double x, double y, String newTip) {
		super(x, y, newTip);
	}

	public DefaultPointItemWithData(double x, double y) {
		super(x, y);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1257995218667911338L;

	private E data = null;

	public DefaultPointItemWithData(double x, double y, String newTip, E newData) {
		super(x, y, newTip);
		data = newData;
	}

	public E getData() {
		return data;
	}

	public void setData(E data) {
		this.data = data;
	}

	public DefaultPointItemWithData(double x, double y, E newData) {
		super(x, y);
		data = newData;
	}

}
