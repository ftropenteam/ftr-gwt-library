package eu.future.earth.gwt.charts.client;

import java.io.Serializable;

public class DefaultPointItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2025590724622327066L;

	private double x = 0;

	private double y = 0;

	private String tip = null;

	private String customColor = null;
	
	public String getCustomColor() {
		return customColor;
	}

	public void setCustomColor(String customColor) {
		this.customColor = customColor;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public DefaultPointItem(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public DefaultPointItem(double x, double y, String newTip) {
		super();
		this.x = x;
		this.y = y;
		tip = newTip;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

}
