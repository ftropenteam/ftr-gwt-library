/*
 * Copyright 2009 EasyEnterprise, all rights reserved.
 */
package eu.future.earth.gwt.charts.client.event.mouseover;

import com.google.gwt.event.shared.EventHandler;

public interface MouseOverPointHandler<T> extends EventHandler {

	void onMouseOverPointEvent(MouseOverPointEvent<T> newDataEvent);
}
