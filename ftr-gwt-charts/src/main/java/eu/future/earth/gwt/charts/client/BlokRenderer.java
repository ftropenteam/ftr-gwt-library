package eu.future.earth.gwt.charts.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.event.shared.HandlerRegistration;

import eu.future.earth.gwt.charts.client.event.EventBase;
import eu.future.earth.gwt.charts.client.event.mouseclick.HasMouseClickOnPointHandlers;
import eu.future.earth.gwt.charts.client.event.mouseclick.MouseClickOnPointEvent;
import eu.future.earth.gwt.charts.client.event.mouseclick.MouseClickOnPointHandler;
import eu.future.earth.gwt.charts.client.event.mouseover.HasMouseOverPointHandlers;
import eu.future.earth.gwt.charts.client.event.mouseover.MouseOverPointEvent;
import eu.future.earth.gwt.charts.client.event.mouseover.MouseOverPointEventType;
import eu.future.earth.gwt.charts.client.event.mouseover.MouseOverPointHandler;

public class BlokRenderer<E> extends EventBase implements GraphRenderer, HasMouseClickOnPointHandlers<E>, HasMouseOverPointHandlers<E> {

	private PointTranslator<E> translator;

	// private PointSorter<E> sorter;

	private boolean fill = true;

	private boolean showCircles = false;

	public boolean isFill() {
		return fill;
	}

	public boolean isShowCircles() {
		return showCircles;
	}

	public void setShowCircles(boolean showCircles) {
		this.showCircles = showCircles;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}

	private int radiusCicle = 2;

	private int radiusCicleMatch = 4;

	public int getRadiusCicle() {
		return radiusCicle;
	}

	public void setRadiusCicle(int radiusCicle) {
		this.radiusCicle = radiusCicle;
	}

	public int getRadiusCicleMatch() {
		return radiusCicleMatch;
	}

	public void setRadiusCicleMatch(int radiusCicleMatch) {
		this.radiusCicleMatch = radiusCicleMatch;
	}

	public BlokRenderer(PointTranslator<E> newTranslator) {
		super();
		translator = newTranslator;
		// sorter = new PointSorter<E>(translator);
	}

	public List<E> data = new ArrayList<E>();

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	private String color = "#90ee90";

	public void addItem(E newItems) {
		data.add(newItems);
	}

	private List<TipHolder<E>> tips = new ArrayList<TipHolder<E>>();

	public boolean hasData() {
		return !data.isEmpty();
	}

	public void clearData() {
		data.clear();
	}

	@Override
	public void renderData(GraphRaster raster) {
		Canvas canvas = raster.getCanvas();
		tips.clear();
		last = null;
		if (!data.isEmpty()) {
			double maxX = 0;
			double maxY = 0;
			for (E item : data) {
				maxX = Math.max(maxX, translator.getXCoor(item));
				maxY = Math.max(maxY, translator.getYCoor(item));
			}
			int height = canvas.getCoordinateSpaceHeight();

			// Move to first point at zero

			if (showCircles) {
				for (E item : data) {
					String tip = translator.getToolTip(item);
					if (tip != null) {
						Context2d paintOnE = canvas.getContext2d();
						int radius = radiusCicle;
						paintOnE.beginPath();
						String customColor = translator.getCustomColor(item);
						if (customColor != null) {
							radius = 3;
							paintOnE.setStrokeStyle(customColor);
							paintOnE.setFillStyle(customColor);
						} else {
							paintOnE.setStrokeStyle(color);
							paintOnE.setFillStyle(color);
						}

						double yCoor = height - translator.getYCoor(item);
						double xCoor = translator.getXCoor(item);
						paintOnE.arc(xCoor, yCoor, radius, 0, Math.PI * 2.0, true);

						paintOnE.fill();
						paintOnE.closePath();
						TipHolder<E> newTip = new TipHolder<E>(item, tip, xCoor, yCoor, radius);
						tips.add(newTip);
					}

				}
			}

			Context2d paintOn = canvas.getContext2d();
			paintOn.setLineWidth(1);
			paintOn.setStrokeStyle(color);
			paintOn.beginPath();
			E firstPoint = data.get(0);
			paintOn.moveTo(translator.getXCoor(firstPoint), height);
			for (E item : data) {
				double yCoor = height - translator.getYCoor(item);
				double xCoor = translator.getXCoor(item);
				paintOn.lineTo(xCoor, yCoor);
			}
			paintOn.lineTo(maxY, height);
			paintOn.setLineWidth(1);
			paintOn.setStrokeStyle(color);

			paintOn.closePath();
			paintOn.stroke();

			paintOn.restore();
			if (fill) {
				paintOn.beginPath();
				paintOn.moveTo(translator.getXCoor(firstPoint), height);
				for (E item : data) {
					paintOn.lineTo(translator.getXCoor(item), height - translator.getYCoor(item));
				}
				paintOn.lineTo(maxY, height);
				paintOn.setGlobalAlpha(1);
				paintOn.setFillStyle(color);
				paintOn.setGlobalAlpha(0.3);
				paintOn.closePath();
				paintOn.fill();
			}
		}
	}

	@Override
	public double getMaxX() {
		double maxX = 0;
		for (E item : data) {
			maxX = Math.max(maxX, translator.getXCoor(item));
		}
		return maxX;
	}

	@Override
	public double getMaxY() {
		double maxY = 0;
		for (E item : data) {
			maxY = Math.max(maxY, translator.getYCoor(item));
		}
		return maxY;
	}

	private TipHolder<E> last = null;

	@Override
	public void mouseMoveOver(int clientX, int clientY) {
		for (TipHolder<E> tip : tips) {
			if (intersects(clientX, clientY, tip.getxCoor(), tip.getyCoor(), radiusCicleMatch)) {
				if (last == null || !last.equals(tip)) {
					if (last != null) {
						MouseOverPointEvent.fire(this, MouseOverPointEventType.Out, last.getData());
					}
					MouseOverPointEvent.fire(this, MouseOverPointEventType.In, tip.getData());
					last = tip;
					return;
				}
			}
		}
		if (last != null) {
			MouseOverPointEvent.fire(this, MouseOverPointEventType.Out, last.getData());
			last = null;
		}
	}

	private boolean intersects(double x, double y, double cx, double cy, int r) {
		double dx = x - cx;
		double dy = y - cy;
		return dx * dx + dy * dy <= r * r;
	}

	@Override
	public HandlerRegistration addMouseOverPointHandler(MouseOverPointHandler<E> handler) {
		return super.addHandler(handler, MouseOverPointEvent.getType());
	}

	@Override
	public HandlerRegistration addMouseClickOnPointHandler(MouseClickOnPointHandler<E> handler) {
		return super.addHandler(handler, MouseClickOnPointEvent.getType());
	}

	@Override
	public void mouseClick(int clientX, int clientY) {
		for (TipHolder<E> tip : tips) {
			if (intersects(clientX, clientY, tip.getxCoor(), tip.getyCoor(), radiusCicleMatch)) {
				MouseClickOnPointEvent.fire(this, tip.getData());
			}
		}

	}
}
