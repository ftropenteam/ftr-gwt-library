/*
 * Copyright 2009 EasyEnterprise, all rights reserved.
 */
package eu.future.earth.gwt.charts.client.event.mouseclick;

import com.google.gwt.event.shared.GwtEvent;

public class MouseClickOnPointEvent<T> extends GwtEvent<MouseClickOnPointHandler<T>> {

	/**
	 * Handler type.
	 */
	private static Type<MouseClickOnPointHandler<?>> TYPE;

	/**
	 * Fires a before selection event on all registered handlers in the handler manager. If no such handlers exist, this
	 * method will do nothing.
	 * 
	 * @param source
	 *            the source of the handlers
	 * @param navigator
	 *            the navigator associated with this event
	 * @param token
	 *            the navigation token associated with this event
	 * @return the event so that the caller can check if it was canceled, or null if no handlers of this event type have
	 *         been registered
	 */
	public static <T> MouseClickOnPointEvent<T> fire(HasMouseClickOnPointHandlers<T> source, T newData) {
		// If no handlers exist, then type can be null.
		if (TYPE != null) {
			final MouseClickOnPointEvent<T> event = new MouseClickOnPointEvent<T>(newData);
			source.fireEvent(event);
			return event;
		}
		return null;
	}

	/**
	 * Gets the type associated with this event.
	 * 
	 * @return returns the handler type
	 */
	public static <T> Type<MouseClickOnPointHandler<?>> getType() {
		if (TYPE == null) {
			TYPE = new Type<MouseClickOnPointHandler<?>>();
		}
		return TYPE;
	}

	private final T data;

	protected MouseClickOnPointEvent(T newData) {
		data = newData;
	}

	public T getData() {
		return data;
	}

	@SuppressWarnings({
			"rawtypes", "unchecked"
	})
	@Override
	public final Type<MouseClickOnPointHandler<T>> getAssociatedType() {
		return (Type) TYPE;
	}

	protected void dispatch(MouseClickOnPointHandler<T> handler) {
		handler.onMouseOverPointEvent(this);
	}
}
