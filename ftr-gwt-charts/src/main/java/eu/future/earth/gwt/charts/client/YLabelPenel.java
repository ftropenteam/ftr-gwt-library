package eu.future.earth.gwt.charts.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RequiresResize;

public class YLabelPenel extends Composite implements RequiresResize {

	private List<RasterLine> yRaster = new ArrayList<RasterLine>();

	private AbsolutePanel holder = new AbsolutePanel();

	public YLabelPenel() {
		super();
		initWidget(holder);
	}

	private int height = 400;

	private int width = 400;

	public void addYLine(RasterLine newLine) {
		yRaster.add(newLine);
	}

	public void setPixelSize(int newWidth, int newHeight) {
		if (newHeight > 0) {
			height = newHeight;
		}
		if (newWidth > 0) {
			width = newWidth;
		}
		holder.setPixelSize(width, height);
	}

	
	@Override
	public void onResize() {
		
	}

	public void clear() {
		holder.clear();
		
	}

	public void add(Label w, int left, int top) {
		holder.add(w, left, top);
	}

}
