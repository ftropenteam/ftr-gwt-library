/*
 * Copyright 2009 EasyEnterprise, all rights reserved.
 */
package eu.future.earth.gwt.charts.client.event.mouseclick;

import com.google.gwt.event.shared.EventHandler;

public interface MouseClickOnPointHandler<T> extends EventHandler {

	void onMouseOverPointEvent(MouseClickOnPointEvent<T> newDataEvent);
}
