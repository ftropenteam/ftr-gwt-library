/*
 * Copyright 2009 EasyEnterprise, all rights reserved.
 */
package eu.future.earth.gwt.charts.client.event.mouseclick;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

public interface HasMouseClickOnPointHandlers<T> extends HasHandlers {

  HandlerRegistration addMouseClickOnPointHandler(MouseClickOnPointHandler<T> handler);
}
