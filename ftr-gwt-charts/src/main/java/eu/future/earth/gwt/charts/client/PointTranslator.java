package eu.future.earth.gwt.charts.client;

public interface PointTranslator<T> {

	double getXCoor(T theObject);
	
	double getYCoor(T theObject);

	String getToolTip(T theObect);
	
	String getCustomColor(T theObect);
}
