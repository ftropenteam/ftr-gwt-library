/*
 * Copyright 2009 EasyEnterprise, all rights reserved.
 */
package eu.future.earth.gwt.charts.client.event.mouseover;

import com.google.gwt.event.shared.GwtEvent;

public class MouseOverPointEvent<T> extends GwtEvent<MouseOverPointHandler<T>> {

	/**
	 * Handler type.
	 */
	private static Type<MouseOverPointHandler<?>> TYPE;

	/**
	 * Fires a before selection event on all registered handlers in the handler manager. If no such handlers exist, this
	 * method will do nothing.
	 * 
	 * @param source
	 *            the source of the handlers
	 * @param navigator
	 *            the navigator associated with this event
	 * @param token
	 *            the navigation token associated with this event
	 * @return the event so that the caller can check if it was canceled, or null if no handlers of this event type have
	 *         been registered
	 */
	public static <T> MouseOverPointEvent<T> fire(HasMouseOverPointHandlers<T> source, MouseOverPointEventType navigator, T newData) {
		// If no handlers exist, then type can be null.
		if (TYPE != null) {
			final MouseOverPointEvent<T> event = new MouseOverPointEvent<T>(navigator, newData);
			source.fireEvent(event);
			return event;
		}
		return null;
	}

	
	
	
	/**
	 * Fires a before selection event on all registered handlers in the handler manager. If no such handlers exist, this
	 * method will do nothing.
	 * 
	 * @param source
	 *            the source of the handlers
	 * @param navigator
	 *            the navigator associated with this event
	 * @param token
	 *            the navigation token associated with this event
	 * @return the event so that the caller can check if it was canceled, or null if no handlers of this event type have
	 *         been registered
	 */
	public static <T> MouseOverPointEvent<T> fire(HasMouseOverPointHandlers<T> source, MouseOverPointEventType navigator) {
		// If no handlers exist, then type can be null.
		if (TYPE != null) {
			final MouseOverPointEvent<T> event = new MouseOverPointEvent<T>(navigator);
			source.fireEvent(event);
			return event;
		}
		return null;
	}

	/**
	 * Gets the type associated with this event.
	 * 
	 * @return returns the handler type
	 */
	public static <T> Type<MouseOverPointHandler<?>> getType() {
		if (TYPE == null) {
			TYPE = new Type<MouseOverPointHandler<?>>();
		}
		return TYPE;
	}

	private final MouseOverPointEventType dataEventType;

	private final T data;

	protected MouseOverPointEvent(MouseOverPointEventType navigator) {
		this.dataEventType = navigator;
		data = null;
	}

	protected MouseOverPointEvent(MouseOverPointEventType navigator, T newData) {
		this.dataEventType = navigator;
		data = newData;
	}

	public MouseOverPointEventType getDataEventType() {
		return dataEventType;
	}

	public T getData() {
		return data;
	}

	@SuppressWarnings({
			"rawtypes", "unchecked"
	})
	@Override
	public final Type<MouseOverPointHandler<T>> getAssociatedType() {
		return (Type) TYPE;
	}

	protected void dispatch(MouseOverPointHandler<T> handler) {
		handler.onMouseOverPointEvent(this);
	}
}
