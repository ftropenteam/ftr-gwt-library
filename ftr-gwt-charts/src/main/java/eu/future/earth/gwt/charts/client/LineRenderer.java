package eu.future.earth.gwt.charts.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;

public class LineRenderer<E> implements GraphRenderer {

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	private String color = "green";

	private PointTranslator<E> translator;

	private PointSorter<E> sorter;

	public LineRenderer(PointTranslator<E> newTranslator) {
		super();
		translator = newTranslator;
		sorter = new PointSorter<E>(translator);
	}

	public List<E> data = new ArrayList<E>();

	public void addItem(E newItems) {
		data.add(newItems);
	}

	@Override
	public void renderData(GraphRaster raster) {
		Canvas canvas = raster.getCanvas();
		Context2d paintOn = canvas.getContext2d();
		if (data != null && data.size() > 0) {
			double maxX = 0;
			double maxY = 0;
			for (E item : data) {
				maxX = Math.max(maxX, translator.getXCoor(item));
				maxY = Math.max(maxY, translator.getYCoor(item));
			}
			Collections.sort(data, sorter);

			paintOn.setLineWidth(1);
			paintOn.setStrokeStyle(color);

			int height = canvas.getCoordinateSpaceHeight();

			paintOn.beginPath();
			paintOn.moveTo(0, height);
			for (E item : data) {
				paintOn.lineTo(translator.getXCoor(item), height - translator.getYCoor(item));
			}
			paintOn.lineTo(maxY + 10, height);
			paintOn.setLineWidth(1);
			paintOn.setStrokeStyle(color);

			paintOn.closePath();
			paintOn.stroke();

			paintOn.restore();

			paintOn.beginPath();
			paintOn.moveTo(0, height);
			for (E item : data) {
				paintOn.lineTo(translator.getXCoor(item), height - translator.getYCoor(item));
			}
			paintOn.lineTo(maxY + 10, height);
			paintOn.setGlobalAlpha(1);
			paintOn.setFillStyle(color);
			paintOn.setGlobalAlpha(0.3);
			paintOn.closePath();
			paintOn.fill();
		}
	}

	@Override
	public double getMaxX() {
		double maxX = 0;
		for (E item : data) {
			maxX = Math.max(maxX, translator.getXCoor(item));
		}
		return maxX;
	}

	@Override
	public double getMaxY() {
		double maxY = 0;
		for (E item : data) {
			maxY = Math.max(maxY, translator.getYCoor(item));
		}
		return maxY;
	}

	@Override
	public void mouseMoveOver(int clientX, int clientY) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClick(int clientX, int clientY) {
		// TODO Auto-generated method stub

	}

}
