package eu.future.earth.gwt.charts.client;

import java.io.Serializable;

public class TipHolder<E> implements Serializable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("rawtypes")
		TipHolder other = (TipHolder) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (radius != other.radius)
			return false;
		if (tip == null) {
			if (other.tip != null)
				return false;
		} else if (!tip.equals(other.tip))
			return false;
		if (xCoor != other.xCoor)
			return false;
		if (yCoor != other.yCoor)
			return false;
		return true;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5775343462487554233L;

	private E data = null;

	private String tip = null;

	public TipHolder() {
		super();
	}

	public TipHolder(E data, String tip, double xCoor, double yCoor, int radius) {
		super();
		this.data = data;
		this.tip = tip;
		this.xCoor = xCoor;
		this.yCoor = yCoor;
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	private double xCoor;

	public E getData() {
		return data;
	}

	public void setData(E data) {
		this.data = data;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public double getxCoor() {
		return xCoor;
	}

	public void setxCoor(double xCoor) {
		this.xCoor = xCoor;
	}

	public double getyCoor() {
		return yCoor;
	}

	public void setyCoor(double yCoor) {
		this.yCoor = yCoor;
	}

	private double yCoor;

	private int radius;

}
