package eu.future.earth.gwt.charts.client;

public class DefaultPointTranslator implements PointTranslator<DefaultPointItem> {

	public double getXCoor(DefaultPointItem theObject) {
		return theObject.getX();
	}

	public double getYCoor(DefaultPointItem theObject) {
		return theObject.getY();
	}

	@Override
	public String getToolTip(DefaultPointItem theObect) {
		return theObect.getTip();
	}

	@Override
	public String getCustomColor(DefaultPointItem theObect) {
		return theObect.getCustomColor();
	}

}
