package com.google.gwt.user.client.ui;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;

/**
 * Clean checkbox.
 * @author mnouwens
 *
 */
public class CleanCheckBox extends ButtonBase implements HasName, HasValue<Boolean> {

	InputElement inputElem;

	private boolean valueChangeHandlerInitialized;

	/**
	 * Creates a check box with no label.
	 */
	public CleanCheckBox() {
		this(DOM.createInputCheck());
	}

	protected CleanCheckBox(Element elem) {
		super(elem);
		inputElem = InputElement.as(elem);
		String uid = DOM.createUniqueId();
		inputElem.setPropertyString("id", uid);
		setTabIndex(0);
	}

	public void setIdName(String newId) {
		inputElem.setId(newId);
	}

	@Override
	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Boolean> handler) {
		// Is this the first value change handler? If so, time to add handlers
		if (!valueChangeHandlerInitialized) {
			ensureDomEventHandlers();
			valueChangeHandlerInitialized = true;
		}
		return addHandler(handler, ValueChangeEvent.getType());
	}

	/**
	 * Returns the value property of the input element that backs this widget. This is the value that will be associated
	 * with the CheckBox name and submitted to the server if a {@link FormPanel} that holds it is submitted and the box
	 * is checked.
	 * <p>
	 * Don't confuse this with {@link #getValue}, which returns true or false if the widget is checked.
	 */
	public String getFormValue() {
		return inputElem.getValue();
	}

	@Override
	public String getName() {
		return inputElem.getName();
	}

	@Override
	public int getTabIndex() {
		return inputElem.getTabIndex();
	}

	/**
	 * Determines whether this check box is currently checked.
	 * <p>
	 * Note that this <em>does not</em> return the value property of the checkbox input element wrapped by this widget.
	 * For access to that property, see {@link #getFormValue()}
	 * 
	 * @return <code>true</code> if the check box is checked, false otherwise. Will not return null
	 */
	@Override
	public Boolean getValue() {
		if (isAttached()) {
			return inputElem.isChecked();
		} else {
			return inputElem.isDefaultChecked();
		}
	}

	/**
	 * Determines whether this check box is currently checked.
	 * 
	 * @return <code>true</code> if the check box is checked
	 * @deprecated Use {@link #getValue} instead
	 */
	@Deprecated
	public boolean isChecked() {
		// Funny comparison b/c getValue could in theory return null
		return getValue() == true;
	}

	@Override
	public boolean isEnabled() {
		return !inputElem.isDisabled();
	}

	@Override
	public void setAccessKey(char key) {
		inputElem.setAccessKey("" + key);
	}

	/**
	 * Checks or unchecks this check box. Does not fire {@link ValueChangeEvent}. (If you want the event to fire, use
	 * {@link #setValue(Boolean, boolean)})
	 * 
	 * @param checked
	 *            <code>true</code> to check the check box.
	 * @deprecated Use {@link #setValue(Boolean)} instead
	 */
	@Deprecated
	public void setChecked(boolean checked) {
		setValue(checked);
	}

	@Override
	public void setEnabled(boolean enabled) {
		inputElem.setDisabled(!enabled);
		if (enabled) {
			removeStyleDependentName("disabled");
		} else {
			addStyleDependentName("disabled");
		}
	}

	@Override
	public void setFocus(boolean focused) {
		if (focused) {
			inputElem.focus();
		} else {
			inputElem.blur();
		}
	}

	/**
	 * Set the value property on the input element that backs this widget. This is the value that will be associated
	 * with the CheckBox's name and submitted to the server if a {@link FormPanel} that holds it is submitted and the
	 * box is checked.
	 * <p>
	 * Don't confuse this with {@link #setValue}, which actually checks and unchecks the box.
	 * 
	 * @param value
	 */
	public void setFormValue(String value) {
		inputElem.setAttribute("value", value);
	}

	@Override
	public void setName(String name) {
		inputElem.setName(name);
	}

	@Override
	public void setTabIndex(int index) {
		// Need to guard against call to setTabIndex before inputElem is
		// initialized. This happens because FocusWidget's (a superclass of
		// CheckBox) setElement method calls setTabIndex before inputElem is
		// initialized. See CheckBox's protected constructor for more information.
		if (inputElem != null) {
			inputElem.setTabIndex(index);
		}
	}

	/**
	 * Checks or unchecks the check box.
	 * <p>
	 * Note that this <em>does not</em> set the value property of the checkbox input element wrapped by this widget. For
	 * access to that property, see {@link #setFormValue(String)}
	 * 
	 * @param value
	 *            true to check, false to uncheck; null value implies false
	 */
	@Override
	public void setValue(Boolean value) {
		setValue(value, false);
	}

	/**
	 * Checks or unchecks the check box, firing {@link ValueChangeEvent} if appropriate.
	 * <p>
	 * Note that this <em>does not</em> set the value property of the checkbox input element wrapped by this widget. For
	 * access to that property, see {@link #setFormValue(String)}
	 * 
	 * @param value
	 *            true to check, false to uncheck; null value implies false
	 * @param fireEvents
	 *            If true, and value has changed, fire a {@link ValueChangeEvent}
	 */
	@Override
	public void setValue(Boolean value, boolean fireEvents) {
		if (value == null) {
			value = Boolean.FALSE;
		}

		Boolean oldValue = getValue();
		inputElem.setChecked(value);
		inputElem.setDefaultChecked(value);
		if (value.equals(oldValue)) {
			return;
		}
		if (fireEvents) {
			ValueChangeEvent.fire(this, value);
		}
	}

	// Unlike other widgets the CheckBox sinks on its inputElement, not
	// its wrapper
	@Override
	public void sinkEvents(int eventBitsToAdd) {
		if (isOrWasAttached()) {
			Event.sinkEvents(inputElem, eventBitsToAdd | Event.getEventsSunk(inputElem));
		} else {
			super.sinkEvents(eventBitsToAdd);
		}
	}

	protected void ensureDomEventHandlers() {
		addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// Checkboxes always toggle their value, no need to compare
				// with old value. Radio buttons are not so lucky, see
				// overrides in RadioButton
				ValueChangeEvent.fire(CleanCheckBox.this, getValue());
			}
		});
	}

	/**
	 * <b>Affected Elements:</b>
	 * <ul>
	 * <li>-label = label next to checkbox.</li>
	 * </ul>
	 * 
	 * @see UIObject#onEnsureDebugId(String)
	 */
	@Override
	protected void onEnsureDebugId(String baseID) {
		super.onEnsureDebugId(baseID);
		ensureDebugId(inputElem, baseID, "input");
	}

	/**
	 * This method is called when a widget is attached to the browser's document. onAttach needs special handling for
	 * the CheckBox case. Must still call {@link Widget#onAttach()} to preserve the <code>onAttach</code> contract.
	 */
	@Override
	protected void onLoad() {
		setEventListener(inputElem, this);
	}

	/**
	 * This method is called when a widget is detached from the browser's document. Overridden because of IE bug that
	 * throws away checked state and in order to clear the event listener off of the <code>inputElem</code>.
	 */
	@Override
	protected void onUnload() {
		// Clear out the inputElem's event listener (breaking the circular
		// reference between it and the widget).
		setEventListener(asOld(inputElem), null);
		setValue(getValue());
	}

	/**
	 * Replace the current input element with a new one. Preserves all state except for the name property, for nasty
	 * reasons related to radio button grouping. (See implementation of {@link RadioButton#setName}.)
	 * 
	 * @param elem
	 *            the new input element
	 */
	protected void replaceInputElement(Element elem) {
		InputElement newInputElem = InputElement.as(elem);
		// Collect information we need to set
		int tabIndex = getTabIndex();
		boolean checked = getValue();
		boolean enabled = isEnabled();
		String formValue = getFormValue();
		String uid = inputElem.getId();
		String accessKey = inputElem.getAccessKey();
		int sunkEvents = Event.getEventsSunk(inputElem);

		// Clear out the old input element
		setEventListener(asOld(inputElem), null);

		getElement().replaceChild(newInputElem, inputElem);

		// Sink events on the new element
		Event.sinkEvents(elem, Event.getEventsSunk(inputElem));
		Event.sinkEvents(inputElem, 0);
		inputElem = newInputElem;

		// Setup the new element
		Event.sinkEvents(inputElem, sunkEvents);
		inputElem.setId(uid);
		if (!"".equals(accessKey)) {
			inputElem.setAccessKey(accessKey);
		}
		setTabIndex(tabIndex);
		setValue(checked);
		setEnabled(enabled);
		setFormValue(formValue);

		// Set the event listener
		if (isAttached()) {
			setEventListener(asOld(inputElem), this);
		}
	}

	private Element asOld(com.google.gwt.dom.client.Element elem) {
		Element oldSchool = elem.cast();
		return oldSchool;
	}

	private void setEventListener(com.google.gwt.dom.client.Element e, EventListener listener) {
		DOM.setEventListener(asOld(e), listener);
	}
}
