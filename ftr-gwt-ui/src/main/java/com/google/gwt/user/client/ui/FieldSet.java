package com.google.gwt.user.client.ui;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.user.client.DOM;

/**
 * This class emulates an FieldSet in html.
 * 
 * @author mnouwens
 *
 */
public class FieldSet extends ComplexPanel implements InsertPanel.ForIsWidget {
	/**
	 * Creates an empty flow panel.
	 */
	public FieldSet() {
		setElement((Element) DOM.createFieldSet());
	}

	/**
	 * Adds a new child widget to the panel.
	 * 
	 * @param w
	 *            the widget to be added
	 */
	@Override
	public void add(final Widget w) {
		add(w, (Element) getElement());
	}

	@Override
	public void clear() {
		try {
			doLogicalClear();
		} finally {
			// Remove all existing child nodes.
			Node child = getElement().getFirstChild();
			while (child != null) {
				getElement().removeChild(child);
				child = getElement().getFirstChild();
			}
		}
	}

	@Override
	public void insert(final IsWidget w, final int beforeIndex) {
		insert(asWidgetOrNull(w), beforeIndex);
	}

	/**
	 * Inserts a widget before the specified index.
	 * 
	 * @param w
	 *            the widget to be inserted
	 * @param beforeIndex
	 *            the index before which it will be inserted
	 * @throws IndexOutOfBoundsException
	 *             if <code>beforeIndex</code> is out of range
	 */
	@Override
	public void insert(final Widget w, final int beforeIndex) {
		insert(w, (Element) getElement(), beforeIndex, true);
	}

	public void setFor(final String forField) {
		getElement().setAttribute("for", forField);
	}

}
