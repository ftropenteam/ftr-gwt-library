package eu.future.earth.gwt.client.ui.panels;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.*;
import eu.future.earth.gwt.client.logic.UUID;
import eu.future.earth.gwt.client.ui.ImageResources;
import eu.future.earth.gwt.client.ui.button.TouchButton;
import eu.future.earth.gwt.client.ui.general.UiUtil;

import java.util.Iterator;

public class PlaceHolderLabelFieldPanel extends CommonBasedPanel {

    private FlowPanel holderGrid = null;

    private final InlineLabel requiredLabel = new InlineLabel(ImageResources.STATIC_TEXT.requiredFields());

    public void setRequiredMessage(Field birthDayRef) {
        birthDayRef.getLabelFieldHolder().setFeedbackMessage(birthDayRef, "" + birthDayRef.getLabel());
    }

    public PlaceHolderLabelFieldPanel() {
        final FlowPanel main = new FlowPanel();
        holderGrid = new FlowPanel();
        main.add(holderGrid);
        main.add(requiredLabel);
        initWidget(holderGrid);
        requiredLabel.setVisible(false);
    }

    private void addAndSetStyle(final Widget item) {
        holderGrid.add(item);
        if (item instanceof TouchButton || item instanceof NoSizingWanted) {

        } else {
            item.getElement().getStyle().setMargin(0, Unit.PX);
            item.getElement().getStyle().setPadding(0, Unit.PX);
            item.getElement().getStyle().setPaddingLeft(2, Unit.PX);
            item.getElement().getStyle().setMarginTop(3, Unit.PX);
            item.getElement().getStyle().setMarginBottom(3, Unit.PX);
            item.getElement().getStyle().setFontSize(13, Unit.PX);
        }
        if (item instanceof TouchButton || item instanceof TextArea || item instanceof NoSizingWanted) {
            if (item instanceof TextArea) {
                item.getElement().getStyle().setWidth(100, Unit.PCT);
                item.getElement().getStyle().setHeight(64, Unit.PX);
            }
            if (item instanceof TouchButton) {

            }
        } else {
            item.getElement().getStyle().setHeight(32, Unit.PX);
            item.getElement().getStyle().setWidth(100, Unit.PCT);
        }

    }

    @Override
    protected void addFieldInternal(final Field real) {
        addFieldInternal(real, null);
    }

    @Override
    protected void addFieldInternal(final Field real, Field below) {
        FlowPanel wrap = new FlowPanel();

        if (real.getWidget() != null) {
            if (real.getWidget() instanceof NeedsLabel) {
                Label label = new Label(real.getLabel());
                label.setStyleName("inline-label");
                wrap.add(label);
            }
            if (real.getWidget() instanceof PlaceHolderWidget) {
                PlaceHolderWidget placeHolderWidget = (PlaceHolderWidget) real.getWidget();
                placeHolderWidget.setPlaceholder(real.getLabel());
            }
            wrap.add(real.getWidget());
            setElements(real.getId(), null, wrap);
            addAndSetStyle(wrap);
            if (real.getLabel() != null) {
                UiUtil.setPlaceHolder(real.getWidget(), real.getLabel());
            }
        }
    }

    @Override
    public Field addTwoRows(final Field real) {
        if (real.getId() == null) {
            real.setId(UUID.uuid());
        }
        if (real.getLabel() != null) {
            UiUtil.setPlaceHolder(real.getManager(), real.getLabel());
        }
        FlowPanel wrap = new FlowPanel();
        if (real.getLabel() != null) {
            if (real.getWidget() instanceof NeedsLabel) {
                Label label = new Label(real.getLabel());
                label.setStyleName("inline-label");
                wrap.add(label);
            }
            if (real.getWidget() instanceof PlaceHolderWidget) {
                PlaceHolderWidget placeHolderWidget = (PlaceHolderWidget) real.getWidget();
                placeHolderWidget.setPlaceholder(real.getLabel());
            }
        }
        wrap.add(real.getWidget());
        addAndSetStyle(wrap);
        setElements(real.getId(), null, wrap);


        return real;
    }

    @Override
    public void clear() {
        holderGrid.clear();
    }

    public void clearButtons() {
        holderGrid.clear();
    }

    @Override
    public String getLabel(final String name) {
        return UiUtil.getPlaceHolder(getEdit(name));
    }

    @Override
    public Iterator<Widget> iterator() {
        return holderGrid.iterator();
    }

    @Override
    public boolean remove(final Widget w) {
        return holderGrid.remove(w);
    }

    @Override
    public void setError(final String name, final boolean newState) {
        if (name != null) {
            super.setErrorForLabel(name, newState);
        }
    }

    @Override
    public void setLabel(final String name, final String newText) {
        UiUtil.setPlaceHolder(getEdit(name), newText);
    }

    @Override
    public void setRequired(final String name, final boolean newState) {
        if (name != null) {
            super.setRequiredForLabel(name, newState);
            final Widget lbl = super.getEdit(name);
            if (lbl != null) {
                final String pla = UiUtil.getPlaceHolder(lbl);
                if (pla != null) {
                    if (pla.contains("*")) {
                        if (!newState) {
                            UiUtil.setPlaceHolder(lbl, pla.replace(" *", ""));
                        }
                    } else {
                        if (newState) {
                            UiUtil.setPlaceHolder(lbl, pla + " *");

                        }
                    }
                }
            }
            checkRequiredLegendas();
        }
    }

    public void setRequiredAndVisible(final String name, final boolean newState) {
        setRequired(name, newState);
        setVisible(name, newState);
    }

    @Override
    public void setVisible(final String name, final boolean newState) {
        if (name != null) {
            setVisibleElements(name, newState);
        }
    }

    @Override
    public void setWordWrapForLabel(final String labelName, final boolean b) {

    }

    @Override
    public void showFilled() {
        final Iterator<String> labelsName = labels.keySet().iterator();
        while (labelsName.hasNext()) {
            final String labelName = labelsName.next();
            final Widget item = edits.get(labelName);
            if (item instanceof HasText) {
                final HasText real = (HasText) item;
                if (real.getText() != null && real.getText().length() > 0) {
                    setVisible(labelName, true);
                } else {
                    setVisible(labelName, false);
                }
            } else {

            }
        }
    }

    @Override
    public void showRequired(final boolean hasRequired) {
        requiredLabel.setVisible(hasRequired);
    }
}
