package eu.future.earth.gwt.client.ui.panels;

import java.util.HashMap;import java.util.Map;

public abstract class CommonBasedPanel extends AbstractLabelFieldPanel {
	private final Map<String, FeedbackHolder> feedbackPanels = new HashMap<>();

	@Override
	public void clearFeedback() {
		for (final FeedbackHolder string : feedbackPanels.values()) {
			string.clearError();
		}
	}

	public void setFeedbackHolder(String id, FeedbackHolder holder) {
		feedbackPanels.put(id, holder);
	}

	@Override
	public void setFeedbackMessage(final String key, final String newFeedback) {
		final FeedbackHolder feedback = feedbackPanels.get(key);
		if (feedback != null) {
			feedback.setError(newFeedback);
		}
	}
}
