package eu.future.earth.gwt.client.formfactor;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.Navigator;

public class DeviceProvider {

	public DeviceForm getDeviceType() {
		final String agent = Navigator.getUserAgent().toLowerCase();
		if (agent.indexOf("phone") > -1) {
			return DeviceForm.Phone;
		}
		if (agent.indexOf("ipod") > -1) {
			return DeviceForm.Phone;
		}
		if (agent.indexOf("ipad") > -1) {
			return DeviceForm.Tablet;
		}
		if (agent.indexOf("android") > -1 || agent.indexOf("bb10") > -1 || agent.indexOf("nokia") > -1) {

			if (agent.indexOf("phone") > -1) {
				return DeviceForm.Phone;
			} else {
				final int min = Math.min(Window.getClientHeight(), Window.getClientWidth());
				if (min < 500) {
					return DeviceForm.Phone;
				}
				return DeviceForm.Tablet;
			}
		}
		if (agent.indexOf("tablet") > -1) {
			return DeviceForm.Tablet;
		}

		return DeviceForm.Desktop;
	}

}
