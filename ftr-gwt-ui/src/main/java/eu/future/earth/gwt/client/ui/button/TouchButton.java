package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasBlurHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.ui.event.ClickTouchEvent;
import eu.future.earth.gwt.client.ui.event.ClickTouchHandler;
import eu.future.earth.gwt.client.ui.event.HasClickTouchHandlers;

public class TouchButton extends Composite implements Focusable, HasClickTouchHandlers, HasBlurHandlers {

	protected FocusPanel main = new FocusPanel();

	protected InlineLabel text = new InlineLabel();

	private Element imagePlaced = null;

	private boolean enabled = true;

	@UiConstructor
	public TouchButton() {
		this(null, null);
	}

	public TouchButton(final Image newImage) {
		this(null, newImage);
	}

	public TouchButton(final String newText) {
		this(newText, null);
	}

	public TouchButton(final String newText, final Image newImage) {
		super();
		initWidget(main);
		super.addStyleName("btn");
		text.removeStyleName("gwt-InlineLabel");
		main.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(final ClickEvent event) {
				if (enabled) {
					handleClick();
				}
			}

		});
		setImage(newImage);
		if (newText != null) {
			main.getElement().appendChild(text.getElement());
			text.setText(newText);
		}
	}

	@Override
	public HandlerRegistration addClickHandler(final ClickTouchHandler handler) {
		return super.addHandler(handler, ClickTouchEvent.getType());
	}

	public void addStyleNameToMain(final String newStyle) {
		main.addStyleName(newStyle);
	}

	@Override
	public int getTabIndex() {
		return main.getTabIndex();
	}

	public void handleClick() {
		ClickTouchEvent.fire(this);
	}

	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void onBrowserEvent(final Event event) {
		if (enabled) {
			// if (DOM.eventGetType(event) == Event.ONCLICK) {
			//
			// }
			super.onBrowserEvent(event);
			// handleClick();
		} else {
			event.preventDefault();
		}
	}

	protected void removeStyleNameToMain(final String newStyle) {
		main.removeStyleName(newStyle);
	}

	@Override
	public void setAccessKey(final char key) {
		main.setAccessKey(key);
	}

	public void setEnabled(final boolean newState) {
		if (enabled != newState) {
			if (enabled) {
				super.addStyleName("disabled");
			} else {
				super.removeStyleName("disabled");
			}
			enabled = newState;

		}
	}

	@Override
	public void setFocus(final boolean focused) {
		main.setFocus(focused);
	}

	public void setImage(final Widget newImage) {
		if (imagePlaced != null) {
			main.getElement().removeChild(imagePlaced);
		}

		if (newImage != null) {
			imagePlaced = newImage.getElement();
			main.getElement().insertFirst(imagePlaced);
		}
	}

	@Override
	public void setTabIndex(final int index) {
		main.setTabIndex(index);
	}

	public void setText(final String newText) {
		text.setText(newText);
		if (!main.getElement().isOrHasChild(text.getElement())) {
			main.getElement().appendChild(text.getElement());
		}
	}

	@Override
	public HandlerRegistration addBlurHandler(BlurHandler handler) {
		return super.addHandler(handler, BlurEvent.getType());
	}

}
