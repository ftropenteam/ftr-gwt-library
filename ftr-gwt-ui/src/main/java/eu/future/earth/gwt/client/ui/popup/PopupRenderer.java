package eu.future.earth.gwt.client.ui.popup;



public interface PopupRenderer<D> extends IdRenderer<D> {

    String getLabel(D item);

    
    
}
