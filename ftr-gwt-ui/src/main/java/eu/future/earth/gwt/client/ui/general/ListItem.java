package eu.future.earth.gwt.client.ui.general;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;

public class ListItem extends ComplexPanel implements HasText, HasClickHandlers, HasEnabled {

	private boolean init = true;

	private boolean enabled = true;

	public ListItem() {
		setElement(Document.get().createLIElement());
	}

	public ListItem(final Widget newItem) {
		setElement(Document.get().createLIElement());
		add(newItem);

	}

	@Override
	public void add(final Widget w) {
		super.add(w, (Element) getElement());
	}

	@Override
	public HandlerRegistration addClickHandler(final ClickHandler handler) {
		if (init) {
			DOM.sinkEvents(getElement(), Event.ONCLICK);
			DOM.setEventListener(getElement(), new EventListener() {
				@Override
				public void onBrowserEvent(final Event event) {
					if (enabled) {
						fireClick(event);
					} else {
						event.preventDefault();
					}
				}
			});
			init = false;
		}
		return super.addHandler(handler, ClickEvent.getType());
	}

	private void fireClick(final Event event) {
		if (enabled) {
			DomEvent.fireNativeEvent(event, this);
		}
	}

	@Override
	public String getText() {
		return getElement().getInnerText();
	}

	public void insert(final Widget w, final int beforeIndex) {
		super.insert(w, (Element) getElement(), beforeIndex, true);
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void onBrowserEvent(final Event event) {
		if (enabled) {
			super.onBrowserEvent(event);
		} else {
			event.preventDefault();
		}
	}

	@Override
	public void setEnabled(final boolean newEnabled) {
		enabled = newEnabled;
	}

	@Override
	public void setText(final String text) {
		getElement().setInnerText(text);
	}

}
