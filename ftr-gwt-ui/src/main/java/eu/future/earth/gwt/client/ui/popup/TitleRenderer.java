package eu.future.earth.gwt.client.ui.popup;



public interface TitleRenderer<D> {

    String getTitle(D item);
    
}
