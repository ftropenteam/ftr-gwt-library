package eu.future.earth.gwt.client.ui.events.dataentry;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

public interface HasDataEntryEventHandlers extends HasHandlers {

	HandlerRegistration addDataEntryHandler(DataEntryHandler handler);

}
