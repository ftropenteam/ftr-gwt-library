package eu.future.earth.gwt.client.ui.button;

public class FontAwsomeIconCodes {

	public static final String LINK = "f0c1";

	public static final String UN_LINK = "f127";

	public static final String FA_PLUS = "f067";
	public static final String FA_MINUS = "f068";
	public static final String FA_QUESTION = "3f";


	public static final String FA_TRASH_O = "f014";

	public static final String FA_REFRESH = "f021";

	public static final String FA_ARROW_CIRCLE_O_LEFT = "f190";

	public static final String FA_ARROW_CIRCLE_O_RIGHT = "f18e";

	public static final String FA_QUESTION_CIRCLE_O = "f29c";

	public static final String FA_USER = "f096";

	public static final String UN_SELECTED = "f096";

	public static final String SELECTED = "f046";

	public static final String FA_STOP_CIRCLE_O = "f28e";

	public static final String FA_SORT_UP = "f0de"; // Up

	public static final String FA_SORT_DOWN = "f0dd"; // Down

	public static final String FA_CARET_SQUARE_O_UP = "f151"; // Up

	public static final String FA_CARET_SQUARE_O_DOWN = "f150"; // Down

	public static final String SAVE = "f0c7"; // Save

	public static final String SEARCH_ADD = "f00e"; // Save

	public static final String FA_CHECK = "f00c";


	public static final String SQUARE = "f0c8";
	public static final String SQUARE_CHECK = "f14a";


	public static final String FA_FILE_EXCEL_O = "f1c3";

	public static final String FA_FILE_PDF_O = "f1c1";

	public static final String FA_PLAY = "f04b";

	public static final String CALENDAR = "f073";

	public static final String DOWNLOAD = "f019";

	public static final String TIMES = "f00d";

	public static final String SEARCH = "f002";

	public static final String THUMBS_O_UP = "f087";

	public static final String REFRESH = "f021";

	public static final String HEARTBEAT = "f21e";

	public static final String EDIT = "f044";

	public static final String CLOSE = "f00d";

	public static final String CARET_DOWN = "f0d7";

	public static final String CARET_UP = "f0d8";

	public static final String FA_CARET_SQUARE_O_RIGHT = "f152";
	public static final String FA_CARET_SQUARE_O_LEFT = "f191";


	public static final String ARROW_LEFT = "f060";
	public static final String ARROW_RIGHT = "f061";
	public static final String ARROW_UP = "f062";
	public static final String ARROW_DOWN = "f063";

}
