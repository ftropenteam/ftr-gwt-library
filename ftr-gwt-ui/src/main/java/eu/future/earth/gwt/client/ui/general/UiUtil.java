package eu.future.earth.gwt.client.ui.general;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;
import eu.future.earth.gwt.client.ui.log.SystemAnalyzer;

public class UiUtil {

    private static UiUtil instance = new UiUtil();

    public static UiUtil getInstance() {
        return instance;
    }

    public void scrollIntoViewTopOnly(Widget elem) {
        scrollIntoViewTopOnlyIntern(elem.getElement());
    }

    public native void scrollIntoViewTopOnlyIntern(Element elem) /*-{
        var left = elem.offsetLeft;
        var top = elem.offsetTop;
        var width = elem.offsetWidth;
        var height = elem.offsetHeight;

        if (elem.parentNode != elem.offsetParent) {
            left -= elem.parentNode.offsetLeft;
            top -= elem.parentNode.offsetTop;
        }

        var cur = elem.parentNode;
        while (cur && (cur.nodeType == 1)) {
            if (left < cur.scrollLeft) {
                //cur.scrollLeft = left;
            }
            if (left + width > cur.scrollLeft + cur.clientWidth) {
                //cur.scrollLeft = (left + width) - cur.clientWidth;
            }
            if (top < cur.scrollTop) {
                cur.scrollTop = top;
            }
            if (top + height > cur.scrollTop + cur.clientHeight) {
                cur.scrollTop = (top + height) - cur.clientHeight;
            }

            var offsetLeft = cur.offsetLeft, offsetTop = cur.offsetTop;
            if (cur.parentNode != cur.offsetParent) {
                //offsetLeft -= cur.parentNode.offsetLeft;
                offsetTop -= cur.parentNode.offsetTop;
            }

            //left += offsetLeft - cur.scrollLeft;
            top += offsetTop - cur.scrollTop;
            cur = cur.parentNode;
        }
    }-*/;

    public static void setPlaceHolder(Widget item, String name) {
        if (item.getElement() != null) {
            item.getElement().setAttribute("placeholder", name);
        }
    }

    public static String getPlaceHolder(Widget item) {
        return item.getElement().getAttribute("placeholder");
    }

    public static void setId(Widget parent, Widget widget, String id) {
        String parentId = parent.getElement().getId();
        if (parentId != null && !parentId.isEmpty()) {
            setId(widget, parentId + ":" + id);
        } else {
            setId(widget, id);
        }
    }

    public static void setId(String parentId, Widget widget, String id) {
        if (parentId != null && !parentId.isEmpty()) {
            setId(widget, parentId + ":" + id);
        } else {
            setId(widget, id);
        }
    }

    public static void setId(Widget widget, String id) {
        if (widget != null) {
            if (widget instanceof IdUiObject) {
                IdUiObject idImplementer = (IdUiObject) widget;
                idImplementer.setId(id);
            } else {
                widget.getElement().setId(id);
            }
        }
    }

    public static void setUiIdentifier(Widget widget, String id) {
        if (widget != null) {
            widget.getElement().setAttribute("uiidentifier", id);
        }
    }

    public static void setBorderRight(Widget top) {
        // Set rightBorder
        CssHelper.setProperty(top, CssHelper.A.BORDER_RIGHT_COLOR, "#cacaca");
        // CSS.setProperty(top, CssHelper.PADDING_RIGHT, "5px");
        CssHelper.setProperty(top, CssHelper.A.BORDER_RIGHT_WIDTH, "2px");
        CssHelper.setProperty(top, CssHelper.A.BORDER_RIGHT_STYLE, BorderStyle.SOLID.getCssName());
    }

    public static void setBorderBottum(Widget top) {
        // Set rightBorder
        CssHelper.setProperty(top, CssHelper.A.BORDER_BOTTOM_COLOR, "#cacaca");
        CssHelper.setProperty(top, CssHelper.A.BORDER_BOTTOM_WIDTH, "2px");
        CssHelper.setProperty(top, CssHelper.A.BORDER_BOTTOM_STYLE, BorderStyle.SOLID.getCssName());
    }

    public static void setBorderLeft(Widget top) {
        CssHelper.setProperty(top, CssHelper.A.BORDER_LEFT_COLOR, "#cacaca");
        CssHelper.setProperty(top, CssHelper.A.BORDER_LEFT_WIDTH, "2px");
        CssHelper.setProperty(top, CssHelper.A.BORDER_LEFT_STYLE, BorderStyle.SOLID.getCssName());
    }

    public static void setBorderTop(Widget top) {
        CssHelper.setProperty(top, CssHelper.A.BORDER_TOP_COLOR, "#cacaca");
        CssHelper.setProperty(top, CssHelper.A.BORDER_TOP_WIDTH, "2px");
        CssHelper.setProperty(top, CssHelper.A.BORDER_TOP_STYLE, BorderStyle.SOLID.getCssName());

    }

    public static String getTextOnWidget(Widget to) {
        if (to instanceof HasText) {
            HasText re = (HasText) to;
            return re.getText();
        }
        return null;
    }

    public static void setBorder(Widget top, String color) {
        setBorderBottum(top, 2, color);
        setBorderTop(top, 2, color);
        setBorderRight(top, 2, color);
        setBorderLeft(top, 2, color);
    }

    public static void setBorder(Widget top, int size, String color) {
        setBorderBottum(top, size, color);
        setBorderTop(top, size, color);
        setBorderRight(top, size, color);
        setBorderLeft(top, size, color);
    }

    public static void setBorderRight(Widget top, int size, String color) {
        CssHelper.setProperty(top, CssHelper.A.BORDER_RIGHT_COLOR, color);
        CssHelper.setProperty(top, CssHelper.A.BORDER_RIGHT_WIDTH, size + "px");
        CssHelper.setProperty(top, CssHelper.A.BORDER_RIGHT_STYLE, BorderStyle.SOLID.getCssName());
    }

    public static void setBorderBottum(Widget top, int size, String color) {
        CssHelper.setProperty(top, CssHelper.A.BORDER_BOTTOM_COLOR, color);
        CssHelper.setProperty(top, CssHelper.A.BORDER_BOTTOM_WIDTH, size + "px");
        CssHelper.setProperty(top, CssHelper.A.BORDER_BOTTOM_STYLE, BorderStyle.SOLID.getCssName());
    }

    public static void setBorderLeft(Widget top, int size, String color) {
        CssHelper.setProperty(top, CssHelper.A.BORDER_LEFT_COLOR, color);
        CssHelper.setProperty(top, CssHelper.A.BORDER_LEFT_WIDTH, size + "px");
        CssHelper.setProperty(top, CssHelper.A.BORDER_LEFT_STYLE, BorderStyle.SOLID.getCssName());
    }

    public static void setBorderTop(Widget top, int size, String color) {
        // Set rightBorder
        CssHelper.setProperty(top, CssHelper.A.BORDER_TOP_COLOR, color);
        CssHelper.setProperty(top, CssHelper.A.BORDER_TOP_WIDTH, size + "px");
        CssHelper.setProperty(top, CssHelper.A.BORDER_TOP_STYLE, BorderStyle.SOLID.getCssName());

    }

}
