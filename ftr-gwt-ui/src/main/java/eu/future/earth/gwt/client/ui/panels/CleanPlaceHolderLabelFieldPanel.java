package eu.future.earth.gwt.client.ui.panels;

import com.google.gwt.user.client.ui.*;
import eu.future.earth.gwt.client.logic.UUID;
import eu.future.earth.gwt.client.ui.ImageResources;
import eu.future.earth.gwt.client.ui.general.UiUtil;

import java.util.Iterator;

public class CleanPlaceHolderLabelFieldPanel extends CommonBasedPanel {

    private final FlowPanel holderGrid;

    private final InlineLabel requiredLabel = new InlineLabel(ImageResources.STATIC_TEXT.requiredFields());

    public CleanPlaceHolderLabelFieldPanel() {
        final FlowPanel main = new FlowPanel();
        holderGrid = new FlowPanel();
        holderGrid.setStyleName("clean-inline-form");
        main.setStyleName("clean-inline-form-main");
        main.add(holderGrid);
        main.add(requiredLabel);
        initWidget(main);
        requiredLabel.setVisible(false);
    }

    public void setRequiredMessage(Field birthDayRef) {
        birthDayRef.getLabelFieldHolder().setFeedbackMessage(birthDayRef, birthDayRef.getLabel());
    }


    @Override
    protected void addFieldInternal(final Field real) {
        addFieldInternal(real, null);
    }

    @Override
    protected void addFieldInternal(final Field real, Field below) {
        FlowPanel row = new FlowPanel();
        row.setStyleName("label-row");
        FlowPanel wrap = new FlowPanel();
        wrap.setStyleName("label-wrap");
        if (real.getLabel() != null) {
            if (real.getWidget() instanceof NeedsLabel) {
                Label label = new Label(real.getLabel());
                label.setStyleName("inline-label");
                wrap.add(label);
            }
            if(real.getWidget() instanceof PlaceHolderWidget){
                PlaceHolderWidget placeHolderWidget = (PlaceHolderWidget) real.getWidget();
                placeHolderWidget.setPlaceholder(real.getLabel());
            }
        }

        wrap.add(real.getWidget());
        {
            final FeedbackHolder errorHolder = new FeedbackHolder();
            super.setFeedbackHolder(real.getId(), errorHolder);
            wrap.add(errorHolder);
        }
        row.add(wrap);
        setElements(real.getId(), null, row);
        if (real.getWidget() != null) {
            addAndSetStyle(row);
            if (real.getLabel() != null) {
                UiUtil.setPlaceHolder(real.getWidget(), real.getLabel());
            }

        }
    }


    @Override
    public Field addTwoRows(final Field real) {
        if (real.getId() == null) {
            real.setId(UUID.uuid());
        }
        if (real.getLabel() != null) {
            UiUtil.setPlaceHolder(real.getManager(), real.getLabel());
        }

        FlowPanel row = new FlowPanel();
        row.setStyleName("label-row");
        FlowPanel wrap = new FlowPanel();
        wrap.setStyleName("label-wrap");
        if (real.getLabel() != null) {
            if (real.getWidget() instanceof NeedsLabel) {
                Label label = new Label(real.getLabel());
                label.setStyleName("inline-label");
                wrap.add(label);
            }
            if(real.getWidget() instanceof PlaceHolderWidget){
              PlaceHolderWidget placeHolderWidget = (PlaceHolderWidget) real.getWidget();
              placeHolderWidget.setPlaceholder(real.getLabel());
            }
        }
        wrap.add(real.getWidget());
        {
            final FeedbackHolder errorHolder = new FeedbackHolder();
            super.setFeedbackHolder(real.getId(), errorHolder);
            wrap.add(errorHolder);
        }
        addAndSetStyle(row);
        row.add(wrap);
        setElements(real.getId(), null, real);


        return real;
    }


    private void addAndSetStyle(final FlowPanel item) {
        item.setStyleName("row");
        holderGrid.add(item);
    }
    @Override
    public void clear() {
        holderGrid.clear();
    }

    public void clearButtons() {
        holderGrid.clear();
    }

    @Override
    public String getLabel(final String name) {
        return UiUtil.getPlaceHolder(getEdit(name));
    }

    @Override
    public Iterator<Widget> iterator() {
        return holderGrid.iterator();
    }

    @Override
    public boolean remove(final Widget w) {
        return holderGrid.remove(w);
    }

    @Override
    public void setError(final String name, final boolean newState) {
        if (name != null) {
            super.setErrorForLabel(name, newState);
        }
    }

    @Override
    public void setLabel(final String name, final String newText) {
        UiUtil.setPlaceHolder(getEdit(name), newText);
    }

    @Override
    public void setRequired(final String name, final boolean newState) {
        if (name != null) {
            super.setRequiredForLabel(name, newState);
            checkRequiredLegendas();
        }
    }

    @Override
    public void setVisible(final String name, final boolean newState) {
        if (name != null) {
            setVisibleElements(name, newState);
        }
    }

    @Override
    public void setWordWrapForLabel(final String labelName, final boolean b) {

    }

    @Override
    public void showFilled() {
        for (String labelName : labels.keySet()) {
            final Widget item = edits.get(labelName);
            if (item instanceof HasText) {
                final HasText real = (HasText) item;
                if (real.getText() != null && !real.getText().isEmpty()) {
                    setVisible(labelName, true);
                } else {
                    setVisible(labelName, false);
                }
            }
        }
    }

    @Override
    public void showRequired(final boolean hasRequired) {
        requiredLabel.setVisible(hasRequired);
    }
}
