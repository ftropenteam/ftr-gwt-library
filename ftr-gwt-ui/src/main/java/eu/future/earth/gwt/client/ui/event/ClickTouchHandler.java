package eu.future.earth.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;

public interface ClickTouchHandler extends EventHandler {

	void onClick(ClickTouchEvent naviEvent);

}
