package eu.future.earth.gwt.client.ui.general;

public class EeGwtFrameworkCss {

    public static final String PREFIX = "Ftr";

    public static final String REQUIRED_TEXT_FIELD = PREFIX + "Required";

    public static final String REQUIRED_LABEL = PREFIX + "RequiredLabel";

    public static final String REQUIRED_INDICATOR = PREFIX + "RequiredIndicator";

    public static final String ERROR_TEXT_FIELD = PREFIX + "Error";

    public static final String ERROR_LABEL = PREFIX + "ErrorLabel";

    public static final String ERROR_INDICATOR = PREFIX + "ErrorIndicator";

}
