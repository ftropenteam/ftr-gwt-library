package eu.future.earth.gwt.client.ui.popup;

import java.util.ArrayList;
import java.util.HashMap;import java.util.Map;
import java.util.List;

import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;

import eu.future.earth.gwt.client.ui.ImageResources;
import eu.future.earth.gwt.client.ui.button.CleanPopupPanel;
import eu.future.earth.gwt.client.ui.button.ValueListItem;
import eu.future.earth.gwt.client.ui.general.UiUtil;
import eu.future.earth.gwt.client.ui.general.UnOrderedList;

public class SelectPopup<D> extends CleanPopupPanel implements HasValueChangeHandlers<D>, ClickHandler {

	private static final String NONE = "NONE";

	private final PopupRenderer<D> renderer;

	private IconRenderer<D> imageRenderer = null;

	private final UnOrderedList ul = new UnOrderedList();

	private boolean allowDeselect = false;

	private List<D> choices = null;

	private final Map<D, ValueListItem<D>> ref = new HashMap<D, ValueListItem<D>>();

	public D selected = null;

	private String selectedKey = NONE;

	private ValueListItem<D> noneSelectedRow = null;


	public SelectPopup(final PopupRenderer<D> newRenderer) {
		super(true);
		renderer = newRenderer;
		if (renderer instanceof IconRenderer<?>) {
			imageRenderer = (IconRenderer<D>) renderer;
		}
		inittWidget();
	}

	public void addAllowDeselect() {
		this.allowDeselect = true;
		if (noneSelectedRow == null) {
			noneSelectedRow = new ValueListItem<D>();
			if (imageRenderer != null) {
				// noneSelectedRow.add(new Image(ImageResources.s24.no()));
			}
			noneSelectedRow.add(new Label(ImageResources.STATIC_TEXT.nothingSelected()));
			noneSelectedRow.addClickHandler(this);
		}
		ul.add(noneSelectedRow);
	}

	public void addValue(final D d) {
		if (choices == null) {
			choices = new ArrayList<D>();
		}
		choices.add(d);
		addValueInternal(d);
	}

	@Override
	public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<D> handler) {
		return super.addHandler(handler, ValueChangeEvent.getType());
	}

	private void addValueInternal(final D d) {
		final ValueListItem<D> newRow = new ValueListItem<D>(d);
		if (imageRenderer != null) {
			newRow.add(imageRenderer.getImage(d));
		}
		ref.put(d, newRow);
		newRow.add(new Label(renderer.getLabel(d)));
		newRow.addClickHandler(this);
		// newRow.addDomHandler(this, ClickEvent.getType());
		ul.add(newRow);
	}

	public void clearItems() {
		setChoices(null);
	}

	public void deleteValue(final D d) {
		if (d == null) {
			return;
		}
		if (choices == null) {
			return;
		}
		final List<D> newChoices = new ArrayList<D>();
		for (final D da : choices) {
			if (!renderer.getId(da).equals(renderer.getId(d))) {
				newChoices.add(da);
			}
		}
		setChoices(newChoices);
	}

	public void focusSelected() {
		if (selected != null) {
			final ValueListItem<D> sel = ref.get(selected);
			if (sel != null) {
				// sel.getElement().focus();
			}
		}
	}

	public String getSelectedKey() {
		if (selected != null) {
			return renderer.getId(selected);
		}
		return NONE;
	}

	public D getValue() {
		return selected;
	}


	private void handelSelect(final Object source) {
		if (source instanceof ValueListItem) {
			final ValueListItem<D> real = (ValueListItem<D>) source;
			selected = real.getValue();
			setKeyValue();
			hide();
			ValueChangeEvent.fire(this, selected);
		}
	}

	public boolean hasChoices() {
		return numberChoices() > 0;
	}

	private void inittWidget() {
		final ScrollPanel scroller = new ScrollPanel(ul);
		add(scroller);
		setStyleName("ftr-dropdown");
		scroller.addStyleName("ul-selectable");
		ul.getElement().getStyle().setOverflowX(Overflow.HIDDEN);
		Event.addNativePreviewHandler(new NativePreviewHandler() {
			@Override
			public void onPreviewNativeEvent(final NativePreviewEvent event) {
				if (isAttached()) {
					final int type = event.getTypeInt();
					switch (type) {
					case Event.ONKEYDOWN: {
						final int keyCode = event.getNativeEvent().getKeyCode();
						switch (keyCode) {
						case KeyCodes.KEY_DOWN: {
							// Window.alert("" + keyCode);
							next();
							event.cancel();
							break;
						}
						case KeyCodes.KEY_UP: {
							// Window.alert("" + keyCode);
							prev();
							event.cancel();
							break;
						}
						case KeyCodes.KEY_ENTER: {
							// Window.alert("" + keyCode);
							hide();
							event.cancel();
							break;
						}
						}
					}

					}
				}
			}
		});
	}

	public boolean isAllowDeselect() {
		return allowDeselect;
	}

	private void next() {
		if (selected != null) {
			final int current = choices.indexOf(selected);
			if (current < choices.size() - 1) {
				final D newSelect = choices.get(current + 1);
				setValue(newSelect);

				scrollToValue();
				ValueChangeEvent.fire(this, selected);
			}
		}
	}

	public int numberChoices() {
		if (choices == null) {
			return 0;
		}
		return choices.size();
	}

	@Override
	public void onClick(final ClickEvent event) {
		handelSelect(event.getSource());
	}

	private void prev() {
		if (selected != null) {
			final int current = choices.indexOf(selected);
			if (current > 0) {
				setValue(choices.get(current - 1));
				scrollToValue();
			}
			ValueChangeEvent.fire(this, selected);
		}
	}

	public void scrollToValue() {
		if (selected != null) {
			final ValueListItem<D> sel = ref.get(selected);
			if (sel != null) {
				UiUtil.getInstance().scrollIntoViewTopOnly(sel);
			}
		}
	}

	public void setChoices(final List<D> newData) {
		choices = newData;
		ul.clear();
		ref.clear();
		if (isAllowDeselect()) {
			addAllowDeselect();
		}
		if (choices != null) {
			for (final D d : newData) {
				addValueInternal(d);
			}
		}
		setSelectedInternal();
	}

	private void setKeyValue() {
		if (selected != null) {
			selectedKey = renderer.getId(selected);
		} else {
			selectedKey = NONE;
		}
		setSelectedToButton();
	}

	private void setSelectedInternal() {
		if (selectedKey == null || selectedKey.equals(NONE)) {
			setValue(null);
		} else {
			if (choices != null) {
				for (final D sel : choices) {
					final String found = renderer.getId(sel);
					if (selectedKey.equals(found)) {
						setValue(sel);
					}
				}
			}
		}
	}

	private void setSelectedToButton() {
		for (final ValueListItem<D> remSelected : ref.values()) {
			remSelected.removeStyleName("selected");
		}
		if (noneSelectedRow != null) {
			noneSelectedRow.removeStyleName("selected");
		}

		if (selected == null) {
			if (noneSelectedRow != null) {
				noneSelectedRow.addStyleName("selected");
			}
		} else {
			if (imageRenderer != null) {
				// super.setImage(imageRenderer.getImage(selected));
			}
			final ValueListItem<D> fo = ref.get(selected);
			if (fo != null) {
				fo.addStyleName("selected");
			}
		}
	}

	public void setSelectionByKey(final String data) {
		selectedKey = data;
		if (choices != null && !choices.isEmpty()) {
			setSelectedInternal();
		}
	}

	public void setValue(final D newSelected) {
		this.selected = newSelected;
		setKeyValue();
	}

	public void tellParentChanged() {
		ValueChangeEvent.fire(this, selected);
	}

}
