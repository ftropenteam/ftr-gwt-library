package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.resources.client.ImageResource;

public interface ButtonImages {

	public abstract ImageResource back();

	public abstract ImageResource first();

	public abstract ImageResource next();

	public abstract ImageResource last();
	
	public abstract ImageResource no();

}