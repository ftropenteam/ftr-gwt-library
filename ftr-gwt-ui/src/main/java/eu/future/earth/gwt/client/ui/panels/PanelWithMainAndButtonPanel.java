package eu.future.earth.gwt.client.ui.panels;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class PanelWithMainAndButtonPanel extends Composite {

	private ButtonFlowPanelRight buttons = new ButtonFlowPanelRight();

	private FlowPanel dock = new FlowPanel();

	private SimplePanel sim = new SimplePanel();

	public PanelWithMainAndButtonPanel(final Widget all) {
		super();
		initWidget(dock);
		dock.add(sim);
		if (all != null) {
			sim.setWidget(all);
		}
		dock.add(buttons);
		dock.addStyleName("main");
		sim.setStyleName("main-content");
		// dock.setWidth("100%");
		// dock.setHeight("100%");
	}

	public void addButton(final Widget closeButton) {
		buttons.addButton(closeButton);
	}

	public void addLeft(final Widget closeButton, final String extraClassName) {
		if (extraClassName != null) {
			closeButton.addStyleName(extraClassName);
		}
		buttons.addLeft(closeButton);
	}

	public void setMain(final Widget newMain) {
		sim.clear();
		sim.setWidget(newMain);
	}

}
