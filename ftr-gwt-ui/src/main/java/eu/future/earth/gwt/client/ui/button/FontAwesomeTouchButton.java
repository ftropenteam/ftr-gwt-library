package eu.future.earth.gwt.client.ui.button;

import eu.future.earth.gwt.client.ui.event.ClickTouchHandler;

public class FontAwesomeTouchButton extends TouchButton {

	public FontAwesomeTouchButton() {
		super();
		super.addStyleName("awesome");
	}

	public FontAwesomeTouchButton(final String newText) {
		super();
		setSymbol(newText);
	}

	public FontAwesomeTouchButton(final String newText, final String textOnButton) {
		super(textOnButton);
		setSymbol(newText);
	}

	public FontAwesomeTouchButton(final String newText, final String textOnButton, final ClickTouchHandler newHandler) {
		super(textOnButton);
		setSymbol(newText);
		super.addClickHandler(newHandler);
	}

	public void setSymbol(final String newText) {
		if (newText != null) {
			if (!main.getElement().isOrHasChild(text.getElement())) {
				main.getElement().appendChild(text.getElement());
			}
			text.setStyleName("fontawesome-img-button");
			final int i = Integer.parseInt(newText, 16);
			final String news = Character.toString((char) i);
			text.getElement().setAttribute("symbol", news);
		}
		super.addStyleName("awesome");
	}

}
