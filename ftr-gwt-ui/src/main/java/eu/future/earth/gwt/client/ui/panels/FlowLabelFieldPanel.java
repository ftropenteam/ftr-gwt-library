package eu.future.earth.gwt.client.ui.panels;

import java.util.Iterator;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.logic.UUID;
import eu.future.earth.gwt.client.ui.ImageResources;
import eu.future.earth.gwt.client.ui.general.UiUtil;

public class FlowLabelFieldPanel extends CommonBasedPanel {

	private FlowPanel holderGrid = null;

	private final Label requiredLabel = new Label(ImageResources.STATIC_TEXT.requiredFields());
	public void setRequiredMessage(Field birthDayRef) {
		birthDayRef.getLabelFieldHolder().setFeedbackMessage(birthDayRef, "" + birthDayRef.getLabel());
	}
	@UiConstructor
	public FlowLabelFieldPanel() {
		final FlowPanel main = new FlowPanel();
		holderGrid = new FlowPanel();
		holderGrid.addStyleName("flow-info-panel");
		main.add(holderGrid);
		main.add(requiredLabel);

		initWidget(main);
		requiredLabel.setVisible(false);
	}

	@Override
	protected void addFieldInternal(final Field real) {
		addFieldInternal(real, null);
	}

	@Override
	protected void addFieldInternal(final Field real, Field below) {
		final FlowPanel wrapper = new FlowPanel();
		wrapper.addStyleName("ftr-flow-row");
		Label labelWidget = null;
		if (real.getLabel() != null) {
			labelWidget = new Label(real.getLabel());
			labelWidget.addStyleName("ftr-label");
			labelWidget.addStyleName("ftr-flow-label");
			wrapper.add(labelWidget);
		}

		setElements(real.getId(), labelWidget, real.getWidget());
		if (real.getWidget() != null) {
			wrapper.add(real.getWidget());
		}
		holderGrid.add(wrapper);
	}

	@Override
	public Field addTwoRows(final Field result) {

		if (result.getId() == null) {
			result.setId(UUID.uuid());
		}

		Label label = null;
		if (result.getLabel() != null) {
			label = new Label(result.getLabel());
			label.addStyleName("ftr-label");
			label.addStyleName("ftr-flow-label");
			holderGrid.add(label);
		}

		if (result.getManager() != null) {
			holderGrid.add(result.getManager());
			result.getManager().getElement().getStyle().setWidth(100, Unit.PCT);
		}
		setElements(result.getId(), label, result.getManager());

		return result;
	}

	@Override
	public void clear() {
		holderGrid.clear();
	}

	public void clearButtons() {
		holderGrid.clear();
	}

	@Override
	public String getLabel(final String name) {
		final Widget to = labels.get(name);
		return UiUtil.getTextOnWidget(to);
	}

	@Override
	public Iterator<Widget> iterator() {
		return holderGrid.iterator();
	}

	@Override
	public boolean remove(final Widget w) {
		return holderGrid.remove(w);
	}

	@Override
	public void setError(final String name, final boolean newState) {
		if (name != null) {
			super.setErrorForLabel(name, newState);
		}
	}

	@Override
	public void setLabel(final String name, final String newText) {
		final Widget to = labels.get(name);
		if (to instanceof HasText) {
			final HasText re = (HasText) to;
			re.setText(newText);
		}

	}

	@Override
	public void setRequired(final String name, final boolean newState) {
		if (name != null) {
			super.setRequiredForLabel(name, newState);
			if (newState) {
				showRequired(newState);
			}
			checkRequiredLegendas();
		}
	}

	public void setRequiredAndVisible(final String name, final boolean newState) {
		setRequired(name, newState);
		setVisible(name, newState);
	}

	@Override
	public void setVisible(final String name, final boolean newState) {
		if (name != null) {
			setVisibleElements(name, newState);
		}
	}

	@Override
	public void setWordWrapForLabel(final String labelName, final boolean b) {

	}

	@Override
	public void showFilled() {
		final Iterator<String> labelsName = labels.keySet().iterator();
		while (labelsName.hasNext()) {
			final String labelName = labelsName.next();
			final Widget item = edits.get(labelName);
			if (item instanceof HasText) {
				final HasText real = (HasText) item;
				if (real.getText() != null && real.getText().length() > 0) {
					setVisible(labelName, true);
				} else {
					setVisible(labelName, false);
				}
			}
		}
	}

	@Override
	public void showRequired(final boolean hasRequired) {
		requiredLabel.setVisible(hasRequired);
	}

}
