package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DialogBox;

public class CleanDialog extends DialogBox {

	public CleanDialog() {
		super();
		setStyleName("gwt-CleanDialogBox");
	}

	public CleanDialog(final boolean autoHide) {
		super(autoHide);
		setStyleName("gwt-CleanDialogBox");
	}

	public CleanDialog(final boolean autoHide, final boolean modal) {
		super(autoHide, modal);
		setStyleName("gwt-CleanDialogBox");
	}

	public CleanDialog(final boolean autoHide, final boolean modal, final Caption captionWidget) {
		super(autoHide, modal, captionWidget);
		setStyleName("gwt-CleanDialogBox");
	}

	public CleanDialog(final Caption captionWidget) {
		super(captionWidget);
		setStyleName("gwt-CleanDialogBox");
	}

	public void setPixelSizeOrToFullSize(final int maxWide, final int maxHeight) {
		int wide = Window.getClientWidth() - 20;
		if (wide > maxWide) {
			wide = maxWide;
		}
		int hi = Window.getClientHeight() - 10;
		if (hi > maxHeight) {
			hi = maxHeight;
		}
		getElement().getStyle().setWidth(wide, Unit.PX);
		getElement().getStyle().setHeight(hi, Unit.PX);

	}

}
