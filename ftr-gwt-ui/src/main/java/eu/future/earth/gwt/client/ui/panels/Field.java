package eu.future.earth.gwt.client.ui.panels;

import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.ui.general.HasWidget;

public class Field extends SimplePanel implements HasWidget {

    private Widget manager = null;

    private String label = null;

    private String id = null;

    private String help = null;

    private boolean shift = false;

    private boolean ident = true;

    private AbstractLabelFieldPanel labelFieldHolder = null;

    private Widget row = null;

    private boolean alignTop = false;

    private boolean required = false;

    @UiConstructor
    public Field() {
        super();
    }

    public static void setVisible(Field followDefaultPriceRef, boolean b) {
        if (followDefaultPriceRef != null) {
            followDefaultPriceRef.setVisible(b);
        }
    }

    public static void setErrorVisible(Field followDefaultPriceRef, boolean b) {
        if (followDefaultPriceRef != null) {
            followDefaultPriceRef.getLabelFieldHolder().setErrorForLabel(followDefaultPriceRef.getId(), b);
        }
    }

    public static void setLabel(Field followDefaultPriceRef, String b) {
        if (followDefaultPriceRef != null) {
            followDefaultPriceRef.setLabel(b);
        }
    }

    public static void setRequired(Field pricePerMonthRef, boolean b) {
        if (pricePerMonthRef != null) {
            pricePerMonthRef.changeRequired(b);
        }
    }

    public Widget getRow() {
        return row;
    }

    public void setRow(Widget row) {
        this.row = row;
    }

    @Override
    public void add(final Widget newWidget) {
        manager = newWidget;
    }

    public void changeRequired(final boolean newRequired) {
        required = newRequired;
        if (labelFieldHolder != null) {
            labelFieldHolder.setRequiredForLabel(this);
        }
    }

    @Override
    public Widget containingWidget() {
        return manager;
    }

    public String getHelp() {
        return help;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public AbstractLabelFieldPanel getLabelFieldHolder() {
        return labelFieldHolder;
    }

    public Widget getManager() {
        return manager;
    }

    @Override
    public Widget getWidget() {
        return manager;
    }

    public boolean hasHelp() {
        return help != null && !help.trim().isEmpty();
    }

    public boolean isAlignTop() {
        return alignTop;
    }

    public boolean isIdent() {
        return ident;
    }

    public boolean isRequired() {
        return required;
    }

    public boolean isShift() {
        return shift;
    }

    public void setAlignTop(final boolean alignTop) {
        this.alignTop = alignTop;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setIdent(final boolean ident) {
        this.ident = ident;
    }

    public void setLabel(final String label) {
        this.label = label;
        if (labelFieldHolder != null) {
            labelFieldHolder.setLabel(getId(), label);
        }
    }

    public void setLabelFieldHolder(final AbstractLabelFieldPanel labelFieldHolder) {
        this.labelFieldHolder = labelFieldHolder;
    }

    public void setManager(final Widget manager) {
        this.manager = manager;
    }

    public void setRequired(final boolean required) {
        this.required = required;
    }

    public void setFeedback(String newMessage) {
        getLabelFieldHolder().setFeedbackMessage(this, newMessage);
    }

    public void setShift(final boolean shift) {
        this.shift = shift;
    }

    @Override
    public void setVisible(final boolean newState) {
        if (labelFieldHolder != null) {
            labelFieldHolder.setVisibleForLabel(this, newState);
        }
    }

}
