package eu.future.earth.gwt.client.ui.panels;

public interface PlaceHolderWidget {

    void setPlaceholder(String label);

}
