package eu.future.earth.gwt.client.help;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;

public class HelpAnchor extends Composite implements ClickHandler {

	private String message = null;

	private final Anchor buttom;

	public HelpAnchor(String titleForAnchor, final String helpMessage) {
		super();
		buttom = new Anchor(titleForAnchor);
		initWidget(buttom);
		message = helpMessage;
		buttom.addClickHandler(this);
		buttom.addStyleName("help-marker");
	}

	@Override
	public void onClick(final ClickEvent event) {
		final HelpPopup info = new HelpPopup(message);
		info.showRelativeTo(buttom);
	}

}
