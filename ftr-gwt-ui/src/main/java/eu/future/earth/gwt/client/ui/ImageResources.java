package eu.future.earth.gwt.client.ui;

import com.google.gwt.core.client.GWT;

import eu.future.earth.gwt.client.ui.button.ButtonsTexts;

public class ImageResources {

	public static final String LABEL_FIELD_PANEL = "ftr-label-field-panel";

	public static final String LABEL = "ftr-label";

	public static final String LABEL_REQUIRED = "ftr-label-required";

	public static final String LABEL_ERROR = "ftr-label-error";

	public static final String FIELD = "ftr-field";

	public static final String FIELD_REQUIRED = "ftr-field-required";

	public static final String FIELD_ERROR = "ftr-field-error";

	public static final ButtonsTexts STATIC_TEXT = (ButtonsTexts) GWT.create(ButtonsTexts.class);

}
