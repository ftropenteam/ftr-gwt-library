package eu.future.earth.gwt.client.ui.panels;

import com.google.gwt.user.client.ui.InlineLabel;

public class FeedbackHolder extends InlineLabel {

	public FeedbackHolder() {
		super();
		super.setStyleName("material-label");
	}

	public void clearError() {
		super.removeStyleName("field-error-label");
		super.setText(null);

	}

	public void setError(String newFeedback) {
		super.addStyleName("field-error-label");
		super.setText(newFeedback);
	}

}
