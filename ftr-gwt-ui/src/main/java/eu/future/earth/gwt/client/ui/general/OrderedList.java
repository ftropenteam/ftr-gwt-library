package eu.future.earth.gwt.client.ui.general;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Widget;

public class OrderedList extends ComplexPanel {

	public OrderedList() {
		super();
		setElement(Document.get().createOLElement());
	}

	@Override
	public void add(final Widget w) {
		super.add(w, (Element) getElement());
	}

	public void insert(final Widget w, final int beforeIndex) {
		super.insert(w, (Element) getElement(), beforeIndex, true);
	}
}
