package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Image;

import eu.future.earth.gwt.client.ui.actionevent.ActionEvent;
import eu.future.earth.gwt.client.ui.actionevent.ActionHandler;
import eu.future.earth.gwt.client.ui.actionevent.HasActionHandlers;
import eu.future.earth.gwt.client.ui.popup.PopupRenderer;

public class TouchActionButton<D> extends TouchPopupButton<D> implements HasActionHandlers<D> {

	public TouchActionButton(final String newText, final Image newImage, final PopupRenderer<D> newRenderer) {
		super(newText, newImage, newRenderer);
		initWidget();
	}

	public TouchActionButton(final String newText, final PopupRenderer<D> newRenderer) {
		super(newText, newRenderer);
		initWidget();
	}

	private void initWidget() {
		super.addValueChangeHandler(event -> fireActionEvent(event.getValue()));
	}

	private void fireActionEvent(final D action) {
		ActionEvent.fire(this, action);
	}

	@Override
	public HandlerRegistration addActionHandler(final ActionHandler<D> handler) {
		return super.addHandler(handler, ActionEvent.getType());
	}

}
