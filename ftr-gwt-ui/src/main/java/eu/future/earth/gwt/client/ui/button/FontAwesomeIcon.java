package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * Anchor styled as a button
 */
public class FontAwesomeIcon extends Composite {

	protected SimplePanel main = new SimplePanel();

	public FontAwesomeIcon() {
		this(null);
	}

	public FontAwesomeIcon(final String newText) {
		super();
		initWidget(main);
		setSymbol(newText);
		super.setStyleName("fontawesome-icon");
	}

	public void setSymbol(final String newText) {
		if (newText != null) {
			final int i = Integer.parseInt(newText, 16);
			final String news = Character.toString((char) i);
			main.getElement().setAttribute("symbol", news);
		}
	}

}
