package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.user.client.ui.Image;

import eu.future.earth.gwt.client.ui.event.ClickTouchEvent;
import eu.future.earth.gwt.client.ui.event.ClickTouchHandler;

public abstract class BaseEditableTouchButton extends TouchButton {

	public BaseEditableTouchButton(final String newText) {
		this(newText, null);
	}

	public BaseEditableTouchButton(final String newText, final Image newImage) {
		super(newText, newImage);
		inittWidget();
	}

	private void inittWidget() {
		super.addStyleNameToMain("ftr-toggle");
		super.addStyleNameToMain("ftr-dropdown");
		super.addClickHandler(new ClickTouchHandler() {

			@Override
			public void onClick(final ClickTouchEvent event) {
				addStyleNameToMain("ftr-active");
				showPopup();
			}

		});

		main.addBlurHandler(new BlurHandler() {

			@Override
			public void onBlur(final BlurEvent event) {
				removeStyleNameToMain("ftr-active");
			}
		});
	}

	public abstract void showPopup();

}
