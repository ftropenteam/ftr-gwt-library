package eu.future.earth.gwt.client.ui.event;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;

public interface HasClickTouchHandlers extends HasHandlers {

	HandlerRegistration addClickHandler(ClickTouchHandler handler);

}
