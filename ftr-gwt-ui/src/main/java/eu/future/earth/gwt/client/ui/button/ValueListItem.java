package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.ui.general.ListItem;

public class ValueListItem<D> extends ListItem {

	private D value = null;

	public ValueListItem() {
		super();
	}

	public ValueListItem(Widget newItem) {
		super(newItem);
	}

	public ValueListItem(D newValue) {
		super();
		setValue(newValue);
	}

	public ValueListItem(Widget newItem, D newValue) {
		super(newItem);
		setValue(newValue);
	}

	public D getValue() {
		return value;
	}

	public void setValue(D value) {
		this.value = value;
	}


}
