/*
 * Copyright 2009 EasyEnterprise, all rights reserved.
 */
package eu.future.earth.gwt.client.ui.panels;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.dom.client.Style.Float;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

public class ButtonFlowPanelRight extends BaseButtonPanel implements HasWidgets {

	private final ButtonFlowPanel paen = new ButtonFlowPanel();
	private final FlowPanel left = new FlowPanel();

	@UiConstructor
	public ButtonFlowPanelRight() {
		super();
		final FlowPanel buttons = new FlowPanel();
		buttons.setStyleName("buttons-panel");
		initWidget(buttons);
		paen.getElement().getStyle().setFloat(Float.RIGHT);
		paen.getElement().getStyle().setPadding(2, Unit.PX);
		buttons.add(left);
		buttons.add(paen);
	}

	@Override
	public void add(final Widget button) {
		addButton(button);
	}

	@Override
	public void addButton(final Widget button) {
		paen.add(button);
	}

	public void addLeft(final Widget button) {
		left.add(button);
	}

	@Override
	public void clear() {
		clearButtons();
	}

	@Override
	public void clearButtons() {
		paen.clear();
	}

	public List<Widget> getButtons() {
		return paen.getButtons();
	}

	@Override
	public Iterator<Widget> iterator() {
		return paen.iterator();
	}

	@Override
	public int numberOfButtons() {
		return paen.numberOfButtons();
	}

	@Override
	public boolean remove(final Widget w) {
		return paen.remove(w);
	}

}
