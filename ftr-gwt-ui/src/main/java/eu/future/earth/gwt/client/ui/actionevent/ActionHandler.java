package eu.future.earth.gwt.client.ui.actionevent;

import com.google.gwt.event.shared.EventHandler;

public interface ActionHandler<T> extends EventHandler {

    void onKeyChange(ActionEvent<T> naviEvent);

}
 