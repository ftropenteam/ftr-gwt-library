package eu.future.earth.gwt.client.ui.general;

import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;

public class Paragraph extends ComplexPanel implements HasText, HasHTML {

	public Paragraph() {
		this(null);
	}

	public Paragraph(String newValue) {
		super();
		setElement(Document.get().createPElement());
		if (newValue != null) {
			setText(newValue);
		}
	}

	@Override
	public String getText() {
		return getElement().getInnerText();
	}

	@Override
	public void setText(String text) {
		getElement().setInnerText(text);
	}

	@Override
	public void setHTML(String text) {
		getElement().setInnerHTML(text);
	}

	@Override
	public String getHTML() {
		return getElement().getInnerHTML();
	}

}
