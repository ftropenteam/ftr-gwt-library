package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import eu.future.earth.gwt.client.ui.ImageResources;
import eu.future.earth.gwt.client.ui.general.UnOrderedList;
import eu.future.earth.gwt.client.ui.popup.PopupRenderer;
import eu.future.earth.gwt.client.ui.popup.StyleRenderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FontAwesomeTouchSelectButton<D> extends FontAwesomeTouchButton implements HasValueChangeHandlers<D>, ClickHandler, HasValue<D> {

    private static final String NONE = "NONE";

    private final PopupRenderer<D> renderer;

    private StyleRenderer<D> styleRenderer = null;

    private UnOrderedList ul = new UnOrderedList();

    private final String defaultText;

    private boolean allowDeselect = false;

    private CleanPopupPanel popup = new CleanPopupPanel(true);

    private boolean normal = true;

    private List<D> choices = null;

    private Map<D, ValueListItem<D>> ref = new HashMap<>();

    public D selected = null;

    private String selectedKey = NONE;

    private String oldStyle = null;

    private ValueListItem<D> noneSelectedRow = null;

    public FontAwesomeTouchSelectButton(final String newText, final PopupRenderer<D> newRenderer) {
        super(newText);
        defaultText = newText;
        renderer = newRenderer;
        if (renderer instanceof StyleRenderer<?>) {
            styleRenderer = (StyleRenderer<D>) renderer;
        }
        initializeWidget();
    }

    public void addAllowDeselect() {
        this.allowDeselect = true;
        if (noneSelectedRow == null) {
            noneSelectedRow = new ValueListItem<>();
            final InlineLabel noneSelected = new InlineLabel(ImageResources.STATIC_TEXT.nothingSelected());
            noneSelected.setStyleName("option");
            noneSelectedRow.add(noneSelected);
            noneSelectedRow.addClickHandler(this);
        }
        ul.add(noneSelectedRow);
    }

    public void addValue(final D d) {
        if (choices == null) {
            choices = new ArrayList<>();
        }
        choices.add(d);
        addValueInternal(d);
    }

    @Override
    public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<D> handler) {
        return super.addHandler(handler, ValueChangeEvent.getType());
    }

    private void addValueInternal(final D d) {
        final ValueListItem<D> newRow = new ValueListItem<>(d);
        ref.put(d, newRow);
        final InlineLabel lbl = new InlineLabel(renderer.getLabel(d));
        lbl.setStyleName("option");
        newRow.add(lbl);
        if (styleRenderer != null) {
            newRow.addStyleName(styleRenderer.getStyle(d));
        }
        newRow.addClickHandler(this);
        ul.add(newRow);
    }

    public void clearItems() {
        setChoices(null);
    }

    public void containInWindow() {
        ul.addStyleName("ul-max");
        ul.getElement().getStyle().setProperty("maxHeight", Window.getClientHeight() - 10, Unit.PX);
        normal = false;
    }

    public void deleteValue(final D d) {
        if (d == null) {
            return;
        }
        if (choices == null) {
            return;
        }
        final List<D> newChoices = new ArrayList<>();
        for (final D da : choices) {
            if (!renderer.getId(da).equals(renderer.getId(d))) {
                newChoices.add(da);
            }
        }
        setChoices(newChoices);
    }

    public String getSelectedKey() {
        if (selected != null) {
            return renderer.getId(selected);
        } else {
            return NONE;
        }
    }

    @Override
    public D getValue() {
        return selected;
    }

    public boolean hasChoices() {
        return numberChoices() > 0;
    }

    public void hidePopup() {
        if (popup != null) {
            popup.hide();
        }
    }

    private void initializeWidget() {
        super.addStyleNameToMain("ftr-dropdown");
        popup.add(ul);

        popup.setStyleName("ftr-dropdown");
        super.addClickHandler(event -> {
            if (normal) {
                popup.showRelativeTo(main);
            } else {

                popup.center();
            }
            addStyleNameToMain("ftr-active");
            popup.addStyleName("ftr-active");
        });

        main.addBlurHandler(event -> removeStyleNameToMain("ftr-active"));
    }

    public boolean isAllowDeselect() {
        return allowDeselect;
    }

    public int numberChoices() {
        if (choices == null) {
            return 0;
        }
        return choices.size();
    }

    @Override
    public void onClick(final ClickEvent event) {
        if (event.getSource() instanceof ValueListItem) {
            final ValueListItem<D> real = (ValueListItem<D>) event.getSource();
            selected = real.getValue();
            setKeyValue();
            popup.hide();
            setSelectedToButton();
			ValueChangeEvent.fire(this, selected);
        }
    }

    public void setChoices(final List<D> newData) {
        choices = newData;
        ul.clear();
        ref.clear();
        if (isAllowDeselect()) {
            addAllowDeselect();
        }
        if (choices != null) {
            for (final D d : newData) {
                addValueInternal(d);
            }
        }
        setSelectedInternal();
    }

    private void setKeyValue() {
        if (selected != null) {
            selectedKey = renderer.getId(selected);
        } else {
            selectedKey = NONE;
        }
    }

    private void setSelectedInternal() {
        if (selectedKey == null || selectedKey.equals(NONE)) {
            setValue(null);
        } else {
            if (choices != null) {
                for (final D sel : choices) {
                    final String found = renderer.getId(sel);
                    if (selectedKey.equals(found)) {
                        setValue(sel);
                    }
                }
            }
        }
    }

    private void setSelectedToButton() {
        for (final ValueListItem<D> remSelected : ref.values()) {
            remSelected.removeStyleName("selected");
        }
        if (noneSelectedRow != null) {
            noneSelectedRow.removeStyleName("selected");
        }
        if (oldStyle != null) {
            super.removeStyleName(oldStyle);
        }
        if (selected == null) {
            super.setTitle(defaultText);
            if (noneSelectedRow != null) {
                noneSelectedRow.addStyleName("selected");
            }
        } else {
            if (styleRenderer != null) {
                oldStyle = styleRenderer.getStyle(selected);
                if (oldStyle != null) {
                    super.addStyleName(oldStyle);
                }
            }
            final ValueListItem<D> fo = ref.get(selected);
            if (fo != null) {
                fo.addStyleName("selected");
            }
            super.setTitle(renderer.getLabel(selected));
        }
    }

    public void setSelectionByKey(final String data) {
        selectedKey = data;
        if (choices != null && !choices.isEmpty()) {
            setSelectedInternal();
        }
    }

    @Override
    public void setValue(final D selected) {
        this.selected = selected;
        setKeyValue();
        setSelectedToButton();
    }

    @Override
    public void setValue(final D value, final boolean fireEvents) {
        setValue(value);
        if (fireEvents) {
            ValueChangeEvent.fire(this, value);
        }
    }

    public void tellParentChanged() {
        ValueChangeEvent.fire(this, selected);
    }

}
