package eu.future.earth.gwt.client.ui.general;

import com.google.gwt.user.client.ui.Widget;

public interface HasWidget {

	Widget containingWidget();
	
}
