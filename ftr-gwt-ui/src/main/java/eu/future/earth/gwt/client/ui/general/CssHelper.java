package eu.future.earth.gwt.client.ui.general;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.user.client.ui.Widget;

public class CssHelper {

	/**
	 * BACKGROUND_IMAGE
	 * <p>
	 * CSS: 'background-image', javaScript: 'backgroundImage' <br>
	 * Values: &lt;uri&gt; | none | inherit <br>
	 * Initial value: none <br>
	 * Applies to: all <br>
	 * Inherited?: no <br>
	 * Media groups: visual
	 */
	public static final String MAX_WIDTH = "maxWidth";

	public static final String MIN_WIDTH = "minWidth";

	/**
	 * This class contains all CSS Attributes.
	 */
	public static final class A {

		/**
		 * FONT-FAMILY
		 * <p>
		 * CSS: 'background-attachment', javaScript: 'backgroundAttachment' <br>
		 * Values: Verdana | Times New Roman | Courier New <br>
		 * Initial value: scroll <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String FONT_FAMILY = "font-family";

		/**
		 * BACKGROUND_ATTACHMENT
		 * <p>
		 * CSS: 'background-attachment', javaScript: 'backgroundAttachment' <br>
		 * Values: scroll | fixed | inherit <br>
		 * Initial value: scroll <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BACKGROUND_ATTACHMENT = "backgroundAttachment";

		/**
		 * BACKGROUND_COLOR
		 * <p>
		 * CSS: 'background-color', javaScript: 'backgroundColor' <br>
		 * Values: &lt;color&gt; | transparent | inherit <br>
		 * Initial value: transparent <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BACKGROUND_COLOR = "backgroundColor";

		/**
		 * BACKGROUND_IMAGE
		 * <p>
		 * CSS: 'background-image', javaScript: 'backgroundImage' <br>
		 * Values: &lt;uri&gt; | none | inherit <br>
		 * Initial value: none <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BACKGROUND_IMAGE = "backgroundImage";

		/**
		 * BACKGROUND_POSITION
		 * <p>
		 * CSS: 'background-position', javaScript: 'backgroundPosition' <br>
		 * Values: [ [ &lt;percentage&gt; | &lt;length&gt; | left | center | right ] [ &lt;percentage&gt; |
		 * &lt;length&gt; | top | center | bottom ]? ] | [ [ left | center | right ] || [ top | center | bottom ] ] |
		 * inherit <br>
		 * Initial value: 0% 0% <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Percentages: refer to the size of the box itself <br>
		 * Media groups: visual
		 */
		public static final String BACKGROUND_POSITION = "backgroundPosition";

		/**
		 * BACKGROUND_REPEAT
		 * <p>
		 * CSS: 'background-repeat', javaScript: 'backgroundRepeat' <br>
		 * Values: repeat | repeat-x | repeat-y | no-repeat | inherit <br>
		 * Initial value: repeat <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BACKGROUND_REPEAT = "backgroundRepeat";

		/**
		 * BORDER_TOP
		 * <p>
		 * CSS: 'border-top', javaScript: 'borderTop' <br>
		 * Values: [ &lt;border-width&gt; || &lt;border-style&gt; || 'border-top-color' ] | inherit <br>
		 * Initial value: see individual properties <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_TOP = "borderTop";

		/**
		 * BORDER_RIGHT
		 * <p>
		 * CSS: 'border-right', javaScript: 'borderRight' <br>
		 * Values: [ &lt;border-width&gt; || &lt;border-style&gt; || 'border-top-color' ] | inherit <br>
		 * Initial value: see individual properties <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_RIGHT = "borderRight";

		/**
		 * BORDER_BOTTOM
		 * <p>
		 * CSS: 'border-bottom', javaScript: 'borderBottom' <br>
		 * Values: [ &lt;border-width&gt; || &lt;border-style&gt; || 'border-top-color' ] | inherit <br>
		 * Initial value: see individual properties <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_BOTTOM = "borderBottom";

		/**
		 * BORDER_LEFT
		 * <p>
		 * CSS: 'border-left', javaScript: 'borderLeft' <br>
		 * Values: [ &lt;border-width&gt; || &lt;border-style&gt; || 'border-top-color' ] | inherit <br>
		 * Initial value: see individual properties <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_LEFT = "borderLeft";

		/**
		 * BORDER_TOP_COLOR
		 * <p>
		 * CSS: 'border-top-color', javaScript: 'borderTopColor' <br>
		 * Values: &lt;color&gt; | transparent | inherit <br>
		 * Initial value: the value of the 'color' property <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_TOP_COLOR = "borderTopColor";

		/**
		 * BORDER_RIGHT_COLOR
		 * <p>
		 * CSS: 'border-right-color', javaScript: 'borderRightColor' <br>
		 * Values: &lt;color&gt; | transparent | inherit <br>
		 * Initial value: the value of the 'color' property <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_RIGHT_COLOR = "borderRightColor";

		/**
		 * BORDER_BOTTOM_COLOR
		 * <p>
		 * CSS: 'border-bottom-color', javaScript: 'borderBottomColor' <br>
		 * Values: &lt;color&gt; | transparent | inherit <br>
		 * Initial value: the value of the 'color' property <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_BOTTOM_COLOR = "borderBottomColor";

		/**
		 * BORDER_LEFT_COLOR
		 * <p>
		 * CSS: 'border-left-color', javaScript: 'borderLeftColor' <br>
		 * Values: &lt;color&gt; | transparent | inherit <br>
		 * Initial value: the value of the 'color' property <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_LEFT_COLOR = "borderLeftColor";

		/**
		 * BORDER_TOP_STYLE
		 * <p>
		 * CSS: 'border-top-style', javaScript: 'borderTopStyle' <br>
		 * Values: &lt;border-style&gt; | inherit <br>
		 * Initial value: none <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_TOP_STYLE = "borderTopStyle";

		/**
		 * BORDER_RIGHT_STYLE
		 * <p>
		 * CSS: 'border-right-style', javaScript: 'borderRightStyle' <br>
		 * Values: &lt;border-style&gt; | inherit <br>
		 * Initial value: none <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_RIGHT_STYLE = "borderRightStyle";

		/**
		 * BORDER_BOTTOM_STYLE
		 * <p>
		 * CSS: 'border-bottom-style', javaScript: 'borderBottomStyle' <br>
		 * Values: &lt;border-style&gt; | inherit <br>
		 * Initial value: none <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_BOTTOM_STYLE = "borderBottomStyle";

		/**
		 * BORDER_LEFT_STYLE
		 * <p>
		 * CSS: 'border-left-style'', javaScript: 'borderLeftStyle' <br>
		 * Values: &lt;border-style&gt; | inherit <br>
		 * Initial value: none <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_LEFT_STYLE = "borderLeftStyle";

		/**
		 * BORDER_TOP_WIDTH
		 * <p>
		 * CSS: 'border-top-width', javaScript: 'borderTopWidth' <br>
		 * Values: &lt;border-width&gt; | inherit <br>
		 * Initial value: medium <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_TOP_WIDTH = "borderTopWidth";

		/**
		 * BORDER_RIGHT_WIDTH
		 * <p>
		 * CSS: 'border-right-width', javaScript: 'borderRightWidth' <br>
		 * Values: &lt;border-width&gt; | inherit <br>
		 * Initial value: medium <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_RIGHT_WIDTH = "borderRightWidth";

		/**
		 * BORDER_BOTTOM_WIDTH
		 * <p>
		 * CSS: 'border-bottom-width, javaScript: 'borderBottomWidth' <br>
		 * Values: &lt;border-width&gt; | inherit <br>
		 * Initial value: medium <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_BOTTOM_WIDTH = "borderBottomWidth";

		/**
		 * BORDER_LEFT_WIDTH
		 * <p>
		 * CSS: 'border-left-width', javaScript: 'borderLeftWidth' <br>
		 * Values: &lt;border-width&gt; | inherit <br>
		 * Initial value: medium <br>
		 * Applies to: all <br>
		 * Inherited?: no <br>
		 * Media groups: visual
		 */
		public static final String BORDER_LEFT_WIDTH = "borderLeftWidth";
	}

	public static final class V {

		public static final class BORDER_WIDTH {

			public static final String THIN = "1px";

		}

		public static final class FONT_SIZE {

			public static final double X_LARGE = 22;

		}

		/**
		 * CSS property {@link A#BACKGROUND_POSITION} values.
		 */
		public static final class BACKGROUND_POSITION {
			public static final String LEFT = "left";

			public static final String CENTER = "center";

			public static final String RIGHT = "right";

			public static final String TOP = "top";
		}

		/**
		 * CSS property {@link A#BACKGROUND_REPEAT} values.
		 */
		public static final class BACKGROUND_REPEAT {

			/**
			 * Default value for {@link A#BACKGROUND_REPEAT }.
			 */
			public static final String REPEAT = "repeat";

			public static final String REPEAT_X = "repeat-x";

			public static final String REPEAT_Y = "repeat-y";

			public static final String NO_REPEAT = "no-repeat";
		}

	}

	public static String getFontFamily(final Element el) {
		if (el != null && el.hasAttribute("style")) {
			final String style = el.getAttribute("style");
			if (style != null) {
				return extractProperty(CssHelper.A.FONT_FAMILY, style);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static String getFontFamilyComputed(final Element el) {
		if (el != null) {
			final String directFont = getFontFamily(el);
			if (directFont != null && !directFont.isEmpty()) {
				return directFont;
			} else {
				return ComputedStyle.getStyleProperty(el, "fontFamily");
			}
		} else {
			return null;
		}
	}

	public static String getFontSizeComputed(final Element el) {
		if (el != null) {
			final String directValue = el.getStyle().getFontSize();
			if (directValue != null && !directValue.isEmpty()) {
				return getSize(directValue);
			} else {
				return getSize(ComputedStyle.getStyleProperty(el, "fontSize"));
			}
		} else {
			return null;
		}
	}

	public static String getColorComputed(final Element el) {
		if (el != null) {
			final String directValue = el.getStyle().getFontSize();
			if (directValue != null && !directValue.isEmpty()) {
				return getSize(directValue);
			} else {
				return getSize(ComputedStyle.getStyleProperty(el, "color"));
			}
		} else {
			return null;
		}
	}

	public static String getSize(final String value) {
		if (value != null && value.indexOf("px") > -1) {
			return value.substring(0, value.indexOf("px"));
		}
		return value;
	}

	public static void setFontFamily(final Element el, final String newValue) {
		setStyleProperty(el, CssHelper.A.FONT_FAMILY, newValue);
	}

	public static void setStyleProperty(final Element el, final String propName, final String newValue) {
		if (el != null) {
			final String style = el.getAttribute("style");
			final String newStyle = replaceProperty(propName, style, newValue);
			if (newStyle != null && !newStyle.isEmpty()) {
				el.setAttribute("style", newStyle);
			} else {
				if (style != null) {
					el.removeAttribute("style");
				}
			}
		}
	}

	public static String extractProperty(final String string, final String style) {
		final int posStartStyle = style.indexOf(string);
		if (posStartStyle == -1) {
			return null;
		} else {
			final int posEndStyle = style.indexOf(";", posStartStyle);
			final int pos = style.indexOf(":", posStartStyle);
			if (pos > posStartStyle && pos < posEndStyle) {
				return style.substring(pos + 1, posEndStyle).trim();
			} else {
				return null;
			}
		}
	}

	public static String replaceProperty(final String string, final String style, final String newValue) {
		if (style == null && newValue == null) {
			return null;
		}
		if (style == null) {
			return string + ": " + newValue + ";";
		}
		final int posStartStyle = style.indexOf(string);
		if (posStartStyle == -1) {
			if (newValue == null) {
				return style;
			} else {
				return style + " " + string + ": " + newValue + ";";
			}
		} else {
			final int posEndStyle = style.indexOf(";", posStartStyle);
			if (newValue == null) {
				return style.substring(0, posStartStyle) + style.substring(posEndStyle + 1, style.length());
			} else {
				return style.substring(0, posStartStyle) + string + ": " + newValue + ";" + style.substring(posEndStyle + 1, style.length());
			}

		}
	}

	/**
	 * Convenience method to set a style property on a widget.
	 * 
	 * <p>
	 * The GWT compiler will optimize this method away, meaning there are no additional costs of an extra method call
	 * when using method.
	 * 
	 * @param widget
	 *            Widget to set the property on
	 * @param name
	 *            Name of the property
	 * @param value
	 *            Value of the property
	 */
	public static void setProperty(final Widget widget, final String name, final String value) {
		widget.getElement().getStyle().setProperty(name, value);
	}

	public static void setProperty(final Element widget, final String name, final String value) {
		widget.getStyle().setProperty(name, value);
	}

	private static void remove(final Element widget, final String name) {
		widget.getStyle().clearProperty(name);

	}

	public static void setBorderBottumColor(final Widget fits, final String color) {
		setBorderBottumColor(fits, 3, color);
	}

	public static void setBorderBottumColor(final Widget fits, final int wide, final String color) {
		if (color != null && wide > 0) {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_BOTTOM_COLOR, color);
			CssHelper.setProperty(fits, CssHelper.A.BORDER_BOTTOM_STYLE, BorderStyle.SOLID.getCssName());
			CssHelper.setProperty(fits, CssHelper.A.BORDER_BOTTOM_WIDTH, wide + "px");
		} else {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_BOTTOM_WIDTH, "0px");
		}
	}

	public static void setBorderBottumColor(final Element fits, final int wide, final String color) {
		if (color != null && wide > 0) {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_BOTTOM_COLOR, color);
			CssHelper.setProperty(fits, CssHelper.A.BORDER_BOTTOM_STYLE, BorderStyle.SOLID.getCssName());
			CssHelper.setProperty(fits, CssHelper.A.BORDER_BOTTOM_WIDTH, wide + "px");
		} else {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_BOTTOM_WIDTH, "0px");
		}
	}

	public static void setBorderRightColor(final Element fits, final int wide, final String color) {
		if (color != null && wide > 0) {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_RIGHT_COLOR, color);
			CssHelper.setProperty(fits, CssHelper.A.BORDER_RIGHT_STYLE, BorderStyle.SOLID.getCssName());
			CssHelper.setProperty(fits, CssHelper.A.BORDER_RIGHT_WIDTH, wide + "px");
		} else {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_RIGHT_WIDTH, "0px");
		}
	}

	public static void setBorderLeftColor(final Element fits, final int wide, final String color) {
		if (color != null && wide > 0) {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_LEFT_COLOR, color);
			CssHelper.setProperty(fits, CssHelper.A.BORDER_LEFT_STYLE, BorderStyle.SOLID.getCssName());
			CssHelper.setProperty(fits, CssHelper.A.BORDER_LEFT_WIDTH, wide + "px");
		} else {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_LEFT_WIDTH, "0px");
		}
	}

	public static void setBorderTopColor(final Widget fits, final String color) {
		setBorderTopColor(fits, 3, color);
	}

	public static void setBorderTopColor(final Widget fits, final int wide, final String color) {
		if (color != null && wide > 0) {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_TOP_COLOR, color);
			CssHelper.setProperty(fits, CssHelper.A.BORDER_TOP_STYLE, BorderStyle.SOLID.getCssName());
			CssHelper.setProperty(fits, CssHelper.A.BORDER_TOP_WIDTH, wide + "px");
		} else {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_TOP_WIDTH, "0px");
		}
	}

	public static void setBorderTopColor(final Element fits, final int wide, final String color) {
		if (color != null && wide > 0) {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_TOP_COLOR, color);
			CssHelper.setProperty(fits, CssHelper.A.BORDER_TOP_STYLE, BorderStyle.SOLID.getCssName());
			CssHelper.setProperty(fits, CssHelper.A.BORDER_TOP_WIDTH, wide + "px");
		} else {
			CssHelper.setProperty(fits, CssHelper.A.BORDER_TOP_WIDTH, "0px");
		}
	}

	public static void setMaxWidth(final Widget fits, final int wide) {
		setMaxWidth(fits.getElement(), wide);
	}

	public static void setMaxWidth(final Element fits, final int wide) {
		if (wide > 0) {
			CssHelper.setProperty(fits, CssHelper.MAX_WIDTH, wide + "px");
		} else {
			CssHelper.remove(fits, CssHelper.MAX_WIDTH);
		}
	}

	public static void setMinWidth(final Widget fits, final int wide) {
		setMinWidth(fits.getElement(), wide);
	}

	public static void setMinWidth(final Element fits, final int wide) {
		if (wide > 0) {
			CssHelper.setProperty(fits, CssHelper.MIN_WIDTH, wide + "px");
		} else {
			CssHelper.remove(fits, CssHelper.MIN_WIDTH);
		}
	}



}
