package eu.future.earth.gwt.client.ui.edit;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.TextBox;

public abstract class FtrBox extends TextBox {

	public FtrBox() {
		super();
		super.sinkEvents(Event.ONKEYDOWN);
	}

	public FtrBox(final Element element) {
		super(element);
		super.sinkEvents(Event.ONKEYDOWN);
	}

	public void setPlaceHolder(String newPlaceHolder){
		getElement().setPropertyString("placeholder", newPlaceHolder);
	}


	public boolean notEmpty() {
		return super.getText() != null && super.getText().length() > 0;
	}

	public static boolean isPast(final Event event) {
		final int keyCode = event.getKeyCode();
		final char ch = ((char) keyCode);
		return event.getCtrlKey() && (ch == 'v' || ch == 'V');
	}

	@Override
	public void onBrowserEvent(final Event event) {
		// int eventType = DOM.eventGetType(event);
		final int keyCode = event.getKeyCode();
		if (isValidNavigationKeyCode(keyCode, event)) {
			if (isPast(event)) {
				beforePaste(event);
			}
			super.onBrowserEvent(event);
			if (isPast(event)) {
				Scheduler.get().scheduleDeferred(new ScheduledCommand() {
					@Override
					public void execute() {
						afterPaste(event);
					}

				});
			}
		} else {
			if (handleKeyDown(event)) {
				super.onBrowserEvent(event);
				handleAfterKeyDown(event);
			} else {
				event.preventDefault();
			}
		}
	}

	public void beforePaste(final Event event) {

	}

	public void handleAfterKeyDown(final Event event) {

	}

	public abstract void afterPaste(Event event);

	public static boolean isValidNavigationKeyCode(final int keyCode, final Event event) {
		switch (keyCode) {
		case KeyCodes.KEY_BACKSPACE:
			return true;
		case KeyCodes.KEY_DELETE:
			return true;
		case KeyCodes.KEY_RIGHT:
			return true;
		case KeyCodes.KEY_LEFT:
			return true;
		case KeyCodes.KEY_TAB:
			return true;
		case KeyCodes.KEY_END:
			return true;
		case KeyCodes.KEY_ENTER:
			return true;
		case KeyCodes.KEY_HOME:
			return true;
		default:
			break;
		}
		final char ch = ((char) keyCode);
		if (event.getCtrlKey() && (ch == 'C' || ch == 'c')) {
			return true;
		} else {
			if (event.getCtrlKey() && (ch == 'v' || ch == 'V')) {
				return true;
			} else {
				if (event.getCtrlKey() && (ch == 'x' || ch == 'X')) {
					return true;
				} else {
					if (event.getCtrlKey() && (ch == 'a' || ch == 'A')) {
						return true;
					} else {

					}
				}

			}
		}
		return false;
	}

	public static boolean isValidNavigation(final int keyCode, final Event event) {
		switch (keyCode) {
		case KeyCodes.KEY_BACKSPACE:
			return true;
		case KeyCodes.KEY_DELETE:
			return true;
		case KeyCodes.KEY_RIGHT:
			return true;
		case KeyCodes.KEY_LEFT:
			return true;
		case KeyCodes.KEY_TAB:
			return true;
		case KeyCodes.KEY_END:
			return true;
		case KeyCodes.KEY_ENTER:
			return true;
		case KeyCodes.KEY_HOME:
			return true;
		default:
			break;
		}

		return false;
	}

	public abstract boolean handleKeyDown(Event event);

}
