package eu.future.earth.gwt.client.ui.panels;

public enum ButtonLayoutType {

	Compact, Fixed, Spaced
	
}
