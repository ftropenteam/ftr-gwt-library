package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.*;
import eu.future.earth.gwt.client.ui.general.UnOrderedList;
import eu.future.earth.gwt.client.ui.popup.IconRenderer;
import eu.future.earth.gwt.client.ui.popup.ObjectRenderer;
import eu.future.earth.gwt.client.ui.popup.PopupRenderer;
import eu.future.earth.gwt.client.ui.popup.TitleRenderer;

import java.util.ArrayList;
import java.util.List;

public class FontAwesomeTouchPopupButton<D> extends FontAwesomeTouchButton implements HasValueChangeHandlers<D>, ClickHandler {

    private final PopupRenderer<D> renderer;

    private IconRenderer<D> imageRenderer = null;

    private ObjectRenderer<D> objectRenderer = null;

    private TitleRenderer<D> titleRenderer = null;

    private UnOrderedList ul = new UnOrderedList();

    private FlowPanel top = new FlowPanel();


    public FontAwesomeTouchPopupButton(final String newText, final PopupRenderer<D> newRenderer) {
        super(newText);
        renderer = newRenderer;
        if (renderer instanceof IconRenderer<?>) {
            imageRenderer = (IconRenderer<D>) renderer;
        }
        if (renderer instanceof ObjectRenderer<?>) {
            objectRenderer = (ObjectRenderer<D>) renderer;
        }
        if (renderer instanceof TitleRenderer<?>) {
            titleRenderer = (TitleRenderer<D>) renderer;
        }
        initializeWidget();
    }


    public void clearItems() {
        setChoices(null);
    }

    private CleanPopupPanel popup = new CleanPopupPanel(true);

    public void addWidgetToPopup(final Widget original) {
        top.add(original);
    }

    private void initializeWidget() {
        super.addStyleNameToMain("ftr-dropdown");

        final ScrollPanel scroller = new ScrollPanel(ul);
        scroller.addStyleName("ul-selectable");
        final FlowPanel ma = new FlowPanel();
        ma.add(top);
        ma.add(scroller);
        ma.addStyleName("ftr-dropdown-border");
        // popup.add(top);
        popup.add(ma);
        popup.setStyleName("ftr-dropdown");
        super.addClickHandler(event -> showPopup());
        main.addBlurHandler(event -> removeStyleNameToMain("ftr-active"));
    }

    private String htmlForNoChoicesAvailable = null;

    public void showPopup() {
        popup.showRelativeTo(main);
        addStyleNameToMain("ftr-active");
        popup.addStyleName("ftr-active");
        if (htmlForNoChoicesAvailable != null && (choices == null || choices.isEmpty())) {
            ul.clear();
            final HTML help = new HTML(htmlForNoChoicesAvailable);
            help.getElement().getStyle().setPadding(10, Unit.PX);
            ul.add(help);
        }
    }

    public void hidePopup() {
        if (popup != null) {
            popup.removeStyleName("ftr-active");
            popup.hide();
        }
    }

    private List<D> choices = null;


    public void setChoices(final List<D> newData) {
        choices = newData;
        ul.clear();
        if (choices != null) {
            for (final D d : newData) {
                addValueInternal(d);
            }
        }
    }

    public void setEnabled(final D value, final boolean newState) {
        if (newState) {
            addValue(value);
        } else {
            deleteValue(value);
        }
    }

    public boolean hasChoices() {
        return numberChoices() > 0;
    }

    public int numberChoices() {
        if (choices == null) {
            return 0;
        }
        return choices.size();
    }

    public void addValue(final D d) {
        if (choices == null) {
            choices = new ArrayList<>();
        }
        if (choices.contains(d)) {
            return;
        }
        choices.add(d);
        addValueInternal(d);
    }

    public void deleteValue(final D d) {
        if (d == null) {
            return;
        }
        if (choices == null) {
            return;
        }
        final List<D> newChoices = new ArrayList<>();
        for (final D da : choices) {
            if (!renderer.getId(da).equals(renderer.getId(d))) {
                newChoices.add(da);
            }
        }
        setChoices(newChoices);
    }

    private void addValueInternal(final D d) {
        final ValueListItem<D> newRow = new ValueListItem<>(d);
        if (imageRenderer != null) {
            newRow.add(imageRenderer.getImage(d));
        }

        if (objectRenderer != null) {
            final Widget label = objectRenderer.getWidget(d);
            if (titleRenderer != null) {
                final String ti = titleRenderer.getTitle(d);
                if (ti != null) {
                    label.setTitle(ti);
                }
            }
            newRow.add(label);
        } else {

            final InlineLabel label = new InlineLabel(renderer.getLabel(d));
            if (titleRenderer != null) {
                final String ti = titleRenderer.getTitle(d);
                if (ti != null) {
                    label.setTitle(ti);
                }
            }
            newRow.add(label);
        }
        newRow.addClickHandler(this);
        // newRow.addDomHandler(this, ClickEvent.getType());
        ul.add(newRow);
    }


    @Override
    public void onClick(final ClickEvent event) {
        if (event.getSource() instanceof ValueListItem) {
            final ValueListItem<D> real = (ValueListItem<D>) event.getSource();
            final D selected = real.getValue();
            popup.hide();
            ValueChangeEvent.fire(this, selected);
        }
    }

    @Override
    public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<D> handler) {
        return super.addHandler(handler, ValueChangeEvent.getType());
    }

    public String getHtmlForNoChoicesAvailable() {
        return htmlForNoChoicesAvailable;
    }

    public void setHtmlForNoChoicesAvailable(final String htmlForNoChoicesAvailable) {
        this.htmlForNoChoicesAvailable = htmlForNoChoicesAvailable;
    }

}
