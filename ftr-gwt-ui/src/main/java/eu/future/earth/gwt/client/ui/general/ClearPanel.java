package eu.future.earth.gwt.client.ui.general;

import com.google.gwt.user.client.ui.FlowPanel;

public class ClearPanel extends FlowPanel {

	public ClearPanel() {
		super();
		super.addStyleName("ftr-clear-panel");
	}

}
