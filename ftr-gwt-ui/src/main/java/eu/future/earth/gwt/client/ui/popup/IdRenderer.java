package eu.future.earth.gwt.client.ui.popup;

public interface IdRenderer<D> {

	String getId(D newRow);
	
}
