/*
 * Copyright 2009 EasyEnterprise, all rights reserved.
 */
package eu.future.earth.gwt.client.ui.panels;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

public class ButtonFlowPanel extends BaseButtonPanel implements HasWidgets {

	private final FlowPanel buttonHolder = new FlowPanel();

	private final List<Widget> buttons = new ArrayList<Widget>();

	@UiConstructor
	public ButtonFlowPanel() {
		super();
		initWidget(buttonHolder);
		buttonHolder.setStyleName("ftr-buttons-flowpanel");
	}

	public ButtonFlowPanel(final ButtonLayoutType compactCss) {
		super();
		initWidget(buttonHolder);
		switch (compactCss) {
		case Compact: {
			buttonHolder.setStyleName("ftr-buttons-flowpanel-compact");
			break;
		}
		case Fixed: {
			buttonHolder.setStyleName("ftr-buttons-flowpanel");
			break;
		}
		case Spaced: {
			buttonHolder.setStyleName("ftr-buttons-flowpanel-spaced");
			break;
		}
		default:
			break;
		}

	}

	@Override
	public void add(final Widget w) {
		addButton(w);
	}

	@Override
	public void addButton(final Widget button) {
		if (button != null) {
			buttonHolder.add(button);
			buttons.add(button);
		}
	}

	@Override
	public void clear() {
		clearButtons();
	}

	@Override
	public void clearButtons() {
		buttonHolder.clear();
		buttons.clear();
	}

	public List<Widget> getButtons() {
		return buttons;
	}

	@Override
	public Iterator<Widget> iterator() {
		return buttons.iterator();
	}

	@Override
	public int numberOfButtons() {
		return buttons.size();
	}

	@Override
	public boolean remove(final Widget w) {
		return buttons.remove(w);
	}

}
