package eu.future.earth.gwt.client.ui.panels;

import com.google.gwt.user.client.ui.InlineLabel;

public class HelpHolder extends InlineLabel {

	public HelpHolder() {
		super();
		super.setStyleName("field-helper-label");
	}

	@Override
	public void setText(String text) {
		super.getElement().setInnerHTML(text);
	}
}
