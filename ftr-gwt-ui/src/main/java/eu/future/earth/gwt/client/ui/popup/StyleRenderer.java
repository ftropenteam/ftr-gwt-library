package eu.future.earth.gwt.client.ui.popup;

public interface StyleRenderer<D> extends IdRenderer<D> {

	String getStyle(D item);

}
