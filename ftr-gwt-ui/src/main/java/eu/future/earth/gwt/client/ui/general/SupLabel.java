package eu.future.earth.gwt.client.ui.general;

import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HasText;

public class SupLabel extends ComplexPanel implements HasText {

	public SupLabel() {
		this(null);
	}

	public SupLabel(String newValue) {
		super();
		setElement(Document.get().createElement("sup"));
		if (newValue != null) {
			setText(newValue);
		}
	}

	@Override
	public String getText() {
		return getElement().getInnerText();
	}

	@Override
	public void setText(String text) {
		getElement().setInnerText(text);
	}

}
