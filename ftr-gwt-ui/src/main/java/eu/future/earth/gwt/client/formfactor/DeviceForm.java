package eu.future.earth.gwt.client.formfactor;

public enum DeviceForm {

	Phone, Tablet, Desktop

}
