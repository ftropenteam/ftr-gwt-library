package eu.future.earth.gwt.client.ui.general;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.FlowPanel;

import eu.future.earth.gwt.client.ui.event.ClickTouchEvent;
import eu.future.earth.gwt.client.ui.event.ClickTouchHandler;
import eu.future.earth.gwt.client.ui.event.HasClickTouchHandlers;

public class FlowClickPanel extends FlowPanel implements HasClickTouchHandlers {

	public FlowClickPanel() {
		super();
	}

	private boolean init = true;

	@Override
	public HandlerRegistration addClickHandler(ClickTouchHandler handler) {
		if (init) {
			DOM.sinkEvents(getElement(), Event.ONCLICK);
			DOM.setEventListener(getElement(), new EventListener() {
				@Override
				public void onBrowserEvent(Event event) {
					fireClick(event);
				}
			});
			init = false;
		}
		return super.addHandler(handler, ClickTouchEvent.getType());
	}

	private void fireClick(Event event) {
		ClickTouchEvent.fire(this);
	}
}
