package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.dom.client.Document;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;

public class ElementWithPopup extends Composite {
	protected FlowPanel buttonAndPopup = new FlowPanel();

	protected FlowPanel pops = new FlowPanel();

	private HandlerRegistration handler;

	public ElementWithPopup() {
		super();
		initWidget(buttonAndPopup);
	}

	public void displayPopup() {
		pops.setVisible(true);
		if (handler == null) {
			handler = Event.addNativePreviewHandler(new NativePreviewHandler() {
				@Override
				public void onPreviewNativeEvent(final NativePreviewEvent event) {
					final int eventType = event.getTypeInt();
					switch (eventType) {
					case Event.ONMOUSEMOVE:
						//mouse tracking logic?
						break;
					case Event.ONCLICK:
						final int xx = event.getNativeEvent().getClientX();
						final int yy = event.getNativeEvent().getClientY() + Document.get().getScrollTop();
						//Only if outside
						final int xstart = pops.getAbsoluteLeft();
						final int xend = xstart + pops.getOffsetWidth();

						final int ystart = pops.getAbsoluteTop();
						final int yend = ystart + pops.getOffsetHeight();
						if (!((xx >= xstart && xx <= xend) && (yy >= ystart && yy <= yend))) {
							hidePopup();
						}
						break;
					default:
						// not interested in other events
					}
				}
			});
		}
	}

	public void hidePopup() {
		pops.setVisible(false);
		if (handler != null) {
			handler.removeHandler();
			handler = null;
		}
	}
}
