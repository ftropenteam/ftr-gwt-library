package eu.future.earth.gwt.client.ui.actionevent;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;


public interface HasActionHandlers<T> extends HasHandlers {

    HandlerRegistration addActionHandler(ActionHandler<T> handler);

} 
