package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;
import eu.future.earth.gwt.client.ui.event.ClickTouchHandler;

public class FontAwesomeTouchToggleButton extends FontAwesomeTouchButton implements HasValue<Boolean>, HasValueChangeHandlers<Boolean> {
	private boolean value = false;

	public FontAwesomeTouchToggleButton() {
		super();
		init();
	}

	public FontAwesomeTouchToggleButton(final String newText, final String textOnButton, final ClickTouchHandler newHandler) {
		super(newText, textOnButton, newHandler);
		init();
	}

	public FontAwesomeTouchToggleButton(final String newText, final String textOnButton) {
		super(newText, textOnButton);
		init();
	}

	public FontAwesomeTouchToggleButton(final String newText) {
		super(newText);
		init();
	}

	private void init() {
		super.addStyleNameToMain("ftr-toggle");
		super.addClickHandler(naviEvent -> setValue(!value, true));
	}

	@Override
	public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
		return super.addHandler(handler, ValueChangeEvent.getType());
	}

	@Override
	public Boolean getValue() {
		return value;
	}

	@Override
	public void setValue(final Boolean newValue) {
		setValue(newValue, false);
	}

	private void setState() {
		if (value) {
			super.addStyleNameToMain("ftr-active");
		} else {
			super.removeStyleNameToMain("ftr-active");
		}
	}

	@Override
	public void setValue(final Boolean newValue, final boolean fireEvents) {
		value = newValue;
		setState();
		if (fireEvents) {
			ValueChangeEvent.fire(this, value);
		}
	}

	public void setSelected(final boolean newValue) {
		setValue(newValue, false);

	}

}
