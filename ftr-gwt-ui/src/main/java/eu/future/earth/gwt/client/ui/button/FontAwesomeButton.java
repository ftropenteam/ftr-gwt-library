package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasText;

/**
 * Anchor styled as a button
 */
public class FontAwesomeButton extends Composite implements HasEnabled, HasClickHandlers, ClickHandler, Focusable, HasText {

	protected FocusPanel main = new FocusPanel();

	private boolean enabled = true;

	public FontAwesomeButton() {
		this(null);
	}

	public FontAwesomeButton(final String newText) {
		this(newText, null);
	}

	public FontAwesomeButton(final String newText, final String newT) {
		super();
		initWidget(main);
		setSymbol(newText);
		main.addClickHandler(this);
		super.setStyleName("fontawesome-img-button");
		super.addStyleName("font-icon");
		setText(newT);
	}

	@Override
	public HandlerRegistration addClickHandler(final ClickHandler handler) {
		return addHandler(handler, ClickEvent.getType());
	}

	public String getSymbol() {
		return main.getElement().getAttribute("symbol");
	}

	@Override
	public int getTabIndex() {
		return main.getTabIndex();
	}

	@Override
	public String getText() {
		return main.getElement().getInnerHTML();
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void onBrowserEvent(final Event event) {
		if (enabled) {
			super.onBrowserEvent(event);
		} else {
			event.preventDefault();
		}
	}

	@Override
	public final void onClick(final ClickEvent event) {

	}

	@Override
	public void setAccessKey(final char key) {
		main.setAccessKey(key);
	}

	@Override
	public void setEnabled(final boolean newEnabled) {
		enabled = newEnabled;
		if (enabled) {
			super.addStyleName("enabled");
			super.removeStyleName("disabled");
		} else {
			super.addStyleName("disabled");
			super.removeStyleName("enabled");
		}
	}

	@Override
	public void setFocus(final boolean focused) {
		main.setFocus(focused);
	}

	public void setSymbol(final String newText) {
		if (newText != null) {
			final int i = Integer.parseInt(newText, 16);
			final String news = Character.toString((char) i);

			main.getElement().setAttribute("symbol", news);
		}
	}

	@Override
	public void setTabIndex(final int index) {
		main.setTabIndex(index);
	}

	@Override
	public void setText(final String text) {
		main.getElement().setInnerText(text);
	}

}
