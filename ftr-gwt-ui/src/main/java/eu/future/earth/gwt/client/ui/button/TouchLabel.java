package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;

import eu.future.earth.gwt.client.ui.event.ClickTouchEvent;
import eu.future.earth.gwt.client.ui.event.ClickTouchHandler;
import eu.future.earth.gwt.client.ui.event.HasClickTouchHandlers;

public class TouchLabel extends Composite implements HasClickHandlers, Focusable, HasClickTouchHandlers, HasText {

	protected FocusPanel main = new FocusPanel();

	protected InlineLabel text = new InlineLabel();

	@UiConstructor
	public TouchLabel() {
		this(null, null);
	}

	public TouchLabel(final String newText) {
		this(newText, null);
	}

	public TouchLabel(final Image newImage) {
		this(null, newImage);
	}

	public TouchLabel(final String newText, final Image newImage) {
		this(newText, newImage, true);
	}

	public TouchLabel(final String newText, final Image newImage, final boolean setTouch) {
		super();
		initWidget(main);
		super.addStyleName("btn");
		main.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(final ClickEvent event) {
				if (enabled) {
					handleClick();
				}
			}

		});
		setImage(newImage);
		if (newText != null) {
			main.getElement().appendChild(text.getElement());
			text.setText(newText);
		}
	}

	public void handleClick() {
		ClickTouchEvent.fire(this);
	}

	public void addStyleNameToMain(final String newStyle) {
		main.addStyleName(newStyle);
	}

	protected void removeStyleNameToMain(final String newStyle) {
		main.removeStyleName(newStyle);
	}

	private Element imagePlaced = null;

	public void setImage(final Image newImage) {
		if (imagePlaced != null) {
			main.getElement().removeChild(imagePlaced);
		}

		if (newImage != null) {
			imagePlaced = newImage.getElement();
			main.getElement().insertFirst(imagePlaced);
		}
	}

	@Override
	public void setText(final String newText) {
		text.setText(newText);
		if (!main.getElement().isOrHasChild(text.getElement())) {
			main.getElement().appendChild(text.getElement());
		}
	}

	private boolean enabled = true;

	public void setEnabled(final boolean newState) {
		if (enabled != newState) {
			if (enabled) {
				super.addStyleName("ftr-disabled");
			} else {
				super.removeStyleName("ftr-disabled");
			}
			enabled = newState;

		}
	}

	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void onBrowserEvent(final Event event) {
		if (enabled) {
			// if (DOM.eventGetType(event) == Event.ONCLICK) {
			//
			// }
			super.onBrowserEvent(event);
			// handleClick();
		} else {
			event.preventDefault();
		}
	}

	@Override
	public HandlerRegistration addClickHandler(final ClickHandler handler) {
		return addHandler(handler, ClickEvent.getType());
	}

	@Override
	public int getTabIndex() {
		return main.getTabIndex();
	}

	@Override
	public void setAccessKey(final char key) {
		main.setAccessKey(key);
	}

	@Override
	public void setFocus(final boolean focused) {
		main.setFocus(focused);
	}

	@Override
	public void setTabIndex(final int index) {
		main.setTabIndex(index);
	}

	@Override
	public HandlerRegistration addClickHandler(final ClickTouchHandler handler) {
		return super.addHandler(handler, ClickTouchEvent.getType());
	}

	@Override
	public String getText() {
		return text.getText();
	}

}
