package eu.future.earth.gwt.client.ui.panels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.help.HelpItem;
import eu.future.earth.gwt.client.logic.UUID;
import eu.future.earth.gwt.client.ui.general.EeGwtFrameworkCss;

public abstract class AbstractLabelFieldPanel extends Composite implements HasWidgets {

    protected List<String> ids = new ArrayList<>();

    protected HashMap<String, Widget> labels = new HashMap<>();

    protected HashMap<String, Boolean> req = new HashMap<>();

    protected HashMap<String, Widget> edits = new HashMap<>();

    public abstract void setRequiredMessage(Field birthDayRef);

    @Override
    public void add(final Widget w) {
        if (w instanceof Field) {
            final Field real = (Field) w;
            addField(real);
            if (real.isRequired()) {
                setRequired(real.getLabel(), true);
            }
        } else {
            addRow(w);
        }
    }

    public Field addField(final Field real) {
        return addField(real, null);
    }

    public Field addField(final Field real, Field below) {
        if (real.getId() == null) {
            if (real.getLabel() == null) {
                real.setId(UUID.uuid());
            } else {
                real.setId(real.getLabel());
            }
        }
        real.setLabelFieldHolder(this);
        addFieldInternal(real, below);
        return real;
    }


    protected abstract void addFieldInternal(Field real);

    protected abstract void addFieldInternal(Field real, Field below);

    public Field addRawRow(final String id, final Widget item) {
        return addRawRow(id, item, false);
    }

    private Field addRawRow(final String id, final Widget item, final boolean alignTop) {
        final Field res = new Field();
        res.setId(id);
        res.setShift(true);
        res.setAlignTop(alignTop);
        res.setManager(item);
        return addField(res);
    }

    public Field addRawRow(final Widget item) {
        return addRawRow(null, item, false);
    }

    public Field addRawRow(final Widget item, final boolean alignTop) {
        return addRawRow(null, item, alignTop);
    }

    public final Field addRow(final String labelName, final Widget item) {
        return addRow(labelName, item, false);
    }

    public final Field addRow(final String id, final String labelName, final Widget item) {
        return addRow(id, labelName, item, false);
    }


    public Field addRow(final String id, final String labelName, final Widget item, final boolean alignTop) {
        final Field res = new Field();
        res.setLabel(labelName);
        res.setId(id);
        res.setAlignTop(alignTop);
        res.setManager(item);
        return addField(res);
    }

    public Field addRow(final String labelName, final Widget item, final boolean alignTop) {
        return addRow(null, labelName, item, alignTop);
    }

    public Field addRow(final String labelName, final Widget item, final boolean alignTop, String help) {
        final Field res = new Field();
        res.setLabel(labelName);
        res.setAlignTop(alignTop);
        res.setManager(item);
        res.setHelp(help);
        return addField(res);
    }

    public final Field addRow(final String labelName, final Widget item, String help) {
        return addRow(labelName, item, false, help);
    }


    public Field addRow(final String marginTop, final Widget top, final Widget topUnit) {
        final FlowPanel wrapper = new FlowPanel();
        wrapper.addStyleName("com-wrapper");
        if (top instanceof TextBox && topUnit instanceof HelpItem) {
            wrapper.setStyleName("text-number-with-help");
        }
        wrapper.add(top);
        wrapper.add(topUnit);
        top.addStyleName("marginOnTheRight");
//		topUnit.addStyleName("leftSpace");
        return addRow(marginTop, wrapper);
    }

    public final Field addRow(final Widget item) {
        if (item instanceof Field) {
            return addField((Field) item);
        } else {
            return addTwoRows(null, item);
        }
    }

    public Field addRowNoLabel(final String id, final Widget item) {
        return addTwoRows(id, null, item);
    }

    public Field addSection(final String label) {
        final InlineLabel secionLabel = new InlineLabel(label);
        return addSection(secionLabel);
    }

    public final Field addSection(final Widget item) {
        item.addStyleName("ftr-section");
        return addRow(item);
    }

    public abstract Field addTwoRows(Field result);

    public Field addTwoRows(final String id, final String labelName, final Widget managedItem) {
        return addTwoRows(id, labelName, managedItem, null);
    }

    public Field addTwoRows(final String id, final String labelName, final Widget managedItem, String help) {
        final Field result = new Field();
        result.setId(id);
        result.setManager(managedItem);
        result.setHelp(help);
        result.setLabel(labelName);
        if (labelName != null && result.getId() == null) {
            result.setId(labelName);
        }
        result.setAlignTop(true);
        result.setLabelFieldHolder(this);
        return addTwoRows(result);
    }

    public Field addTwoRows(final String labelName, final Widget item) {
        return addTwoRows(null, labelName, item);
    }

    public Field addTwoRows(final String labelName, final Widget item, String help) {
        return addTwoRows(null, labelName, item, help);
    }

    public void checkRequiredLegendas() {
        showRequired(hasRequiredShowing());
    }

    public abstract void clearFeedback();

    public Widget getEdit(final String name) {
        return edits.get(name);
    }

    public abstract String getLabel(String name);

    public boolean hasFields() {
        return !ids.isEmpty();
    }

    public boolean hasRequiredShowing() {
        for (final String label : ids) {
            final Widget cur = edits.get(label);
            if (cur != null && cur.isVisible()) {
                final Boolean setToValue = req.get(label);
                if (setToValue != null && setToValue) {
                    return true;
                }
            }
        }
        return false;
    }

    protected void setElements(final String name, final Widget label, final Widget edit) {
        if (name != null) {
            ids.add(name);
            if (label != null) {
                labels.put(name, label);
            }
            if (edit != null) {
                edits.put(name, edit);
            }
        }
    }

    public abstract void setError(String id, boolean newState);

    public void setErrorForLabel(final String id, final boolean newState) {
        final Widget label = labels.get(id);
        if (label != null) {
            if (newState) {
                setErrorLabel(label);
            } else {
                clearErrorLabel(label);
            }
        }
        final Widget edit = edits.get(id);
        if (edit != null) {
            if (newState) {
                setError(edit);
            } else {
                clearError(edit);
            }
        }
    }

    public void setFeedbackMessage(Field key, String newFeedback) {
        if (key != null) {
            setFeedbackMessage(key.getId(), newFeedback);
        }
    }

    public abstract void setFeedbackMessage(String key, String newFeedback);

    public void setFieldRequired(final Field real, final boolean newState) {
        setRequired(real.getLabel(), newState);
    }

    public abstract void setLabel(String id, String newText);

    public abstract void setRequired(String name, boolean newState);

    public void setRequiredForLabel(final Field name) {
        setRequired(name.getId(), name.isRequired());
    }

    public void setRequiredForLabel(final String id, final boolean newState) {
        if (id != null) {
            req.put(id, newState);
            final Widget label = labels.get(id);
            if (label != null) {
                if (newState) {
                    setRequiredLabel(label);
                } else {
                    clearRequiredLabel(label);
                }
            }
            final Widget edit = edits.get(id);
            if (edit != null) {
                if (newState) {
                    setRequiredToTextField(edit);
                } else {
                    clearRequiredToTextField(edit);
                }
            }
        }

    }

    public abstract void setVisible(String name, boolean newState);

    public void setVisibleElements(final String id, final boolean newState) {
        final Widget label = labels.get(id);
        if (label != null) {
            label.setVisible(newState);
        }
        final Widget edit = edits.get(id);
        if (edit != null) {
            edit.setVisible(newState);
        }
    }

    public void setVisibleForLabel(final Field name, final boolean newState) {
        setVisible(name.getId(), newState);
    }

    public abstract void setWordWrapForLabel(String labelName, boolean b);

    public void showFilled() {
        final Iterator<String> labelsName = labels.keySet().iterator();
        while (labelsName.hasNext()) {
            final String labelName = labelsName.next();
            final Widget item = edits.get(labelName);
            if (item instanceof HasText) {
                final HasText real = (HasText) item;
                if (real.getText() != null && real.getText().length() > 0) {
                    setVisibleElements(labelName, true);
                } else {
                    setVisibleElements(labelName, false);
                }
            }
        }
    }

    public abstract void showRequired(boolean hasRequired);

    public static void clearError(final Widget element) {
        if (element != null) {
            element.removeStyleName(EeGwtFrameworkCss.ERROR_TEXT_FIELD);
        }
    }

    public static void clearErrorLabel(final Widget element) {
        if (element != null) {
            element.removeStyleName(EeGwtFrameworkCss.ERROR_LABEL);
        }
    }

    public static void clearRequiredLabel(final Widget element) {
        if (element != null) {
            element.removeStyleName(EeGwtFrameworkCss.REQUIRED_LABEL);
        }
    }

    public static void clearRequiredToTextField(final Widget element) {
        if (element != null) {
            element.removeStyleName(EeGwtFrameworkCss.REQUIRED_TEXT_FIELD);
        }
    }

    public static void setError(final Widget element) {
        if (element != null) {
            element.addStyleName(EeGwtFrameworkCss.ERROR_TEXT_FIELD);
        }
    }

    // Error handling
    public static void setErrorLabel(final Widget element) {
        if (element != null) {
            element.addStyleName(EeGwtFrameworkCss.ERROR_LABEL);
        }
    }

    public static void setRequiredLabel(final Widget element) {
        if (element != null) {
            element.addStyleName(EeGwtFrameworkCss.REQUIRED_LABEL);
        }
    }

    public static void setRequiredToTextField(final Widget element) {
        if (element != null) {
            element.addStyleName(EeGwtFrameworkCss.REQUIRED_TEXT_FIELD);
        }
    }

    public void setVisible(Field emailRef, boolean value) {
        if (emailRef != null) {
            emailRef.setVisible(value);
        }
    }
}
