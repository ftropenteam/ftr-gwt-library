package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.i18n.client.Constants;

public interface ButtonsTexts extends Constants {

	String nothingSelected();

	String requiredFields();

	String close();

	String ok();

	String cancel();

	String select();

}
