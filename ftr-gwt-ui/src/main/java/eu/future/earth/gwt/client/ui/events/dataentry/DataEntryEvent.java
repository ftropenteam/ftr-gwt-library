package eu.future.earth.gwt.client.ui.events.dataentry;

import com.google.gwt.event.shared.GwtEvent;

public class DataEntryEvent extends GwtEvent<DataEntryHandler> {

	public enum DataEntryEventType {
		KeyEnter, KeyUp, FocusLost, FocusGained, ValueChanged
	}

	public static boolean isValidKeyCode(int keyCode) {
		if (keyCode == 13) {
			return true;
		}
		return false;

	}

	private final DataEntryEventType command;

	public DataEntryEvent(DataEntryEventType newEnterEvent) {
		super();
		command = newEnterEvent;
	}

	/**
	 * Handler type.
	 */
	private static Type<DataEntryHandler> TYPE;

	/**
	 * Fires a before selection event on all registered handlers in the handler manager. If no such handlers exist, this
	 * method will do nothing.
	 * 
	 * @param source
	 *            the source of the handlers
	 * @return the event so that the caller can check if it was canceled, or null if no handlers of this event type have
	 *         been registered
	 */
	public static DataEntryEvent fire(HasDataEntryEventHandlers source) {
		// If no handlers exist, then type can be null.
		if (TYPE != null) {
			final DataEntryEvent event = new DataEntryEvent(DataEntryEventType.KeyUp);
			source.fireEvent(event);
			return event;
		}
		return null;
	}

	public static DataEntryEvent fire(HasDataEntryEventHandlers source, DataEntryEventType newIsEnterEvent) {
		// If no handlers exist, then type can be null.
		if (TYPE != null) {
			final DataEntryEvent event = new DataEntryEvent(newIsEnterEvent);
			source.fireEvent(event);
			return event;
		}
		return null;
	}

	/**
	 * Gets the type associated with this event.
	 * 
	 * @return returns the handler type
	 */
	public static Type<DataEntryHandler> getType() {
		if (TYPE == null) {
			TYPE = new Type<DataEntryHandler>();
		}
		return TYPE;
	}

	@Override
	public final Type<DataEntryHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DataEntryHandler handler) {
		handler.handleDataEntryEvent(this);
	}

	public DataEntryEventType getCommand() {
		return command;
	}

}
