package eu.future.earth.gwt.client.ui.panels;

import java.util.ArrayList;
import java.util.HashMap;import java.util.Map;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.help.HelpItem;
import eu.future.earth.gwt.client.logic.UUID;
import eu.future.earth.gwt.client.ui.ImageResources;
import eu.future.earth.gwt.client.ui.general.EeGwtFrameworkCss;
import eu.future.earth.gwt.client.ui.general.UiUtil;

public class LabelFieldPanel extends CommonBasedPanel {

	private enum Cols {

		LABEL_COLUMN(0), FIELD_COLUMN(1), HELP_COLUMN(2);

		private final int pos;

		private Cols(final int newPos) {
			pos = newPos;
		}

		public int getPos() {
			return pos;
		}
	}


	public void setRequiredMessage(Field birthDayRef) {
		birthDayRef.getLabelFieldHolder().setFeedbackMessage(birthDayRef, "" + birthDayRef.getLabel());
	}

	private final static int PADDING_LEFT = 6;

	private final static int PADDING_RIGHT = 8;

	private FlexTable holderGrid = null;


	private final Map<String, List<Integer>> idToRows = new HashMap<>();

	private int row = 0;

	private final InlineLabel requiredLabel = new InlineLabel(ImageResources.STATIC_TEXT.requiredFields());

	public LabelFieldPanel() {
		this(false);
	}

	public LabelFieldPanel(final boolean fullWidth) {
		super();
		holderGrid = new FlexTable();
		holderGrid.addStyleName("label-field-grid-panel");
		if (fullWidth) {
			holderGrid.addStyleName("label-field-grid-panel-full");
		}
		initWidget(holderGrid);
		holderGrid.setCellPadding(1);
		requiredLabel.setVisible(false);
	}


	public void showAsCompact() {
		holderGrid.addStyleName("compact");
	}

	@Override
	protected void addFieldInternal(final Field real) {
		addFieldInternal(real, null);
	}

	@Override
	protected void addFieldInternal(final Field real, Field below) {

		final int labelCol = Cols.LABEL_COLUMN.getPos();
		final int fieldCol = Cols.FIELD_COLUMN.getPos();
		InlineLabel labelWidget = null;
		if (real.getLabel() != null) {
			labelWidget = new InlineLabel(real.getLabel());
			holderGrid.setWidget(row, labelCol, labelWidget);
			labelWidget.setStyleName("common-field-label");
		}
		super.setElements(real.getId(), labelWidget, real.getManager());
		if (real.isAlignTop()) {
			holderGrid.getCellFormatter().setVerticalAlignment(row, labelCol, HasVerticalAlignment.ALIGN_TOP);
			// holderGrid.getCellFormatter().getElement(row, labelCol).getStyle().setPaddingTop(paddingTop, Unit.PX);
		}
		holderGrid.getCellFormatter().setStyleName(row, labelCol, "lbl-col");

		holderGrid.setWidget(row, fieldCol, real.getManager());

		{
			final FeedbackHolder errorHolder = new FeedbackHolder();
			super.setFeedbackHolder(real.getId(), errorHolder);
			holderGrid.getCellFormatter().getElement(row, fieldCol).appendChild(errorHolder.getElement());
		}

		if (real.hasHelp()) {
			final HelpItem helpButton = new HelpItem(real.getHelp());
			holderGrid.setWidget(row, Cols.HELP_COLUMN.getPos(), helpButton);
		}

		holderGrid.getCellFormatter().setHorizontalAlignment(row, fieldCol, HasHorizontalAlignment.ALIGN_LEFT);
		if (real.isAlignTop()) {
			holderGrid.getCellFormatter().setVerticalAlignment(row, fieldCol, HasVerticalAlignment.ALIGN_TOP);
		}
		final List<Integer> rows = new ArrayList<>();
		rows.add(row);
		idToRows.put(real.getId(), rows);
		row++;

	}

	@Override
	public Field addTwoRows(final Field result) {
		if (result.getId() == null) {
			result.setId(UUID.uuid());
		}
		final List<Integer> rows = new ArrayList<>();
		InlineLabel label = null;
		if (result.getLabel() != null) {
			label = new InlineLabel(result.getLabel());
			label.getElement().getStyle().setFontWeight(FontWeight.BOLD);

			if (result.hasHelp()) {
				final HelpItem helpButton = new HelpItem(result.getHelp());
				final FlowPanel ho = new FlowPanel();
				ho.add(label);
				ho.add(helpButton);
				holderGrid.setWidget(row, 0, ho);
			} else {
				holderGrid.setWidget(row, 0, label);
			}

			holderGrid.getFlexCellFormatter().setColSpan(row, 0, Cols.values().length);
			if (result.isIdent()) {
				holderGrid.getFlexCellFormatter().getElement(row, 0).getStyle().setPaddingLeft(PADDING_LEFT, Unit.PX);
			}
			rows.add(row);
			row++;

		} else {
			if (result.hasHelp()) {
				final HelpItem helpButton = new HelpItem(result.getHelp());
				holderGrid.setWidget(row, 0, helpButton);
			}
		}

		if (result.getManager() != null) {
			final int col = Cols.LABEL_COLUMN.getPos();
			holderGrid.setWidget(row, col, result.getManager());
			if (result.isIdent()) {
				holderGrid.getFlexCellFormatter().getElement(row, col).getStyle().setPaddingLeft(PADDING_LEFT, Unit.PX);
			}
			if (result.getManager() instanceof RichTextArea) {
				result.getManager().getElement().getStyle().setWidth(100, Unit.PCT);
				holderGrid.getFlexCellFormatter().getElement(row, col).getStyle().setPaddingRight(PADDING_RIGHT, Unit.PX);
			}
			holderGrid.getFlexCellFormatter().setColSpan(row, col, Cols.values().length);
			rows.add(row);
			row++;
		}

		setElements(result.getId(), label, result.getManager());
		idToRows.put(result.getId(), rows);
		// idToColMap.put(result.getId(), 1);
		return result;
	}

	@Override
	public void clear() {
		holderGrid.clear();
	}

	public void clearButtons() {
		row = 0;
		holderGrid.clear();
		idToRows.clear();
	}

	@Override
	public String getLabel(final String name) {
		final Widget to = labels.get(name);
		return UiUtil.getTextOnWidget(to);
	}

	@Override
	public Iterator<Widget> iterator() {
		return holderGrid.iterator();
	}

	@Override
	public boolean remove(final Widget w) {
		return holderGrid.remove(w);
	}

	public void setBorderSpacing(final int va) {
		holderGrid.setCellSpacing(va);
	}

	@Override
	public void setError(final String name, final boolean newState) {
		super.setErrorForLabel(name, newState);
		final Widget label = labels.get(name);
		if (label != null) {
			if (newState) {
				label.addStyleName(EeGwtFrameworkCss.REQUIRED_INDICATOR);
			} else {
				label.removeStyleName(EeGwtFrameworkCss.REQUIRED_INDICATOR);
			}
		}
	}

	@Override
	public void setLabel(final String name, final String newText) {
		final Widget to = labels.get(name);
		if (to instanceof HasText) {
			final HasText re = (HasText) to;
			re.setText(newText);
		}

	}

	@Override
	public void setRequired(final String name, final boolean newState) {
		super.setRequiredForLabel(name, newState);
		final Widget label = labels.get(name);
		if (label != null) {
			if (newState) {
				label.addStyleName(EeGwtFrameworkCss.REQUIRED_INDICATOR);
			} else {
				label.removeStyleName(EeGwtFrameworkCss.REQUIRED_INDICATOR);
			}
		}
		checkRequiredLegendas();
	}

	public void setRequiredAndVisible(final Field name, final boolean newState) {
		name.changeRequired(newState);
		name.setVisible(newState);
	}

	public void setRequiredAndVisible(final String name, final boolean newState) {
		setRequired(name, newState);
		setVisible(name, newState);
	}

	public void setVisible(final Field currentField, final boolean newState) {
		if (currentField != null) {
			currentField.setVisible(newState);
		}
	}

	@Override
	public void setVisible(final String name, final boolean newState) {
		if (name != null && idToRows.containsKey(name)) {
			final List<Integer> rows = idToRows.get(name);
			for (final Integer currentRow : rows) {
				if (currentRow != null && currentRow > -1) {
					holderGrid.getRowFormatter().setVisible(currentRow, newState);
				}
			}
		}
		if (name != null) {
			setVisibleElements(name, newState);
		}
	}

	@Override
	public void setWordWrapForLabel(final String name, final boolean newState) {
		if (name != null && idToRows.containsKey(name)) {
			final List<Integer> rows = idToRows.get(name);
			for (final Integer currentRow : rows) {
				if (holderGrid.isCellPresent(currentRow, Cols.LABEL_COLUMN.getPos())) {
					holderGrid.getCellFormatter().setWordWrap(currentRow, Cols.LABEL_COLUMN.getPos(), newState);
				}
			}
		}
	}

	@Override
	public void showFilled() {
		final Iterator<String> labelsName = labels.keySet().iterator();
		while (labelsName.hasNext()) {
			final String labelName = labelsName.next();
			final Widget item = edits.get(labelName);
			if (item instanceof HasText) {
				final HasText real = (HasText) item;
				if (real.getText() != null && real.getText().length() > 0) {
					setVisible(labelName, true);
				} else {
					setVisible(labelName, false);
				}
			}

		}
	}

	@Override
	public void showRequired(final boolean hasRequired) {
		requiredLabel.setVisible(hasRequired);
	}

}
