package eu.future.earth.gwt.client.ui.events.enter;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;


public interface HasKeyEnterEventHandlers extends HasHandlers {

	HandlerRegistration addKeyEnterHandler(KeyEnterHandler handler);

}
