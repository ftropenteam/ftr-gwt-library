package eu.future.earth.gwt.client.help;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;

import eu.future.earth.gwt.client.ui.button.FontAwesomeButton;
import eu.future.earth.gwt.client.ui.button.FontAwsomeIconCodes;

public class HelpItem extends Composite implements ClickHandler {

	private final String message;

	private FontAwesomeButton button = new FontAwesomeButton(FontAwsomeIconCodes.FA_QUESTION_CIRCLE_O);

	public HelpItem(final String helpMessage) {
		super();
		initWidget(button);
		message = helpMessage;
		button.addClickHandler(this);
		button.addStyleName("help-marker");
	}

	@Override
	public void onClick(final ClickEvent event) {
		final HelpPopup info = new HelpPopup(message);
		info.showRelativeTo(button);
	}

}
