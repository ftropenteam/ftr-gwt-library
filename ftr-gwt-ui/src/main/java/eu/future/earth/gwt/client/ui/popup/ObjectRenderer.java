package eu.future.earth.gwt.client.ui.popup;

import com.google.gwt.user.client.ui.Widget;


public interface ObjectRenderer<D> {

    Widget getWidget(D item);
    
}
