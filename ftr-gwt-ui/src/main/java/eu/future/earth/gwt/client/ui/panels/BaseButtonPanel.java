/*
 * Copyright 2009 EasyEnterprise, all rights reserved.
 */
package eu.future.earth.gwt.client.ui.panels;

import java.util.List;

import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public abstract class BaseButtonPanel extends Composite {

	public BaseButtonPanel() {
		super();
	}

	abstract public void clearButtons();

	abstract public int numberOfButtons();

	@UiChild(tagname = "addButton")
	abstract public void addButton(Widget button);

	public void setChildren(List<Widget> children) {
		if (children != null) {
			for (Widget child : children) {
				addButton(child);
			}
		}
	}

}
