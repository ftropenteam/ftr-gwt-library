package eu.future.earth.gwt.client.ui.general;

import java.util.Date;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.Dictionary;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.ui.button.CleanDialog;

public class WindowUtils {

	private final static int DIALOG_MARGIN = 20;

	public static boolean allowsFullScreen() {
		return testAllowsFullscreen();
	}

	public static void cancelFullScreen() {
		closeFullScreen();
	}

	protected static native void changeUrl(String url)
	/*-{
		$wnd.location.href = url;
	}-*/
	;

	protected static native void changeUrlAndBreakOut(String url)
	/*-{
	
		$wnd.top.location = url;
	
	}-*/
	;

	public static native void closeFullScreen() /*-{
												
												if ($doc.exitFullscreen) {
												$doc.exitFullscreen();
												} else if ($doc.mozCancelFullScreen) {
												$doc.mozCancelFullScreen();
												} else if ($doc.webkitCancelFullScreen) {
												$doc.webkitCancelFullScreen();
												}
												
												}-*/;

	public static boolean copySelectedToClipboard() {
		return copyToClipboard();
	}

	private static native boolean copyToClipboard() /*-{
													return $doc.execCommand('copy');
													}-*/;

	public static String createUrl(final String string, final String newHistoryToken) {
		String newBase = string;
		if (string.contains("#")) {
			newBase = string.substring(0, string.indexOf("#") + 1);
		}
		return newBase + newHistoryToken;
	}

	private static native void deselect(Element elem) /*-{
														if ($doc.selection && $doc.selection.createRange) {
														var selection = $wnd.getSelection();
														selection.removeAllRanges();
														} else if ($doc.createRange && $wnd.getSelection) {
														var selection = $wnd.getSelection();
														selection.removeAllRanges();
														
														}
														}-*/;


	public static native void deselect(String id) /*-{
		if ($doc.selection && $doc.selection.createRange) {
			var selection = $wnd.getSelection();
			selection.removeAllRanges();
		} else if ($doc.createRange && $wnd.getSelection) {
			var selection = $wnd.getSelection();
			selection.removeAllRanges();
		}
	}-*/;

	public static void deselectElement(final Element elem) {
		deselect(elem);
	}

	public static boolean getBooleanParameter(final Dictionary from, final String parameterName, final boolean defaultValue) {
		try {
			final String reservationStringId = from.get(parameterName);
			if (reservationStringId != null && !reservationStringId.isEmpty()) {
				return Boolean.parseBoolean(reservationStringId);
			}
		} catch (final Exception e) {
			return defaultValue;
		}
		return defaultValue;
	}

	public static boolean getBooleanParameter(final String parameterName) {
		final String reservationStringId = com.google.gwt.user.client.Window.Location.getParameter(parameterName);
		if (reservationStringId != null && !reservationStringId.isEmpty()) {
			return Boolean.parseBoolean(reservationStringId);
		}
		return false;
	}

	public static Date getDateParameter(final String parameterName) {
		final String reservationStringId = com.google.gwt.user.client.Window.Location.getParameter(parameterName);
		if (reservationStringId != null && !reservationStringId.isEmpty()) {
			return new Date(Long.parseLong(reservationStringId));
		}
		return null;
	}

	public static long getLongParameter(final Dictionary from, final String parameterName) {
		return getLongParameter(from, parameterName, -1);
	}

	public static long getLongParameter(final Dictionary from, final String parameterName, final long defaultValue) {
		try {
			final String reservationStringId = from.get(parameterName);
			if (reservationStringId != null && !reservationStringId.isEmpty()) {
				return Long.parseLong(reservationStringId);
			}
		} catch (final Exception e) {
			return defaultValue;
		}
		return defaultValue;
	}




	public static long getLongParameter(final String parameterName) {
		final String reservationStringId = com.google.gwt.user.client.Window.Location.getParameter(parameterName);
		if (reservationStringId != null && !reservationStringId.isEmpty()) {
			return Long.parseLong(reservationStringId);
		}
		return -1;
	}

	public static String getStringParameter(final Dictionary from, final String parameterName) {
		try {
			final String reservationStringId = from.get(parameterName);
			if (reservationStringId != null && !reservationStringId.isEmpty()) {
				return reservationStringId;
			}
		} catch (final Exception e) {
			return null;
		}
		return null;
	}

	public static String getStringParameter(final String parameterName) {
		final String reservationStringId = com.google.gwt.user.client.Window.Location.getParameter(parameterName);
		if (reservationStringId != null && !reservationStringId.isEmpty()) {
			return reservationStringId;
		}
		return null;
	}

	public static void gotoFullScreen() {
		toFullScreen();
	}

	public static void goToUrl(final String website) {
		changeUrl(website);
	}

	private static native void markText(Element elem) /*-{
														if ($doc.selection && $doc.selection.createRange) {
														var range = $doc.selection.createRange();
														range.moveToElementText(elem);
														range.select();
														} else if ($doc.createRange && $wnd.getSelection) {
														var range = $doc.createRange();
														range.selectNode(elem);
														var selection = $wnd.getSelection();
														selection.removeAllRanges();
														selection.addRange(range);
														}
														}-*/;


	public static native void markText(String id) /*-{
		if ($doc.selection && $doc.selection.createRange) {
			var range = $doc.selection.createRange();
			range.moveToElementText($doc.getElementById(id));
			range.select();
		} else if ($doc.createRange && $wnd.getSelection) {
			var range = $doc.createRange();
			range.selectNode($doc.getElementById(id));
			var selection = $wnd.getSelection();
			selection.removeAllRanges();
			selection.addRange(range);
		}
	}-*/;

	public static void markTextInElement(final Element elem) {
		markText(elem);
	}

	public static void sendToAdressAndBreakout(final String website) {
		if (website != null && !website.isEmpty()) {
			if (website.contains("http")) {
				changeUrlAndBreakOut(website);
			} else {
				changeUrlAndBreakOut("https://" + website);
			}
		} else {
			changeUrlAndBreakOut("/");
		}

	}

	public static void sendToAdressNoBreakOut(final String website) {
		if (website != null && website.length() > 0) {
			if (website.contains("http")) {
				changeUrl(website);
			} else {
				changeUrl("https://" + website);
			}
		} else {
			changeUrl("/");
		}

	}

	public static void setDialogToMaxSize(final CleanDialog dialog) {
		dialog.setGlassEnabled(true);
		dialog.setPixelSize(Window.getClientWidth() - DIALOG_MARGIN, Window.getClientHeight() - DIALOG_MARGIN);
	}

	public static void setDialogToMaxSizeOrSpecifiedWidth(final PopupPanel st, final int maxWidth, final int margin) {
		st.getElement().getStyle().setWidth(Math.min(maxWidth, (Window.getClientWidth() - margin)), Unit.PX);
	}

	public static void setToMaxSize(final Widget dialog) {
		dialog.setPixelSize(Window.getClientWidth() - DIALOG_MARGIN, Window.getClientHeight() - DIALOG_MARGIN);
	}

	public static void setToMaxSize(final Widget dialog, final int extraSpace) {
		dialog.setPixelSize(Window.getClientWidth() - DIALOG_MARGIN, Window.getClientHeight() - (DIALOG_MARGIN + extraSpace));
	}

	public static native boolean testAllowsFullscreen() /*-{
														var docElm = $doc.documentElement;
														if (docElm.requestFullscreen) {
														return true;
														} else if (docElm.mozRequestFullScreen) {
														return true;
														} else if (docElm.webkitRequestFullScreen) {
														return true;
														}
														return false;
														}-*/;

	public static native void toFullScreen() /*-{
												
												var docElm = $doc.documentElement;
												if (docElm.requestFullscreen) {
												docElm.requestFullscreen();
												} else if (docElm.mozRequestFullScreen) {
												docElm.mozRequestFullScreen();
												} else if (docElm.webkitRequestFullscreen) {
												docElm.webkitRequestFullscreen ();
												}
												}-*/;
}
