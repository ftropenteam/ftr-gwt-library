/*
 * Copyright 2009 EasyEnterprise, all rights reserved.
 */
package eu.future.earth.gwt.client.ui.event;

import com.google.gwt.event.shared.GwtEvent;

public class ClickTouchEvent extends GwtEvent<ClickTouchHandler> {

	/**
	 * Handler type.
	 */
	private static Type<ClickTouchHandler> TYPE;

	/**
	 * Fires a before selection event on all registered handlers in the handler manager. If no such handlers exist, this
	 * method will do nothing.
	 * 
	 * @param source
	 *            the source of the handlers
	 * @return the event so that the caller can check if it was canceled, or null if no handlers of this event type have
	 *         been registered
	 */
	public static ClickTouchEvent fire(HasClickTouchHandlers source) {
		// If no handlers exist, then type can be null.
		if (TYPE != null) {
			final ClickTouchEvent event = new ClickTouchEvent();
			source.fireEvent(event);
			return event;
		}
		return null;
	}

	/**
	 * Gets the type associated with this event.
	 * 
	 * @return returns the handler type
	 */
	public static Type<ClickTouchHandler> getType() {
		if (TYPE == null) {
			TYPE = new Type<ClickTouchHandler>();
		}
		return TYPE;
	}

	protected ClickTouchEvent() {
		super();
	}

	@Override
	public final Type<ClickTouchHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ClickTouchHandler handler) {
		handler.onClick(this);
	}
}
