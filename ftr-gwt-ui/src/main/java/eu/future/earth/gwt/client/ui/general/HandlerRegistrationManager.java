package eu.future.earth.gwt.client.ui.general;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.HandlerRegistration;

public class HandlerRegistrationManager {
	private List<HandlerRegistration> registrations = new ArrayList<HandlerRegistration>();

	public void clearListeners() {
		for (HandlerRegistration registration : registrations) {
			registration.removeHandler();
		}
		registrations.clear();
	}

	public void addHanlder(HandlerRegistration newHandler) {
		if (newHandler != null) {
			registrations.add(newHandler);
		}
	}
}
