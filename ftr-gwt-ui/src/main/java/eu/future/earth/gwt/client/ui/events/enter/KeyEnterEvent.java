package eu.future.earth.gwt.client.ui.events.enter;

import com.google.gwt.event.shared.GwtEvent;

public class KeyEnterEvent extends GwtEvent<KeyEnterHandler> {

	public static boolean isValidKeyCode(int keyCode) {
		if (keyCode == 13) {
			return true;
		}
		return false;

	}

	public KeyEnterEvent() {
		super();
	}

	/**
	 * Handler type.
	 */
	private static Type<KeyEnterHandler> TYPE;

	/**
	 * Fires a before selection event on all registered handlers in the handler manager. If no such handlers exist, this
	 * method will do nothing.
	 * 
	 * @param source
	 *            the source of the handlers
	 * @return the event so that the caller can check if it was canceled, or null if no handlers of this event type have
	 *         been registered
	 */
	public static KeyEnterEvent fire(HasKeyEnterEventHandlers source) {
		// If no handlers exist, then type can be null.
		if (TYPE != null) {
			final KeyEnterEvent event = new KeyEnterEvent();
			source.fireEvent(event);
			return event;
		}
		return null;
	}

	/**
	 * Gets the type associated with this event.
	 * 
	 * @return returns the handler type
	 */
	public static Type<KeyEnterHandler> getType() {
		if (TYPE == null) {
			TYPE = new Type<KeyEnterHandler>();
		}
		return TYPE;
	}

	@Override
	public final Type<KeyEnterHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(KeyEnterHandler handler) {
		handler.handleKeyEnterEvent(this);
	}

}
