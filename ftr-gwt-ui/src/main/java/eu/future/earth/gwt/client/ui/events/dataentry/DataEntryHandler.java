package eu.future.earth.gwt.client.ui.events.dataentry;

import com.google.gwt.event.shared.EventHandler;

public interface DataEntryHandler extends EventHandler {

	void handleDataEntryEvent(DataEntryEvent newEvent);

}
