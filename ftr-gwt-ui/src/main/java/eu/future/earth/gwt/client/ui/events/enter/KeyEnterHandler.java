package eu.future.earth.gwt.client.ui.events.enter;

import com.google.gwt.event.shared.EventHandler;

public interface KeyEnterHandler extends EventHandler {

	void handleKeyEnterEvent(KeyEnterEvent newEvent);

}
