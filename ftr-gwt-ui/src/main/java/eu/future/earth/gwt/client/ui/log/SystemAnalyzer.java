package eu.future.earth.gwt.client.ui.log;

import java.io.Serializable;

import com.google.gwt.core.shared.GWT;

public class SystemAnalyzer implements Serializable {

	private static ErrorPopup popup = null;

	/**
	 * 
	 */
	private static final long serialVersionUID = 2672998516396654364L;

	private static boolean showPopup = false;

	public static void infoAllwayAllowed(String value) {
		debug(value);
	}

	public static void infoAllwayAllowed(String value, Throwable error) {
		debug(value, error);
	}

	public static void info(String value) {
		debug(value);
	}

	public static void info(String value, Throwable error) {
		debug(value, error);
	}

	public static void debug(String value, Throwable error) {
		debug(value, error, true);
	}

	public static void debug(String value, Throwable error, boolean popupAllow) {
		if (error != null) {
			GWT.log(value, error);
		} else {
			GWT.log(value);
		}

		if (showPopup && popup == null && popupAllow) {
			popup = new ErrorPopup();
		}
		if (popup != null) {
			popup.show();
			popup.addMessage(value);
			popup.addMessage(error);
		}
	}

	public static void debug(String value, boolean popupAllow) {
		debug(value, null, popupAllow);
	}

	public static void debug(String value) {
		debug(value, null);
	}

}
