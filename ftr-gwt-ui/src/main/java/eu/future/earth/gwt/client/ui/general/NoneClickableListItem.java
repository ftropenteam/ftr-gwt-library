package eu.future.earth.gwt.client.ui.general;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;

public class NoneClickableListItem extends ComplexPanel implements HasText {

	public NoneClickableListItem() {
		setElement(Document.get().createLIElement());
	}

	public NoneClickableListItem(final Widget newItem) {
		setElement(Document.get().createLIElement());
		add(newItem);

	}

	@Override
	public void add(final Widget w) {
		super.add(w, (Element) getElement());
	}

	@Override
	public String getText() {
		return getElement().getInnerText();
	}

	public void insert(final Widget w, final int beforeIndex) {
		super.insert(w, (Element) getElement(), beforeIndex, true);
	}

	@Override
	public void setText(final String text) {
		getElement().setInnerText(text);
	}

}
