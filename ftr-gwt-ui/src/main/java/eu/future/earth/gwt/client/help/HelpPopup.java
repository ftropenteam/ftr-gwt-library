package eu.future.earth.gwt.client.help;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.ui.ImageResources;
import eu.future.earth.gwt.client.ui.button.CleanPopupPanel;
import eu.future.earth.gwt.client.ui.button.TouchButton;
import eu.future.earth.gwt.client.ui.event.ClickTouchEvent;
import eu.future.earth.gwt.client.ui.event.ClickTouchHandler;
import eu.future.earth.gwt.client.ui.panels.PanelWithMainAndButtonPanel;

public class HelpPopup extends CleanPopupPanel implements ClickTouchHandler {

	private TouchButton cancel = new TouchButton(ImageResources.STATIC_TEXT.close());

	public HelpPopup(final String message) {
		this(message, null);
	}

	public HelpPopup(final String message, final String title) {
		super(true);
		Widget add = null;
		final HTML html = new HTML(message);
		html.setStyleName("ftr-help-text");
		if (title != null) {
			super.setTitle(title);
			final FlowPanel top = new FlowPanel();
			Label t = new Label(title);
			t.setStyleName("popup-title");
			top.add(t);
			top.add(html);
			add = top;
		} else {
			add = html;
		}
		final PanelWithMainAndButtonPanel top = new PanelWithMainAndButtonPanel(add);
		super.setWidget(top);
		top.addButton(cancel);
		cancel.addClickHandler(this);
	}

	public void show(final Widget below) {
		if (below == null) {
			super.show();
			super.center();
		} else {
			super.show();
			super.setPopupPosition(below.getAbsoluteLeft(), below.getAbsoluteTop() + below.getOffsetHeight() + 10);
			super.getElement().scrollIntoView();
		}
	}

	@Override
	public void onClick(final ClickTouchEvent event) {
		if (event.getSource() == cancel) {
			this.hide();
		}
	}

}
