package eu.future.earth.gwt.client.ui.log;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.ui.ImageResources;
import eu.future.earth.gwt.client.ui.button.CleanPopupPanel;
import eu.future.earth.gwt.client.ui.button.TouchButton;
import eu.future.earth.gwt.client.ui.event.ClickTouchEvent;
import eu.future.earth.gwt.client.ui.event.ClickTouchHandler;
import eu.future.earth.gwt.client.ui.panels.PanelWithMainAndButtonPanel;

public class ErrorPopup extends CleanPopupPanel implements ClickTouchHandler {

	private TouchButton cancel = new TouchButton(ImageResources.STATIC_TEXT.close());

	private TouchButton clear = new TouchButton("Clear");

	private FlowPanel html = new FlowPanel();

	public ErrorPopup() {
		super(true);
		html.setStyleName("ftr-help-text");

		ScrollPanel scroll = new ScrollPanel(html);

		final PanelWithMainAndButtonPanel top = new PanelWithMainAndButtonPanel(scroll);
		super.setWidget(top);
		top.addButton(cancel);
		top.addButton(clear);
		clear.addClickHandler(new ClickTouchHandler() {

			@Override
			public void onClick(ClickTouchEvent event) {
				html.clear();
			}
		});
		cancel.addClickHandler(this);
		scroll.setPixelSize(300, 300);
	}

	public void show(Widget below) {
		if (below == null) {
			super.show();
			super.center();
		} else {
			super.show();
			super.setPopupPosition(below.getAbsoluteLeft(), below.getAbsoluteTop() + below.getOffsetHeight() + 10);
			super.getElement().scrollIntoView();
		}
	}

	public void addMessage(String message) {
		if (message != null) {
			html.add(new HTML(message));
		}
	}

	public void addMessage(Throwable message) {
		if (message != null) {
			html.add(new HTML(message.toString()));
		}
	}

	@Override
	public void onClick(ClickTouchEvent event) {
		if (event.getSource() == cancel) {
			this.hide();
		}
	}

}
