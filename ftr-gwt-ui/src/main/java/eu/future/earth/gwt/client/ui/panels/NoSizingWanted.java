package eu.future.earth.gwt.client.ui.panels;

/**
 * Place this interface on element that arange their own sizing.
 * 
 * @author Marteijn Nouwens
 */
public interface NoSizingWanted {

}
