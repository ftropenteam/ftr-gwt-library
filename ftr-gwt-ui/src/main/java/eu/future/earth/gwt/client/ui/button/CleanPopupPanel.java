package eu.future.earth.gwt.client.ui.button;

import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

public class CleanPopupPanel extends PopupPanel {

	public CleanPopupPanel() {
		super();
		getElement().removeClassName("gwt-PopupPanel");
		getElement().addClassName("clean-PopupPanel");
		getContainerElement().removeClassName("popupContent");
	}

	public CleanPopupPanel(boolean autoHide) {
		super(autoHide);
		getElement().removeClassName("gwt-PopupPanel");
		getElement().addClassName("clean-PopupPanel");
		getContainerElement().removeClassName("popupContent");
	}

	public CleanPopupPanel(boolean autoHide, boolean modal) {
		super(autoHide, modal);
		getElement().removeClassName("gwt-PopupPanel");
		getElement().addClassName("clean-PopupPanel");
		getContainerElement().removeClassName("popupContent");
	}

	@Override
	public void add(Widget newWidget) {
		getElement().removeClassName("gwt-PopupPanel");
		getElement().addClassName("clean-PopupPanel");
		getContainerElement().removeClassName("popupContent");
		getContainerElement().getParentElement().appendChild(newWidget.getElement());
		super.getContainerElement().removeFromParent();
	}

}
