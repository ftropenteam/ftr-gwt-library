package eu.future.earth.gwt.client.logic;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class UUIDTest {

	@Test
	public void testUuid() {
		final String res = UUID.uuid();
		assertNotNull(res);
		assertNotEquals(0, res.length());
	}

}
