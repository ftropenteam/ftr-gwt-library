package eu.future.earth.gwt.client.ui.general;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class CssHelperTest {

	@Test
	public void testGetFontFamily() {

		//		assertEquals("Verdana", CssHelper.extractProperty(CssHelper.A.FONT_FAMILY, "text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px;"));

		assertNull(CssHelper.extractProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px;"));
		assertEquals("Verdana", CssHelper.extractProperty(CssHelper.A.FONT_FAMILY, "font-family: Verdana;"));
		assertEquals("Verdana", CssHelper.extractProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family: Verdana;"));
		assertNull(CssHelper.extractProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family Verdana;"));
		assertEquals("Verdana", CssHelper.extractProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family: Verdana; font-weight : bold;"));
		assertEquals("Verdana", CssHelper.extractProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family : Verdana; font-weight : bold;"));
		assertEquals("Verdana", CssHelper.extractProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family  : Verdana ; font-weight : bold;"));
		assertNull(CssHelper.extractProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family Verdana; font-weight : bold;"));
	}

	@Test
	public void testSetttFontFamily() {
		assertEquals("font-size: 24px;", CssHelper.replaceProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px;", null));
		assertEquals("", CssHelper.replaceProperty(CssHelper.A.FONT_FAMILY, "font-family: Verdana;", null));
		assertEquals("font-family: Times New Roman; font-size: 24px;", CssHelper.replaceProperty(CssHelper.A.FONT_FAMILY, "font-family: Verdana; font-size: 24px;", "Times New Roman"));
		assertEquals("font-size: 24px; font-family: Times New Roman;", CssHelper.replaceProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px;", "Times New Roman"));

		assertEquals("font-size: 24px; font-family: Times New Roman;", CssHelper.replaceProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family: Verdana;", "Times New Roman"));
		assertEquals("font-size: 24px; font-family: Times New Roman;", CssHelper.replaceProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family Verdana;", "Times New Roman"));
		assertEquals("font-family: Times New Roman;", CssHelper.replaceProperty(CssHelper.A.FONT_FAMILY, null, "Times New Roman"));
		assertEquals("font-size: 24px; font-family: Times New Roman; font-weight : bold;", CssHelper.replaceProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family : Verdana; font-weight : bold;", "Times New Roman"));
		assertEquals("font-size: 24px; font-family: Times New Roman; font-weight : bold;", CssHelper.replaceProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family  : Verdana ; font-weight : bold;", "Times New Roman"));
		assertEquals("font-size: 24px; font-family: Times New Roman; font-weight : bold;", CssHelper.replaceProperty(CssHelper.A.FONT_FAMILY, "font-size: 24px; font-family Verdana; font-weight : bold;", "Times New Roman"));
	}

}
