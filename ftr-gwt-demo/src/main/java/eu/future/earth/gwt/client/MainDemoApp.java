/*
 * Copyright 2007 Future Earth, info@future-earth.eu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package eu.future.earth.gwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;

import eu.future.earth.gwt.client.hordate.HorizontalDemo;
import eu.future.earth.gwt.client.slider.SliderDemo;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class MainDemoApp implements EntryPoint {

	private OverviewPanelDemo cal = new OverviewPanelDemo();

	private HorizontalDemo calHor = new HorizontalDemo();

	private ChartDemo chart = new ChartDemo();

	private FlowPanel top = new FlowPanel();

	private DockLayoutPanel panel = new DockLayoutPanel(Unit.PX);

	private SliderDemo slider = new SliderDemo();
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		RootLayoutPanel.get().add(panel);
		panel.addNorth(top, 30);
//		panel.add(chart);
		panel.add(cal);
		{
			Button vert = new Button("Vertical");
			top.add(vert);
			vert.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					panel.clear();
					panel.addNorth(top, 30);
					panel.add(cal);
				}
			});
		}
		{
			Button vert = new Button("Chart");
			top.add(vert);
			vert.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					panel.clear();
					panel.addNorth(top, 30);
					panel.add(chart);
				}
			});
			
		}
		{
			Button hor = new Button("Horizontal");
			top.add(hor);
			hor.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					panel.clear();
					panel.addNorth(top, 30);
					panel.add(calHor);
				}
			});
		}
		{
			Button hor = new Button("Slider");
			top.add(hor);
			hor.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					panel.clear();
					panel.addNorth(top, 30);
					panel.add(slider);
				}
			});
		}
	}

}
