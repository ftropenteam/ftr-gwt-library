package eu.future.earth.gwt.client;

public class RatioResult {

	private int hourxValue = 1;

	private int yValue = 0;

	public int getHourxValue() {
		return hourxValue;
	}

	public void setxValue(int xValue) {
		this.hourxValue = xValue;
	}

	public int getyValue() {
		return yValue;
	}

	public void setyValue(int yValue) {
		this.yValue = yValue;
	}

	public RatioResult(int xValue, int yValue) {
		super();
		this.hourxValue = xValue;
		this.yValue = yValue;
	}

}
