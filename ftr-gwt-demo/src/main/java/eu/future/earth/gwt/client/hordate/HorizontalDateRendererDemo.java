package eu.future.earth.gwt.client.hordate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.date.DateEvent.DateEventActions;
import eu.future.earth.gwt.client.date.DateEventListener;
import eu.future.earth.gwt.client.date.DefaultEventData;
import eu.future.earth.gwt.client.date.EventPanel;
import eu.future.earth.gwt.client.date.horizontal.BaseHorizontalDateRenderer;
import eu.future.earth.gwt.client.date.horizontal.HorizontalAddCallBackHandler;
import eu.future.earth.gwt.client.date.horizontal.HorizontalRemoveCallBackHandler;
import eu.future.earth.gwt.client.date.horizontal.HorizontalUpdateCallBackHandler;
import eu.future.earth.gwt.client.date.horizontal.HorizontalViewPanelNoDays;

public class HorizontalDateRendererDemo extends BaseHorizontalDateRenderer<DefaultEventData, UserDemo> implements DemoHandler {

	public int getEndHour() {
		return 23;
	}

	public int getStartHour() {
		return 0;
	}

	public boolean enableDragAndDrop() {
		return true;
	}

	public void createNewAfterClick(UserDemo theMaster, Date currentDate, Date endTime, DateEventListener<DefaultEventData> listener) {
		UserDemo master = (UserDemo) theMaster;
		if (theMaster == null) {
			return;
		}
		final DefaultEventData data = new DefaultEventData(System.currentTimeMillis());
		data.setStartTime(currentDate);

		data.setMasterId(master.getId());
		data.setEndTime(endTime);
		final DefaultStringEventDataDialog dialog = new DefaultStringEventDataDialog(this, data);
		dialog.addDateEventHandler(listener);
		dialog.show();
		dialog.center();

	}

	public void createNewAfterClick(UserDemo theMaster, Date currentDate, DateEventListener<DefaultEventData> listener) {
		UserDemo master = (UserDemo) theMaster;
		if (theMaster == null) {
			return;
		}
		final DefaultEventData data = new DefaultEventData(System.currentTimeMillis());
		data.setStartTime(currentDate);
		data.setMasterId(master.getId());
		final DefaultStringEventDataDialog dialog = new DefaultStringEventDataDialog(this, data);
		dialog.addDateEventHandler(listener);
		dialog.show();
		dialog.center();

	}

	public EventPanel<DefaultEventData> createPanel(DefaultEventData newData) {
		final DefaultHorizontalDayField panel = new DefaultHorizontalDayField(this, newData);
		return panel;
	}

	public void editAfterClick(UserDemo theMaster, DefaultEventData data, DateEventListener<DefaultEventData> listener) {
		final DefaultStringEventDataDialog dialog = new DefaultStringEventDataDialog(this, data, DateEventActions.UPDATE);
		dialog.addDateEventHandler(listener);
		dialog.show();
		dialog.center();

	}

	public Date getEndTime(DefaultEventData data) {

		return data.getEndTime();
	}

	public Date getStartTime(DefaultEventData data) {

		return data.getStartTime();
	}

	public void setEndTime(DefaultEventData data, Date newEnd) {

		data.setEndTime(newEnd);

	}

	public void setStartTime(DefaultEventData data, Date newStart) {

		data.setStartTime(newStart);
	}

	public boolean isDurationAcceptable(int minutes) {
		return minutes >= (60 / getIntervalsPerHour());
	}

	public boolean isMaster(UserDemo theMaster, DefaultEventData theDateObject) {
		UserDemo master = (UserDemo) theMaster;

		DefaultEventData real = (DefaultEventData) theDateObject;
		if (real.getMasterId() == master.getId()) {
			return true;
		}
		return false;
	}

	public Widget getLabelForMaster(UserDemo theMaster) {
		UserDemo master = (UserDemo) theMaster;
		if (theMaster != null) {
			return new Label(master.getName());
		}
		return null;
	}

	public boolean isSameMaster(UserDemo theMaster, UserDemo theOtherMaster) {
		UserDemo master = (UserDemo) theMaster;
		UserDemo sec = (UserDemo) theOtherMaster;
		return sec.getId() == master.getId();
	}

	public void getMaster(HorizontalViewPanelNoDays<DefaultEventData, UserDemo> caller, DefaultEventData real) {
		UserDemo test = new UserDemo(real.getMasterId(), "Not set");
		caller.addRow(test);
		caller.addEventData(real);
	}

	@Override
	public boolean equal(DefaultEventData one, DefaultEventData two) {
		return one.getIdentifier().equals(two.getIdentifier());
	}

	@Override
	public String getId(DefaultEventData data) {
		return data.getIdentifier();
	}

	@Override
	public void getEventsForRange(HorizontalViewPanelNoDays<DefaultEventData, UserDemo> view) {
		ArrayList<DefaultEventData> found = new ArrayList<DefaultEventData>();
		java.util.Iterator<DefaultEventData> walker = items.values().iterator();
		while (walker.hasNext()) {
			DefaultEventData data = (DefaultEventData) walker.next();
			found.add(data);
		}
		view.setEvents(found);
	}

	@Override
	public void addEvent(HorizontalAddCallBackHandler<DefaultEventData, UserDemo> callback) {
		items.put(callback.getData().getIdentifier(), callback.getData());
		callback.onSuccess(callback.getData());
	}

	@Override
	public void updateEvent(HorizontalUpdateCallBackHandler<DefaultEventData, UserDemo> callback) {
		items.put(callback.getData().getIdentifier(), callback.getData());
		callback.succes();

	}

	@Override
	public void removeEvent(HorizontalRemoveCallBackHandler<DefaultEventData, UserDemo> callback) {
		items.put(callback.getData().getIdentifier(), callback.getData());
		callback.succes();
	}

	private HashMap<String, DefaultEventData> items = new HashMap<String, DefaultEventData>();

	public void addEvent(DefaultEventData example) {

	}

}
