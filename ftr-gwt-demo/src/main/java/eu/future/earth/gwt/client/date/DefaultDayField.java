/*
 * Copyright 2007 Future Earth, info@future-earth.eu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package eu.future.earth.gwt.client.date;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.DemoCss;
import eu.future.earth.gwt.client.date.week.staend.AbstractDayField;

public class DefaultDayField extends AbstractDayField<DefaultEventData> {

	// private HorizontalPanel panel = new HorizontalPanel();
	private Label description = new Label();

	private VerticalPanel top = new VerticalPanel();

	private DateTimeFormat format = DateTimeFormat.getFormat("HH:mm");

	public DefaultDayField(DateRenderer<DefaultEventData> renderer, DefaultEventData data) {
		super(renderer, data);
		description.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent arg0) {
				sendEdit();
			}
		});
		FlowPanel fl = new FlowPanel();
		fl.add(top);
		super.setBody(fl);
		HorizontalPanel test = new HorizontalPanel();
		test.add(new Label("Vert"));
		top.add(description);
		top.add(test);
		if (data != null) {
			helper.setTime(data.getStartTime());
			if (helper.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
				super.setEventStyleName(DemoCss.EVENT_PANEL_MONDAY, DemoCss.EVENT_HEADER_MONDAY);
			} else {
				if (helper.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
					super.setMainColor("#f9f990");
				}

			}
			description.setText((String) data.getData());
			repaintTime();
		}
	}

	@Override
	public void repaintTime() {
		final DefaultEventData real = super.getValue();
		if (real.getEndTime() == null) {
			super.setTitle(format.format(real.getStartTime()));
		} else {
			super.setTitle(format.format(real.getStartTime()) + "-" + format.format(real.getEndTime()));
		}

	}

	public Widget getClickableItem() {
		return description;
	}

	GregorianCalendar helper = new GregorianCalendar();

	public void setWidth(int width) {
		setWidth(width + "px");

	}

}
