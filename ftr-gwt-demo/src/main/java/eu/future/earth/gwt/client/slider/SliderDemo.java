package eu.future.earth.gwt.client.slider;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import eu.future.earth.slider.client.SliderBarVertical;

public class SliderDemo extends Composite {

	private HorizontalPanel main = new HorizontalPanel();

	public SliderDemo() {
		super();
		initWidget(main);

		{
			// add First
			VerticalPanel vert = new VerticalPanel();
			main.add(vert);
			Button info = new Button();
			final SliderBarVertical slider = new SliderBarVertical(40, 200);

			info.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					slider.setValue(80);
				}
			});
			vert.add(info);
			final TextBox value = new TextBox();
			vert.add(value);
			slider.setTickValue(5);
			slider.setMajorTickValue(20);
			slider.setValue(55);

			vert.add(slider);
			slider.addValueChangeHandler(new ValueChangeHandler<Integer>() {

				@Override
				public void onValueChange(ValueChangeEvent<Integer> event) {
					value.setText(String.valueOf(event.getValue()));
				}
			});
			value.setText(String.valueOf(slider.getValue()));
		}
	}

}
