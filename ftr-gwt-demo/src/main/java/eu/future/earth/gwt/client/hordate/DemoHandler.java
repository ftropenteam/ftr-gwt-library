package eu.future.earth.gwt.client.hordate;

import eu.future.earth.gwt.client.date.DefaultEventData;

public interface DemoHandler {

	void addEvent(DefaultEventData example);

}
