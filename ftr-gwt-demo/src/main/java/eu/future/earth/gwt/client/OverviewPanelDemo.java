/*
 * Copyright 2007 Future Earth, info@future-earth.eu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package eu.future.earth.gwt.client;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ResizeComposite;

import eu.future.earth.gwt.client.date.DateEvent;
import eu.future.earth.gwt.client.date.DateEventListener;
import eu.future.earth.gwt.client.date.DefaultEventData;
import eu.future.earth.gwt.client.date.DefaultStringPanelRenderer;
import eu.future.earth.gwt.client.date.MultiView;
import eu.future.earth.gwt.client.date.PanelType;
import eu.future.earth.gwt.client.date.picker.DatePickerMonthNavigator;
import eu.future.earth.gwt.client.date.picker.DateSelectEvent;
import eu.future.earth.gwt.client.date.picker.DateSelectListener;
import eu.future.earth.gwt.client.hordate.DemoHandler;

public class OverviewPanelDemo extends ResizeComposite implements
		DateEventListener<DefaultEventData>, ClickHandler, DateSelectListener {

	private Label feedback = new Label(" ");

	private DefaultStringPanelRenderer contr = new DefaultStringPanelRenderer();

	private MultiView<DefaultEventData> multiPanel = new MultiView<DefaultEventData>(
			contr);

	private DatePickerMonthNavigator navigator = new DatePickerMonthNavigator(
			new NoMondaysDatePickerRenderer());

	public OverviewPanelDemo() {
		super();
		DockLayoutPanel panel = new DockLayoutPanel(Unit.PX);
		initWidget(panel);

		Grid topLeft = new Grid(1, 1);
		topLeft.setWidth("100%");
		topLeft.setWidget(0, 0, navigator);

		panel.addNorth(feedback, 30);
		panel.addWest(topLeft, 200);
		panel.add(multiPanel);

		multiPanel.addDateEventHandler(this);
		navigator.addDateSelectEventHandler(this);
		// setGrid.addClickListener(this);
		multiPanel.scrollToHour(9);

		
	}

	private boolean run = true;
	
	public static List<DefaultEventData> createPlanningSamples(DemoHandler contr) {
		DateTimeFormat format = DateTimeFormat.getFormat("HH:mm");
		long id = System.currentTimeMillis();
		List<DefaultEventData> data = new ArrayList<DefaultEventData>();
		{
			DefaultEventData example = new DefaultEventData(id++);

			example.setData("example for Nettie");
			Calendar helper = new GregorianCalendar();
			helper.setTime(format.parse("10:30"));
			example.setStartTime(helper.getTime());
			helper.add(Calendar.MINUTE, 10);
			example.setEndTime(helper.getTime());

			data.add(example);
			contr.addEvent(example);
		}
		{
			DefaultEventData example = new DefaultEventData(id++);
			example.setData("example whole for Nettie");
			example.setWholeDay(true);
			Calendar helper = new GregorianCalendar();
			helper.setTime(format.parse("10:30"));
			example.setStartTime(helper.getTime());
			data.add(example);
			contr.addEvent(example);
		}
		{
			DefaultEventData example = new DefaultEventData(id++);
			example.setData("example for Nettie");
			Calendar helper = new GregorianCalendar();
			// helper.setTime(format.parse("10:30"));
			example.setStartTime(helper.getTime());
			helper.add(Calendar.MINUTE, 60);
			example.setEndTime(helper.getTime());

			data.add(example);
			contr.addEvent(example);
		}

		{
			DefaultEventData example = new DefaultEventData(id++);
			example.setData("example for Nettie 22");
			Calendar helper = new GregorianCalendar();
			helper.setTime(format.parse("12:30"));
			example.setStartTime(helper.getTime());
			helper.add(Calendar.MINUTE, 60);
			example.setEndTime(helper.getTime());

			data.add(example);
			contr.addEvent(example);
		}

		{
			DefaultEventData example = new DefaultEventData(id++);
			example.setData("Multi day event");
			Calendar helper = new GregorianCalendar();

			helper.setTime(format.parse("22:30"));
			helper.add(Calendar.DATE, 1);
			example.setStartTime(helper.getTime());
			helper.add(Calendar.HOUR, 4);
			example.setEndTime(helper.getTime());
			data.add(example);
			contr.addEvent(example);
		}
		return data;
	}

	public void handleDateEvent(DateEvent<DefaultEventData> newEvent) {
		switch (newEvent.getCommand()) {
		case ADD: {
			final DefaultEventData data = (DefaultEventData) newEvent.getData();
			feedback.setText("Added event on " + data.getStartTime() + " - "
					+ data.getEndTime());
			break;
		}
		case SELECT_DAY: {
			if (newEvent.getSource() == navigator) {
				multiPanel.setDate(newEvent.getDate());
			}
			break;
		}
		case SELECT_MONTH: {
			if (newEvent.getSource() == navigator) {
				multiPanel.setType(PanelType.MONTH);
				multiPanel.setDate(newEvent.getDate());
			}
			break;
		}
		case UPDATE: {
			final DefaultEventData data = (DefaultEventData) newEvent.getData();
			feedback.setText("Updated event on " + data.getStartTime() + " - "
					+ data.getEndTime());
			break;
		}
		case REMOVE: {
			final DefaultEventData data = (DefaultEventData) newEvent.getData();
			feedback.setText("Removed event on " + data.getStartTime() + " - "
					+ data.getEndTime());
			break;
		}

		case DRAG_DROP: {
			final DefaultEventData data = (DefaultEventData) newEvent.getData();
			feedback.setText("Removed event on " + data.getStartTime() + " - "
					+ data.getEndTime());
			break;
		}
		}

	}

	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onAttach() {
		super.onAttach();
if(run){
	run = false;
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				List<DefaultEventData> data = createPlanningSamples(contr);
				multiPanel.setEventsByList(data);
			}

		});
}
	}

	@Override
	public void handleDateSelectEvent(DateSelectEvent newEvent) {
		switch (newEvent.getCommand()) {
		case SELECT_DAY: {
			if (newEvent.getSource() == navigator) {
				multiPanel.setDate(newEvent.getDate());
			}
			break;
		}
		case SELECT_NEW_MONTH: {
			if (newEvent.getSource() == navigator) {
				multiPanel.setType(PanelType.MONTH);
				multiPanel.setDate(newEvent.getDate());
			}
			break;
		}
		}
	}
}
