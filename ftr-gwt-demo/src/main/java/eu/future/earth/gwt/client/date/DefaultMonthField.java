/*
 * Copyright 2007 Future Earth, info@future-earth.eu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package eu.future.earth.gwt.client.date;

import java.util.Calendar;
import java.util.GregorianCalendar;


import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import eu.future.earth.gwt.client.DemoCss;
import eu.future.earth.gwt.client.date.month.staend.AbstractMonthField;

public class DefaultMonthField extends AbstractMonthField<DefaultEventData> implements ClickHandler {

	// private HorizontalPanel panel = new HorizontalPanel();
	private Label description = new Label(); // NOPMD;

	private DateTimeFormat format = DateTimeFormat.getFormat("HH:mm"); // NOPMD;

	public DefaultMonthField(DateRenderer<DefaultEventData> renderer, DefaultEventData data) {
		super(renderer, data);
		description.addClickHandler(this);
		super.setBody(description);
		description.setHeight("12px");
		description.setText(data.getData());
		repaintTime();
	}

	public Widget getClickableItem() {
		return description;
	}

	private GregorianCalendar helper = new GregorianCalendar();

	@Override
	public void repaintTime() {
		final DefaultEventData theData = getValue();
		if (theData != null) {
			final DefaultEventData real = (DefaultEventData) theData;
			helper.setTime(real.getStartTime());
			if (helper.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
				super.setEventStyleName(DemoCss.WHOLEDAY_PANEL_MONDAY);
			}
			super.setTitle(format.format(real.getStartTime()));
		}
	}

}
